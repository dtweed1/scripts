package MIDI::64track;

# module to parse and do simple verification of a 64track sequencer file

# To do:
#   Validate data before writing it out to a 64track file.
#   Provide methods to modify an opus.

# History:
#   2003/03/16 DT   Trim trailing spaces from track names;
#                   put them in MIDIfile.
#   2003/03/10 DT   Add track0 conversion.
#   2003/03/09 DT   Start.

sub read_file {
    my ($filename) = @_;
    my $opus = {};

    open (MIDI, $filename) || die "can't read $filename: $!";
    binmode MIDI;

    $opus->{'filename'} = $filename;

    # read the file header
    my $magic = read_bytes (4);
    unless ('MID3' eq $magic) {
        warn "not a 64track file";
        close MIDI;
        return undef;
    }
    $opus->{'events'} = read_int16_rev();
    $opus->{'comment'} = read_nullterm_string();
    $opus->{'ppqn'} = read_int16_rev();
    $opus->{'time_signature'} = read_int8();
    $opus->{'tempo'} = read_int24();
    $opus->{'SMPTE_start'} = read_bytes (6);
    $opus->{'SMPTE_rate'} = read_int16_rev();
    $opus->{'use_switches'} = read_bytes (8);
    $opus->{'port_map'} = read_bytes (32);
    unless (read_bytes (16)) {
        warn "unexpected EOF";
        close MIDI;
        return undef;
    }
    push @{$opus->{'tracks'}}, read_track_zero();
    while (defined (my $track = read_track())) {
        push @{$opus->{'tracks'}}, $track;
    }
    close MIDI;
    $opus;
}

sub write_file {
    my ($opus, $filename) = @_;

    open (MIDI, ">$filename") || die "can't write $filename: $!";
    binmode MIDI;
    print MIDI 'MID3';
    print MIDI write_int16_rev ($opus->{'events'});
    print MIDI $opus->{'comment'}, "\0";
    print MIDI write_int16_rev ($opus->{'ppqn'});
    print MIDI chr $opus->{'time_signature'};
    print MIDI write_int24 ($opus->{'tempo'});
    print MIDI $opus->{'SMPTE_start'};
    print MIDI write_int16_rev ($opus->{'SMPTE_rate'});
    print MIDI $opus->{'use_switches'};
    print MIDI $opus->{'port_map'};
    print MIDI "\0" x 16;
    write_track_zero ($opus->{'tracks'}[0]);
    for (1..$#{$opus->{'tracks'}}) {
        warn "writing track $_\n";
        write_track ($opus->{'tracks'}[$_]);
    }
    close MIDI;
}

sub read_track_zero {
    my $track = {};
    while (defined (my $event = read_track0_event())) {
        push @{$track->{'events'}}, $event;
        last if ord $event->[1] == 0xFD;
    }
    $track;
}

sub write_track_zero {
    my ($track) = @_;
    for (@{$track->{'events'}}) {
        print MIDI write_event ($_);
    }
}

sub read_track0_event {
    my $timestamp = read_int24();
    my $first = read_bytes (1);
    my $rest;
    if (ord $first == 0xFD) {
        return [$timestamp, $first];
    } elsif (ord $first == 0x35) {
        # TBD: this seems to be at odds with the assertion
        # "Track zero events are always three data bytes."
        $rest = read_bytes (1);
    } else {
        $rest = read_bytes (2);
    }
    [$timestamp, $first.$rest];
}

sub read_track {
    my $track = {};
    return undef if eof MIDI;

    # read the track header
    $track->{'channel'} = read_int8();
    $track->{'mute'} = read_int8();
    # read the track name, trim trailing whitespace
    ($track->{'name'} = read_bytes (30)) =~ s/\s+$//;
    while (defined (my $event = read_event())) {
        push @{$track->{'events'}}, $event;
        last if $event->[1] eq "\xFD";
    }
    $track;
}

sub write_track {
    my ($track) = @_;
    print MIDI chr $track->{'channel'};
    print MIDI chr $track->{'mute'};
    # pad name with spaces to 30 characters
    # TBD: truncate if necessary
    printf MIDI '%-30s', $track->{'name'};
    for (@{$track->{'events'}}) {
        print MIDI write_event ($_);
    }
}

sub track_zero_as_midifile {
    my ($opus, @data) = @_;

    # TBD: if @data is defined, set the track data from it.

    my @midi;
    my $time = 0;
    my $tempo = int(600000000/$opus->{'tempo'});
    push @midi, ['track_name', 0, $opus->{'comment'} || $opus->{'filename'}];
    push @midi, ['time_signature', 0, 
                 $opus->{'time_signature'}, # numerator
                 2,                         # denominator (2=quarter note)
                 24,                        # MIDI clocks/click
                 8],                        # 32nd notes/MIDI quarter
    push @midi, ['key_signature', 0, 0, 0];
    push @midi, ['set_tempo', 0, $tempo];

    # TBD: put 64track-specific data in as tagged text comments
    # 64track-use: 0000000000000000 (up to 8 bytes as hex; skip trailing zeros)
    # 64track-ports: 000000000000...0000 (up to 32 bytes as hex)

    for (@{$opus->{'tracks'}[0]{'events'}}) {
        my ($newtime, $event) = @$_;
        my $delta = $newtime - $time;
        $time = $newtime;

        my @event = map ord, split //, $event;
        if ($event[0] == 0x33) {
            # Tempo changes in 64track are expressed as a percentage change
            # from the previous tempo.
            $tempo = int ($tempo / (1 + $event[1]/100));
            # set_tempo delta us_per_quarter
            push @midi, ['set_tempo', $delta, $tempo];
        } elsif ($event[0] == 0xFD) {
            # Normal end-of-track
            push @midi, ['end_track', $delta, 0, 0];
        } else {
            # We should also be able to do cue marks and time signature changes
            warn "unrecognized track0 event: ", sprintf "%02X", $event[0];
            # Insert a dummy event to maintain timekeeping
            push @midi, ['set_tempo', $delta, $tempo];
        }
    }

    @midi;
}

# Render a track as a list-of-lists, where each event is represented as an
# N-tuple: [command, delta time, channel, note, velocity]
# (some commands don't use all bytes)

sub track_as_midifile {
    my ($track, @data) = @_;

    # TBD: if @data is defined, set the track data from it.

    # Render the track as MIDIfile data.

    my @midi;
    my $time = 0;

    # Put the track name in as a text event.
    my $name = $track->{'name'};
    push @midi, ['track_name', 0, $name] if $name;

    # TBD: Create default voice selection.
    # patch_change 0 0 0

    my $status = 0;
    my $command = '';
    my $channel = 0;
    for (@{$track->{'events'}}) {
        # TBD: convert time scale?
        my ($newtime, $event) = @$_;
        my $delta = $newtime - $time;
        $time = $newtime;

        # Does this event have a status byte?
        my @event = map ord, split //, $event;
        if ($event[0] >= 128) {
            $status = shift @event;
            $command = qw(note_off note_on key_after_touch control_change
                          patch_change channel_after_touch pitch_wheel_change
                          end_track
                          )[($status>>4) & 0x07];
            # Use channel from status byte unless track channel is nonzero.
            $channel = $track->{'channel'} || ($status & 0x0F);
        }
        # warn "$command, $delta, $channel, @event\n";
        # my $dummy = <STDIN>;
        push @midi, [$command, $delta, $channel, @event];
    }

    @midi;
}

sub read_event {
    my $timestamp = read_int24();
    my $first = read_bytes (1);
    my $second;
    my $third;

    if ((my $status = ord $first) >= 128) {
        # new channel status
        my $high = ($status >> 4) & 0x07;
        my $chan = $status & 0x0F;
        if ($high == 0) {
            # Note off, two data bytes
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            $third = read_bytes (1);
            warn "unexpected status byte" if ord $third >= 128;
            return [$timestamp, $first.$second.$third];
        } elsif ($high == 1) {
            # Note on, two data bytes
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            $third = read_bytes (1);
            warn "unexpected status byte" if ord $third >= 128;
            return [$timestamp, $first.$second.$third];
        } elsif ($high == 2) {
            # Key pressure, two data bytes
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            $third = read_bytes (1);
            warn "unexpected status byte" if ord $third >= 128;
            return [$timestamp, $first.$second.$third];
        } elsif ($high == 3) {
            # Control change or channel mode, two data bytes
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            $third = read_bytes (1);
            warn "unexpected status byte" if ord $third >= 128;
            return [$timestamp, $first.$second.$third];
        } elsif ($high == 4) {
            # Program change, one data byte
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            return [$timestamp, $first.$second];
        } elsif ($high == 5) {
            # Channel pressure, one data byte
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            return [$timestamp, $first.$second];
        } elsif ($high == 6) {
            # Pitch wheel, two data bytes
            $second = read_bytes (1);
            warn "unexpected status byte" if ord $second >= 128;
            $third = read_bytes (1);
            warn "unexpected status byte" if ord $third >= 128;
            return [$timestamp, $first.$second.$third];
        } else {
            # 0xFn = system status
            if ($status == 0xFD) {
                # end of track
                return [$timestamp, $first];
            } else {
                warn "unexpected system status byte in track event";
            }
        }
    } else {
        # running status, first of two bytes
        $second = read_bytes (1);
        warn "unexpected status byte" if ord $second >= 128;
        return [$timestamp, $first.$second];
    }
}

sub write_event {
    my ($event) = @_;
    write_int24 ($event->[0]) . $event->[1];
}

sub read_bytes {
    my ($length) = @_;
    my $data;
    unless (read MIDI, $data, $length) {
        warn "unexpected EOF";
        return undef;
    }
    $data;
}

sub read_int8 {
    my $data;
    unless (read MIDI, $data, 1) {
        warn "unexpected EOF";
        return undef;
    }
    unpack 'C', $data;
}

sub read_int16_rev {
    my $data;
    unless (read MIDI, $data, 2) {
        warn "unexpected EOF";
        return undef;
    }
    unpack 'v', $data;
}

sub write_int16_rev {
    my ($value) = @_;
    pack 'v', $value;
}

sub read_int24 {
    my $data;
    unless (read MIDI, $data, 3) {
        warn "unexpected EOF";
        return undef;
    }
    my ($hi, $lo) = unpack 'nC', $data;
    ($hi<<8) + $lo;
}

sub write_int24 {
    my ($value) = @_;
    pack 'nC', $value >> 8, $value & 0xFF;
}

sub read_nullterm_string {
    my $string = '';
    my $byte;
    while (1) {
        unless (read MIDI, $byte, 1) {
            warn "unexpected EOF";
            last;
        }
        last if $byte eq "\0";
        $string .= $byte;
    }
    $string;
}

sub dump_opus {
    my ($opus) = @_;

    printf "Events: %d\n", $opus->{'events'};
    printf "Comment: '%s'\n", $opus->{'comment'} if length $opus->{'comment'};
    printf "PPQN: %d\n", $opus->{'ppqn'};
    printf "Time Signature: %d\n", $opus->{'time_signature'};
    printf "Tempo: %.1f bpm\n", $opus->{'tempo'}/10;
    printf "SMPTE Start:%s\n", dump_bytes ($opus->{'SMPTE_start'});
    printf "SMPTE Frame Rate: %d\n", $opus->{'SMPTE_rate'};
    printf "Use switches:%s\n", dump_bytes ($opus->{'use_switches'});
    printf "Port map:%s\n", dump_bytes ($opus->{'port_map'});
    for (0..$#{$opus->{'tracks'}}) {
        print "----- Track $_ -----\n";
        dump_track ($opus->{'tracks'}[$_]);
    }
}

sub dump_track {
    my ($t) = @_;

    printf "Channel: %d\n", $t->{'channel'} if exists $t->{'channel'};
    printf "Mute: %d\n", $t->{'mute'} if exists $t->{'mute'};
    printf "Name: '%s'\n", $t->{'name'} if exists $t->{'name'};
    printf "%d events\n", scalar @{$t->{'events'}} if exists $t->{'events'};
    if ($t->{'name'}) {
        # dump first, last events on other tracks
        for (@{$t->{'events'}}[0, -1]) {
            print &dump_event ($_);
        }
    } else {
        # dump all events on track 0
        for (@{$t->{'events'}}) {
            print &dump_event ($_);
        }
    }
}

sub dump_event {
    my ($e) = @_;
    sprintf "%06X:%s\n", $e->[0], dump_bytes ($e->[1]);
}

sub dump_bytes {
    my ($data) = @_;
    my $out = '';
    for (split //, $data) {
        $out .= sprintf " %02X", ord $_;
    }
    $out;
}

1;
