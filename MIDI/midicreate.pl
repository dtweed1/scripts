#!perl -w

# midicreate.pl - create a trivial MIDI file to test compatibility

# History:
#   2003/03/10 DT   Start.

use MIDI;

my $filename = shift;

my $opus = MIDI::Opus->new ({ticks => 1024, # ppqn
                             format => 1});

my $track0 = MIDI::Track->new;
$track0->events (['track_name', 0, 'Test Piece'],
                 ['time_signature', 0, 3, 3, 36, 8],
                 ['key_signature', 0, 0, 0],
                 ['set_tempo', 0, 250000], # microseconds per quarter note
                 ['set_tempo', 5120, 500000], # microseconds per quarter note
                 ['end_track', 10240],
                 );
                 
my $track1 = MIDI::Track->new;
$track1->events (['track_name', 0, 'Notes'],
                 ['patch_change', 0, 0, 0],
                 ['control_change', 0, 0, 121, 127], # reset (?)
                 ['control_change', 0, 0, 64, 127], # volume
                 ['note_on', 1024, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['note_on', 24, 0, 64, 127],
                 ['note_on', 1000, 0, 64, 0],
                 ['end_track', 24],
                 );

$opus->tracks ($track0, $track1);
$opus->write_to_file ($filename);

print "Format: ", $opus->format, "\n";
print "Ticks: ", $opus->ticks, "\n";
printf "%d tracks\n", scalar $opus->tracks;

for ($opus->tracks) {
    printf "----- Track [%s] -----\n", $_->type;
    for ($_->events) {
        print "@$_\n";
    }
}
