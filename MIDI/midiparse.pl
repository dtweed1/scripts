#!perl -w

# midiparse.pl - preliminary program to parse and do simple verification of
#                a D-5 dump

# Each segment of the dump starts with F0 41 10 16 12, followed by a 3-byte
# address (remember, 7-bit bytes!), followed by the data, a checksum
# (presumably mod 128) and finally F7.

my %addrmap = (
    '00-00-00' => 'Timbre temp (table 1, 16 bytes)',
    '01-00-00' => 'Setup temp (85 x table 2, 340 bytes)',
    '02-00-00' => 'Tone temp (table 3, 246 bytes)',
    '05-00-00' => 'Timbre mem A11-A48 (32 x table 6, 256 bytes)',
    '05-02-00' => 'Timbre mem A51-A88 (32 x table 6, 256 bytes)',
    '05-04-00' => 'Timbre mem B11-B48 (32 x table 6, 256 bytes)',
    '05-06-00' => 'Timbre mem B51-B88 (32 x table 6, 256 bytes)',
    '07-00-00' => 'Patch mem A11-... (6.7 x table 4, 256 bytes)',
    '07-02-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-04-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-06-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-08-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-0A-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-0C-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-0E-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-10-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-12-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-14-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-16-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-18-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-1A-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-1C-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-1E-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-20-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-22-00' => 'Patch mem ...-... (6.7 x table 4, 256 bytes)',
    '07-24-00' => 'Patch mem ...-B88 (6.7 x table 4, 256 bytes)',
    '08-00-00' => 'Tone mem i01 (table 3, 246 bytes)',
    '08-02-00' => 'Tone mem i02 (table 3, 246 bytes)',
    '08-04-00' => 'Tone mem i03 (table 3, 246 bytes)',
    '08-06-00' => 'Tone mem i04 (table 3, 246 bytes)',
    '08-08-00' => 'Tone mem i05 (table 3, 246 bytes)',
    '08-0A-00' => 'Tone mem i06 (table 3, 246 bytes)',
    '08-0C-00' => 'Tone mem i07 (table 3, 246 bytes)',
    '08-0E-00' => 'Tone mem i08 (table 3, 246 bytes)',
    '08-10-00' => 'Tone mem i09 (table 3, 246 bytes)',
    '08-12-00' => 'Tone mem i10 (table 3, 246 bytes)',
    '08-14-00' => 'Tone mem i11 (table 3, 246 bytes)',
    '08-16-00' => 'Tone mem i12 (table 3, 246 bytes)',
    '08-18-00' => 'Tone mem i13 (table 3, 246 bytes)',
    '08-1A-00' => 'Tone mem i14 (table 3, 246 bytes)',
    '08-1C-00' => 'Tone mem i15 (table 3, 246 bytes)',
    '08-1E-00' => 'Tone mem i16 (table 3, 246 bytes)',
    '08-20-00' => 'Tone mem i17 (table 3, 246 bytes)',
    '08-22-00' => 'Tone mem i18 (table 3, 246 bytes)',
    '08-24-00' => 'Tone mem i19 (table 3, 246 bytes)',
    '08-26-00' => 'Tone mem i20 (table 3, 246 bytes)',
    '08-28-00' => 'Tone mem i21 (table 3, 246 bytes)',
    '08-2A-00' => 'Tone mem i22 (table 3, 246 bytes)',
    '08-2C-00' => 'Tone mem i23 (table 3, 246 bytes)',
    '08-2E-00' => 'Tone mem i24 (table 3, 246 bytes)',
    '08-30-00' => 'Tone mem i25 (table 3, 246 bytes)',
    '08-32-00' => 'Tone mem i26 (table 3, 246 bytes)',
    '08-34-00' => 'Tone mem i27 (table 3, 246 bytes)',
    '08-36-00' => 'Tone mem i28 (table 3, 246 bytes)',
    '08-38-00' => 'Tone mem i29 (table 3, 246 bytes)',
    '08-3A-00' => 'Tone mem i30 (table 3, 246 bytes)',
    '08-3C-00' => 'Tone mem i31 (table 3, 246 bytes)',
    '08-3E-00' => 'Tone mem i32 (table 3, 246 bytes)',
    '08-40-00' => 'Tone mem i33 (table 3, 246 bytes)',
    '08-42-00' => 'Tone mem i34 (table 3, 246 bytes)',
    '08-44-00' => 'Tone mem i35 (table 3, 246 bytes)',
    '08-46-00' => 'Tone mem i36 (table 3, 246 bytes)',
    '08-48-00' => 'Tone mem i37 (table 3, 246 bytes)',
    '08-4A-00' => 'Tone mem i38 (table 3, 246 bytes)',
    '08-4C-00' => 'Tone mem i39 (table 3, 246 bytes)',
    '08-4E-00' => 'Tone mem i40 (table 3, 246 bytes)',
    '08-50-00' => 'Tone mem i41 (table 3, 246 bytes)',
    '08-52-00' => 'Tone mem i42 (table 3, 246 bytes)',
    '08-54-00' => 'Tone mem i43 (table 3, 246 bytes)',
    '08-56-00' => 'Tone mem i44 (table 3, 246 bytes)',
    '08-58-00' => 'Tone mem i45 (table 3, 246 bytes)',
    '08-5A-00' => 'Tone mem i46 (table 3, 246 bytes)',
    '08-5C-00' => 'Tone mem i47 (table 3, 246 bytes)',
    '08-5E-00' => 'Tone mem i48 (table 3, 246 bytes)',
    '08-60-00' => 'Tone mem i49 (table 3, 246 bytes)',
    '08-62-00' => 'Tone mem i50 (table 3, 246 bytes)',
    '08-64-00' => 'Tone mem i51 (table 3, 246 bytes)',
    '08-66-00' => 'Tone mem i52 (table 3, 246 bytes)',
    '08-68-00' => 'Tone mem i53 (table 3, 246 bytes)',
    '08-6A-00' => 'Tone mem i54 (table 3, 246 bytes)',
    '08-6C-00' => 'Tone mem i55 (table 3, 246 bytes)',
    '08-6E-00' => 'Tone mem i56 (table 3, 246 bytes)',
    '08-70-00' => 'Tone mem i57 (table 3, 246 bytes)',
    '08-72-00' => 'Tone mem i58 (table 3, 246 bytes)',
    '08-74-00' => 'Tone mem i59 (table 3, 246 bytes)',
    '08-76-00' => 'Tone mem i60 (table 3, 246 bytes)',
    '08-78-00' => 'Tone mem i61 (table 3, 246 bytes)',
    '08-7A-00' => 'Tone mem i62 (table 3, 246 bytes)',
    '08-7C-00' => 'Tone mem i63 (table 3, 246 bytes)',
    '08-7E-00' => 'Tone mem i64 (table 3, 246 bytes)',
    '09-00-00' => 'Setup mem #24-#87 (64 x table 2, 256 bytes)',
    '09-02-00' => 'Setup mem #88-#108 (21 x table 2, 84 bytes)',
    '0D-00-00' => 'Patch mem A11-... (42.6 x table 5, 256 bytes)',
    '0D-02-00' => 'Patch mem ...-... (42.6 x table 5, 256 bytes)',
    '0D-04-00' => 'Patch mem ...-B88 (42.6 x table 5, 256 bytes)',
    '10-00-00' => 'System area (table 7, 50 bytes)',
);

my $filename = shift;
open (MIDI, $filename) || die "can't read $filename: $!";
binmode MIDI;

my $byte;
while (read MIDI, $byte, 1) {
    if ($byte eq "\xF0") {
        printf "%02X Sysex\n", ord $byte;
        {
            my $temp = 7;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            printf "%02X Manufacturer ID\n", ord $byte;
            $temp += ord $byte;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            printf "%02X Device ID\n", ord $byte;
            $temp += ord $byte;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            printf "%02X Model ID\n", ord $byte;
            $temp += ord $byte;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            printf "%02X Command\n", ord $byte;
            $temp += ord $byte;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            my $addr1 = ord $byte;
            $temp += ord $byte;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            my $addr2 = ord $byte;
            $temp += ord $byte;
            read MIDI, $byte, 1 or die "unexpected EOF";
            last if $byte eq "\xF7";
            my $addr3 = ord $byte;
            $temp += ord $byte;
            my $addr = sprintf "%02X-%02X-%02X", $addr1, $addr2, $addr3;
            printf "%s Address => %s\n", $addr, $addrmap{$addr} || 'unknown';
            my @data;
            while (1) {
                read MIDI, $byte, 1 or die "unexpected EOF";
                last if $byte eq "\xF7";
                push @data, ord $byte;
            }
            for (@data) {$temp += $_}
            $temp %= 128;
            my $sum = pop @data;
            printf "   %d bytes of data\n", scalar @data;
            printf "   *** checksum error ***\n" if $temp;
        }
        printf "%02X End of Sysex\n", ord $byte;
    } else {
        printf "%02X *** unknown command ***\n", ord $byte;
    }
}
print "EOF\n";
