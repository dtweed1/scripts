#!perl -w

# mididump.pl - dump an existing MIDI file to see its structure

# History:
#   2003/03/10 DT   Start.

use MIDI;

my $filename = shift;

my $opus = MIDI::Opus->new ({from_file => $filename});

print "Format: ", $opus->format, "\n";
print "Ticks: ", $opus->ticks, "\n";
printf "%d tracks\n", scalar $opus->tracks;

for ($opus->tracks) {
    printf "----- Track [%s] -----\n", $_->type;
    for ($_->events) {
        print "@$_";
        my $dummy = <STDIN>;
    }
}
