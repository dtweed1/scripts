package Bom::Utility;

# Miscellaneous functions required by most scripts that manipulate BOMs

use strict;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION = 1.00;
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = qw(build_database flatten_descriptions quantity sort_refs);
    %EXPORT_TAGS = ();
}

use lib $ENV{'PERLPATH'};
use Bom::Database;

require 'prefix.pl';

sub build_database {
    # Define the database (note that VPN is two key fields)
    my $db = Bom::Database->new (
        # Current Revision (source, VPN, rev, title)
        A => '-**--',

        # Bill of Materials (source, VPN, VPN, quan. or ref.)
        # TBD: If last field is quan., VPN-VPN must be unique (use two tables?)
        B => '-*****',

        # Approved Vendor List (source, VPN, VPN)
        C => '-****',

        # Documentation (source, VPN, doctype, rev., sheet, data)
        D => '-*****-',

        # BLOB (source, handle, format, path, attr., data)
        E => '-*----',

        # Inventory (source, VPN, quan. or serial)
        F => '-**-',

        # Price (source, VPN, quan., price, weight, lead)
        G => '-***-..',

        # Contact Information (vendor, information)
        H => '*-',
    );

    # Load the database from one or more external files
    for (@_) {
        $db->load_file ($_);
    }

    return $db;
}

# Flatten the descriptions in $db->{'A'} by sorting the keys and matching up
# prefixes. Populate the global %describe hash.
sub flatten_descriptions {
    my ($db) = @_;
    my (@stack, $desc, $key, $part, %tableA_key);
    my %describe;

    # Generate a list of the VPNs in table A.
    my @rows = $db->{'A'}->query;
    foreach my $rrow (@rows) {
        my $key = join ($;, @$rrow[1, 2]);
        $tableA_key{$key} = $$rrow[4];
    }

    # Build a stack of prefixes and descriptions, assuming that they sort
    # from more general to more specific. For each new key, look at the
    # last item on the stack; if it is a prefix of the current key, add the
    # current key to the stack. Otherwise, pop keys and generate concatenated
    # descriptions as we go.
    foreach $part (sort keys %tableA_key) {
        $desc = $tableA_key{$part};
        $desc =~ s/\s*\([^(]+\)\s*//g;
        while (@stack && !&prefix (${$stack[0]}[0], $part)) {
            # set description for last item on stack to be the
            # concatenated strings on the stack
            $key = ${$stack[0]}[0];
            $describe{$key} = join (' ', map ($$_[1], @stack));
            # pop stack
            shift @stack;
        }
        # push $_ and its description on the stack
        unshift (@stack, [$part, $desc]) if $desc;
    }

    return \%describe;
}

# Given a reference to an array list of part references, count one for each
# item that begins with a letter, plus add in any bare numbers.
sub quantity {
    my ($ref_partrefs) = shift;
    my ($quan) = 0;

    foreach my $partref (@$ref_partrefs) {
        if ($partref =~ /^[A-Za-z]/) {
            ++$quan;
        } else {
            $quan += $partref;
        }
    }
    $quan;
}

# Sort a list of reference designators, prefix first, then numeric part.
sub sort_refs {
    map {$_->[0]}
        sort {$a->[1] cmp $b->[1] || $a->[2] <=> $b->[2]}
            map {[$_, /^([[:alpha:]]+)/, /(\d+)$/]} @_;
}

1;
