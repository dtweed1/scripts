# Bom::Table.pm

# A Table consists of a set of rows (or records), each containing one set of
# fields. The object has contains the following items:
# $self->{'RAWDATA'}    a reference to an array that contains references to
#                       more arrays that contain the fields
#      ->{'INDEX'}      a reference to an array indexed by column number.
#                       Each element of that array is a reference to a hash
#                       indexed by field value.
#                       Each element of that hash is a reference to an
#                       array that is a list of row numbers containing that
#                       value in that column.
#      ->{'KEYLIST'}    a reference to a list of key field numbers
#      ->{'NONKEYLIST'} a reference to a list of non-key field numbers
#      ->{'REQLIST'}    a reference to a list of required field numbers

# To do:
#   Test the delete() method.
#   At table creation, verify that there is at least one required key field.
#   Do we need to keep the inverted index row numbers in sorted order?

# History:
#   2000/08/21 DT   Use splice() instead of shift(), pop() and slices.
#   2000/08/12 DT   Add delete() method.
#   2000/08/10 DT   Optimize query() to use the inverted index on key fields.
#   2000/08/09 DT   Re-do comments, continue development.
#   1999/04/06 DT   Start.

package Bom::Table;
use strict;

# Constructor
# Takes a parameter indicating which fields must be unique or at least present.

sub new {
    my $proto = shift;
    my $class = ref ($proto) || $proto;
    my $self = {};
    &template ($self, shift);
    bless $self, $class;
    return $self;
}

# Set or get the field template for this table.

sub template {
    my $self = shift;

    if (@_) {
        my $template = shift;
        my @flags = split ('', $template);

        undef $self->{'KEYLIST'};
        undef $self->{'NONKEYLIST'};
        undef $self->{'REQLIST'};
        for (0..$#flags) {
            if ($flags[$_] eq '*') {
                push (@{$self->{'KEYLIST'}}, $_);
                push (@{$self->{'REQLIST'}}, $_);
            } else {
                push (@{$self->{'NONKEYLIST'}}, $_);
                push (@{$self->{'REQLIST'}}, $_) if $flags[$_] ne '.';
            }
        }
    }

    # regenerate the template string from the lists
    my @flags;
    for (@{$self->{'NONKEYLIST'}}) {$flags[$_] = '.'};
    for (@{$self->{'REQLIST'}}) {$flags[$_] = '-'};
    for (@{$self->{'KEYLIST'}}) {$flags[$_] = '*'};
    return join ('', @flags);
}

sub update {
    my ($self, @fields) = @_;

    # Make sure all the required field values have been specified.
    for (@{$self->{'REQLIST'}}) {
        return undef unless $fields[$_];
    }

    # First, determine whether an existing row contains all the same key
    # field values.
    if (defined (my $i = &match_row ($self, @fields))) {
        # warn "updating existing row\n";
        $self->{'RAWDATA'}[$i] = [@fields];
    } else {
        # warn "creating new row\n";
        # Create a new row to hold the data
        push (@{$self->{'RAWDATA'}}, [@fields]);

        # Add the new row number to the inverted indices for the key fields.
        my $newrow = $#{$self->{'RAWDATA'}};
        for (@{$self->{'KEYLIST'}}) {
            push (@{$self->{'INDEX'}[$_]{$fields[$_]}}, $newrow);
        }
    }
    return 1;
}

sub query {
    my ($self, @fields) = @_;

    # Start by using the inverted indices on the key fields.
    my @rows = &match_rows ($self, @fields);
    return unless @rows;

    # If any non-key fields have been specified,
    # weed out rows that don't match in those fields.
    foreach my $f (@{$self->{'NONKEYLIST'}}) {
        next unless $fields[$f];

        @rows = grep ($self->{'RAWDATA'}[$_][$f] =~ /$fields[$f]/, @rows);
    }

    # Return an array of references to the actual data
    return @{$self->{'RAWDATA'}}[@rows];
}

sub delete {
    my ($self, @fields) = @_;
    my $i;

    # Make sure all the required field values have been specified.
    for (@{$self->{'REQLIST'}}) {
        return undef unless $fields[$_];
    }

    # First, determine whether an existing row contains all the same key
    # field values.
    return undef unless defined ($i = &match_row ($self, @fields));

    # Delete the row number from all the indices.
    for (@{$self->{'REQLIST'}}) {
        # Find where in the index array the row number appears.
        my $ra = $self->{'INDEX'}[$_]{$fields[$_]};
        my $j = 0;
        while ($ra->[$j] != $i) {++$j};
        # create a new version that leaves that position out
        splice @$ra, $j, 1;
    }

    # Then delete the row itself.
    my $ra = $self->{'RAWDATA'};
    splice @$ra, $i, 1;

    return 1;
}

sub dump {
    my $self = shift;
    foreach my $k (sort keys %$self) {
        warn "$k: $$self{$k}\n";
        if ($k eq 'INDEX') {
            warn "   (\n";
            foreach my $rcol (@{$self->{$k}}) {
                warn "      (\n";
                foreach my $key (sort keys %$rcol) {
                    warn "         $key: (", join (', ', @{$rcol->{$key}}), ")\n";
                }
                warn "      )\n";
            }
            warn "   )\n";
        } elsif ($k eq 'KEYLIST') {
            warn "   (", join (', ', @{$self->{$k}}), ")\n";
        } elsif ($k eq 'NONKEYLIST') {
            warn "   (", join (', ', @{$self->{$k}}), ")\n";
        } elsif ($k eq 'REQLIST') {
            warn "   (", join (', ', @{$self->{$k}}), ")\n";
        } elsif ($k eq 'RAWDATA') {
            warn "   (\n";
            foreach my $rrow (@{$self->{$k}}) {
                warn "      (", join (', ', @$rrow), ")\n";
            }
            warn "   )\n";
        }
    }
}

#-----------------------------------------------------------------------------
# Private Functions
#

# Returns the index of an existing row or undef

sub match_row {
    my ($self, @fields) = @_;
    my %set1;

    # For each key field, get the list of row numbers that contain the
    # specified value for that field. For the first key field, we use the row
    # numbers as keys to %set1. For any subsequent key fields, we put the row
    # numbers into %set2 and delete any keys from %set1 that don't appear in
    # %set2. There will be at most one row number that appears in all
    # of the sets. If any set comes up empty, we can return immediately.
    my @list = @{$self->{'KEYLIST'}};
    my $i = shift @list;
    my $rset = $self->{'INDEX'}[$i]{$fields[$i]};
    return undef unless $rset;

    map {$set1{$_} = 1} @$rset;

    for (@list) {
        my $rset = $self->{'INDEX'}[$_]{$fields[$_]};
        return undef unless $rset;

        my %set2;
        map ($set2{$_} = 1, @$rset);
        foreach my $key (keys %set1) {
            delete $set1{$key} unless $set2{$key};
        }
        # quit if %set1 is now empty
        return undef unless %set1;
    }
    # %set1 should now contain just one key (TBD: verify?)
    return (keys %set1)[0];
}

# Returns a set of row numbers that contain the specified key field values.
# Differences between this and match_row():
#   - not all key fields need to be specified,
#   - uses regular expressions to select key values,
#   - returns as many rows as possible.
# The results will need additional filtering on non-key fields, if specified
# in the query.

sub match_rows {
    my ($self, @fields) = @_;
    my %set1;

    # If no key fields have been specified, return a list of all the row
    # numbers.
    my @list = grep ($fields[$_], @{$self->{'KEYLIST'}});
    return (0..$#{$self->{'RAWDATA'}}) unless @list;

    # For each key field, get the list of row numbers that contain the
    # specified value(s) for that field. For the first key field, we use the
    # row numbers as keys to %set1. For any subsequent key fields, we put the
    # row numbers into %set2 and delete any keys from %set1 that don't appear
    # in %set2. There will be at most one row number that appears in all
    # of the sets. If any set comes up empty, we can return immediately.
    my $i = shift @list;
    my @set = &matching_rowset ($self->{'INDEX'}[$i], $fields[$i]);
    map {$set1{$_} = 1} @set;

    for (@list) {
        next unless $fields[$_];

        my @set = &matching_rowset ($self->{'INDEX'}[$_], $fields[$_]);
        return unless @set;

        my %set2;
        map {$set2{$_} = 1} @set;
        foreach my $key (keys %set1) {
            delete $set1{$key} unless $set2{$key};
        }
        # quit if %set1 is now empty
        return unless %set1;
    }
    return keys %set1;
}

sub matching_rowset {
    my ($rhash, $pattern) = @_;
    my @keyvals;
    foreach my $key (keys %$rhash) {
        push @keyvals, $key if $key =~ /$pattern/;
    }
    # return a merged list of all the rows for all the key values
    # (in no particular order)
    return map @{$rhash->{$_}}, @keyvals;
}

1;

