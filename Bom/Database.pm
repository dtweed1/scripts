# Database.pm

# A database consists of a set of related Tables that can be written out to
# a text file and loaded again. The database can also keep track of updates
# to source documents and incrementally update itself when these change.

# To do:
#   Decide whether/how to enforce "source document" field in each Table.

# History:
#   2000/08/12 DT   Start.

package Bom::Database;
use Bom::Table;
use strict;

# Constructor
# Takes a hash parameter that gives the table names and templates

sub new {
    my $proto = shift;
    my $class = ref ($proto) || $proto;
    my $self = {};
    my %arg  = @_; 

    foreach (keys %arg) {
        $self->{$_} = Bom::Table->new ($arg{$_});
    }

    bless $self, $class;
    return $self;
}

# The syntax for the input file is to have one record per line. Blank
# lines and anything following a '#' are ignored. Fields are separated
# by whitespace and a field may consist of
#   a series of non-whitespace characters,
#   a "-quoted string of ASCII characters, or
#   a '-quoted base-85 string of binary data
sub load_file {
    my ($self, $filename) = @_;

    open (IN, $filename) || die "can't open $filename: $!\n";
  LINE:
    while (<IN>) {
        my @fields;

        chomp;
        # trim off any leading and trailing whitespace
        s/^\s+//;
        s/\s+$//;
      FIELD:
        while (defined $_ && length) {
            my $f;

            # is the rest of the line a comment?
            last if /^#/;
            # is the next field quoted?
            if (/^(["'])/) { # " # help emacs
                my $x = index ($_, $1, 1);
                if ($x<1) {
                    warn "unmatched '\n";
                    $f = substr ($_, 1);
                    $_ = '';
                } else {
                    $f = substr ($_, 1, $x-1);
                    ($_ = substr ($_, $x+1)) =~ s/^\s+//;
                }
                # TBD: do the decoding based on $1
                push (@fields, $f);
                next FIELD;
            }
            # otherwise, whitespace-delimited field
            ($f, $_) = split (' ', $_, 2);
            push (@fields, $f);
        }
        next LINE unless @fields;

        # for testing, dump the fields out again, quoting as necessary
        if (0) {
            foreach (@fields) {
                if (/\s/) {
                    print "\"$_\" ";
                } else {
                    print "$_ ";
                }
            }
            print "\n";
        }

        # put the fields into the table named by the first field
        # replace the first field with the document number (filename for now)
        my $table;
        ($table = $fields[0]) =~ s/:$//;
        if (defined $self->{$table}) {
            $fields[0] = $filename;
            $self->{$table}->update (@fields)
                || warn "udpate failed - $table: (", join (', ', @fields), ")\n";
        } else {
            warn "table $table not defined\n";
        }
    }
    close IN;
}

sub write_file {
    my ($self, $filename) = @_;

    open (OUT, ">$filename") || die "can't open $filename: $!\n";
    foreach my $t (sort keys %$self) {
        my @rows = $self->{$t}->query;
        foreach my $rr (@rows) {
            # Replace the first field (source document) with the table name
            # followed by a colon.
            print OUT $t, ': ';
            for (@$rr[1..$#$rr]) {
                if (/\s/) {
                    print OUT "\"$_\" ";
                } else {
                    print OUT "$_ ";
                }
            }
            print OUT "\n";
        }
    }
    close OUT;
}

1;
