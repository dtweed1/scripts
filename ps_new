#!/usr/bin/perl

# ps -- print ASCII to postscript printer

# PERL version of a2ps.awk -- handles multiple files not necessarily
# in the working directory or the current drive. Also replaces "ps2.bat",
# "ps2c.bat", "psx.bat".

# This script is a general ASCII-to-Postscript converter with two twists:
# 1. Any IBM PC box-drawing characters are converted to optimized line-
#    drawing commands (Postscript moveto/lineto pairs).
# 2. If any escape (\33) characters appear in the file, it is assumed to be
#    a printer file for an Epson-compatible dot-matrix printer generated
#    by a separate formatting program, and the escape sequences are
#    interpreted appropriately to select fonts and turn on and off bold
#    and italics.

# Usage:
#   ps [ <options> ] <file> ...
# Options:
#   -2      Print two logical pages per sheet of paper (scaled and rotated
#           90 degrees).
#   -b <n>  ("blanks") Set the tab width (default is 8 characters).
#   -c      Compress the output horizontally by 4/3 to accomodate 132-column
#           listings.
#   -d      ("debug") Suppress copying the output file to the printer and
#           deleting it, so the Postscript code can be checked for errors.
#           Also, send some debugging information to STDERR.
#   -h      ("header") Add a line at the beginning of the first page giving
#           the filename and its timestamp.
#   -l      ("landscape") Print in landscape orientation.
#   -p <printer>
#           Override the default printer (PS_PRINTER).
#   -q      ("quiet") Suppress progress information sent to STDERR.
#   -t      Truncate long lines (with '...') instead of wrapping to the
#           next line.
# Environment:
#   PS_PRINTER
#           Set this to a valid key (see %printer_setup below) for your
#           default printer.
#   TEMP
#           Set this to a directory that can be used for spooling.
# You may freely intermingle options and filenames on the command line;
# however, all options apply to all files.

# This was written back when I was using the DJGPP port of Perl 4, so it
# relies on Perl to do any wildcard expansion of the command line. Other
# than that, it works fine with Perl 5.

# To do:
#       Clean up the font-selection code (and make more general).
#       (?) Re-select the current font on each postscript page.
#       Handle "bare" carriage returns (overstrike, as in output from 'bs').
#       We may want to disable line-wrap for Epson files.

# History:
#   2004/05/12 DT   Adjust top and bottom margins.
#   2004/03/19 DT   Add support for gsprint.
#   2003/01/02 DT   Use Date::Zeller instead of zeller.pl.
#   2002/11/04 DT   Add '-l' switch. Fix calculation of line length.
#   2001/06/22 DT   Add '-h' switch.
#   1999/09/15 DT   Convert ps.bat to ps.pl to use with bash.
#                   Eliminate hard tabs.
#   1999/04/14 DT   Do some documentation and minor clean-ups for publication.
#                   Add '-p' and '-q' switches.
#   1997/12/30 DT   Disable login check; not needed at Aris.
#                   Add '\\heart\hp' printer for Aris to work around
#                   strange behavior of 'copy' command.
#   1996/11/19 DT   Add ability to expand tabs to other than 8 columns.
#                   Use "-b<n>" switch.
#   1996/05/17 DT   Fix bug that chops last character of file even if not '\n'.
#                   Fix bug that prevents page breaks if no spaces in line.
#   1996/04/05 DT   Minor tweak so that the $printer mechanism also
#                   supports using A: or G: at home. Assumes LPT2:
#                   at work.
#                   Also, skip the check for the network if a disk
#                   drive is selected.
#   1996/03/20 DT   Add proper support for $printer variable; use
#                   'lpt2:' by default, but ENV{'PS_PRINTER'} will
#                   override.
#                   Add $debugging flag (-d).
#   1996/03/19 DT   Add $printer variable.
#   1996/03/11 DT   Add command to delete temporary files.
#                   Use TEMP environment variable to define directory.
#   1996/02/16 DT   Add line to select lower paper tray on Lexmark
#                   printer (see %%BeginSetup)
#   1995/09/21 DT   Add check for network accessibility.
#   1995/08/02 DT   Use '' for literal strings wherever possible,
#                   for performance.
#   1995/07/21 DT   Add control-D at beginning and end of file.
#                   Delete control-D from input file.
#   1995/07/20 DT   Fix "pi" constants.
#   1995/07/19 DT   Add "-t" option to truncate long lines.
#   1995/07/18 DT   Finish converting a2ps from awk to perl.
#                   Optimize the line drawing commands.
#                   Add Adobe 1.0 comments.
#                   Use a local dictionary.
#   1995/07/17 DT   Begin converting a2ps from awk to perl.
#   1995/03/29 DT   Convert to PERL.

use lib $ENV{'PERLPATH'};
use Date::Zeller qw(print_local_time);

# Check to see whether we're logged in. If PS_PRINTER evaluates to one
# or two characters, assume we're writing to a removable disk and don't
# need the network.
# die "Not logged in!\n"
#    unless (length ($ENV{'PS_PRINTER'}) < 3 || -e 'r:/users/dave');

# Printer-specific setup (goes in %%BeginSetup section)
%printer_setup = (

    # network printer at Aris
    '\\\\heart\\hp', '',

    # HP DeskJet 500 at home, via Ghostscript on Uvula (Win95)
    'djet500', '',

    # HP DeskJet 500 at home, via Ghostscript on Win2K
    'djet500a', '',

    # slow HP printer at Intraplex
    'lpt1:', '',

    # fast Lexmark printer at Intraplex
    'lpt2:',
        # select plain paper in lower tray
        'statusdict begin 1 setpapertray end',

    # spool to floppy for Lexmark
    'a:',
        # select plain paper in lower tray
        'statusdict begin 1 setpapertray end',

    # spool to zip disk for Lexmark
    'g:',
        # select plain paper in lower tray
        'statusdict begin 1 setpapertray end',

    # gsprint to default Windows printer
    'gsprint', '',
);

# Default values
$tabsize = 8;

# Use 'gsprint' by default, but if PS_PRINTER is set to a
# supported value, use it instead.
$printer = 'gsprint';
$printer = $ENV{'PS_PRINTER'} if defined ($printer_setup{$ENV{'PS_PRINTER'}});

# Process switches
for (@ARGV) {
    if ($get_tab) {
        die "tab size must be a number\n" unless /^[0-9]+$/;
        $tabsize = $_;
        $get_tab = 0;
    } elsif ($get_printer) {
        die "unknown printer $_\n" unless defined ($printer_setup{$_});
        $printer = $_;
        $get_printer = 0;
    } elsif (/^-/) {
        foreach $c (split (/ */, substr ($_, 1))) {
            if ($c eq '2') {$two_up++;}
            if ($c eq 'b') {$get_tab++;}
            if ($c eq 'c') {$condensed++;}
            if ($c eq 'd') {$debugging++;}
            if ($c eq 'h') {$timestamp++;}
            if ($c eq 'l') {$landscape++;}
            if ($c eq 'p') {$get_printer++;}
            if ($c eq 'q') {$quiet++;}
            if ($c eq 't') {$truncate++;}
        }
    } else {
        if ((-e $_) && !(-d _)) {
            push (@files, $_);
        }
    }
}

warn "printer is $printer\n" if $debugging;

if ($condensed) {
    $compress = 1.33;
}

for (@files) {
    # split off the actual filename
    @temp = split (/[:\/\\]/);
    $name = $temp[$#temp];
    &a2ps ($_, $ENV{'TEMP'}.'/'.$name);
}

# This function transforms one source file (ASCII text with optional control
# codes) into one destination file (Postcript source representing multiple
# pages)

sub a2ps {
    local ($srcfile, $destfile) = @_;

    print STDERR "$srcfile -> $destfile " unless $quiet;
    open (IN, $srcfile) || do {
        warn "$0: can't open $srcfile: $!\n";
        return;
    };
    open (OUT, ">$destfile") || do {
        warn "$0: can't open $destfile: $!\n";
        close IN;
        return;
    };

    if ($landscape) {
        $pagewidth = 792;
        $pageheight = 612;
    } else {
        $pagewidth = 612;
        $pageheight = 792;
    }
    # These margins are for the individual page images, even when being
    # printed 2-up.
    # TBD: We need a different set of margins that describe how close the
    # printer can go to the edges of the paper.
    $leftmargin = 36;
    $rightmargin = 36;
    $workwidth = $pagewidth - $leftmargin - $rightmargin;
    $topmargin = 20;
    $bottommargin = 20;
    $linespacing = 9;
    $page_state = $two_up ? 2 : 0;
    $ps_pagenum = 0;
    undef %pagefonts;
    undef %docfonts;

    print OUT <<"!";
\004%!PS-Adobe-1.0
%%Title: $destfile
%%For: Dave Tweed
%%Creator: ps.bat
%%CreationDate: <unknown>
%%Pages: (atend)
%%DocumentFonts: (atend)
%%BoundingBox: 0 0 612 792
%%EndComments
%%BeginResource: procset BoxcharDict 3 1
/BoxcharDict 5 dict def
BoxcharDict begin
/VL {
   2 copy moveto
   pop exch lineto stroke
} def
/HL {
   2 copy exch moveto
   pop lineto stroke
} def
/initial_CTM matrix currentmatrix def
/left_page {
   initial_CTM setmatrix
   0.3 setlinewidth
   0 396 moveto 612 396 lineto stroke
   54 792 translate
   -90 rotate
   0.64 0.64 scale
} def
/right_page {
   initial_CTM setmatrix
   54 396 translate
   -90 rotate
   0.64 0.64 scale
} def
end
%%EndResource
/SVDoc save def
%%EndProlog
%%BeginSetup
$printer_setup{$printer}
BoxcharDict begin
%%EndSetup
!

    &use_font ('Courier');
    if ($compress) {
        print OUT '[' . 9/$compress . " 0 0 9 0 0] makefont setfont\n\n";
        $cpi = 40*$compress/3;
    } else {
        print OUT "9 scalefont setfont\n\n";
        $cpi = 40/3;    # 13.33 cpi
    }
    $cpl = int ($cpi * $workwidth / 72 + 0.5);

    &new_page;

    &show_timestamp ($srcfile) if $timestamp;

    while (<IN>) {
        chop if /\n$/;

        # split lines at formfeeds
        while (/\f/) {
            $_ = $';
            &printlongline ($`);
            &new_page;
        }
        &printlongline ($_);
    }

    if ($page_dirty) {
        &dump_segments;
        &page_trailer;
    }

    $docfonts = join (' ', sort keys %docfonts);
    print OUT <<"!";
%%Trailer
SVDoc restore
end
%%Pages: $ps_pagenum
%%DocumentFonts: $docfonts
%%EOF
!

    # Generate ^D with no trailing CR/LF
    print OUT "\004";

    close IN;
    close OUT;
    print STDERR "\n" unless $quiet;
    if (!$debugging) {

        # If the $printer is 'djet500', then we want to run the file
        # through ghostscript before sending it to the printer.
        # One suggested command line is:
        # '/usr/bin/gs -sOutputFile=$file.png -dNOPAUSE -dBATCH -r$scale -sDEVICE=djet500 $file.ps'

        # Another possiblity appears in 'gsdj500.bat' in the original
        # distribution, which is designed to start with plain text:
        # @gs -q -sDEVICE#djet500 -r300 -dNOPAUSE -sPROGNAME=gsdj500 -- gslp.ps %1 %2 %3 %4 %5 %6 %7 %8 %9
        # I wonder if the '=' should be '#' (a hack to avoid the problem
        # that command.com strips '=' from the command line).

        # How about:
        # /gstools/gs5.10/gswin32 -I/gstools/gs5.10;/gstools/gs5.10/fonts -sDEVICE=djet500 -r300 -dNOPAUSE $destfile

        if ($printer eq 'djet500') {
            # on Uvula
            $gspath = 'c:/gstools/gs5.10';
            $gscmd = "$gspath/gswin32";
            $gscmd =~ s/\//\\/g;
            $cmd = "$gscmd -I$gspath;$gspath/fonts -sDEVICE=djet500 -r300 -dNOPAUSE -dBATCH $destfile";
        } elsif ($printer eq 'djet500a') {
            # on Win2K
            $gspath = 'c:/Aladdin';
            $gscmd = "$gspath/gs6.01/bin/gswin32";
            $gscmd =~ s/\//\\/g;
            $cmd = "$gscmd -I$gspath/gs6.01/lib;$gspath/fonts -sDEVICE=djet500 -r300 -dNOPAUSE -dBATCH $destfile";
        } elsif ($printer eq 'gsprint') {
            # Use gsprint to send the file to the default Windows printer
            $gspath = '/cygdrive/c/apps/Ghostgum/gsview';
            $gscmd = "$gspath/gsprint";
            # Under cygwin, we don't want the backslashes in the paths, but
            # gsprint doesn't understand the "/cygdrive/X/" convention, so
            # we change it to "X:/" instead.
            # $gscmd =~ s/\//\\/g;
            my $tempname = $destfile;
            $tempname =~ s#/cygdrive/([a-z])/#$1:/#;
            $cmd = "$gscmd $tempname";
        } else {
            $cmd = "copy $destfile $printer";
        }
        warn "Executing: $cmd\n" unless $quiet;
        system ($cmd);
        unlink $destfile || warn "$0: can't delete $destfile: $!\n";
    }
}

# This function handles one full line of source text, no matter how long it is.
# The line is either wrapped to multiple output lines, or truncated to fit onto
# one output line.

sub printlongline {
    local ($buffer) = @_;

    # expand tabs
    while ($buffer =~ /\t/) {
        local($len) = $tabsize - (length ($`) % $tabsize);
        $buffer = $` . ' ' x $len . $';
    }

    # check for lines longer than page width
    if (length ($buffer) > $cpl) {
        if ($truncate) {
            # truncate long lines, with ellipsis
            substr ($buffer, $cpl-3) = '...';
        } else {
            # wrap long lines
            while (length ($buffer) > $cpl) {
                &printline (substr ($buffer, 0, $cpl));
                $buffer = substr ($buffer, $cpl);
            }
        }
    }
    &printline ($buffer);
}

# This function renders one output line as a Postscript source code fragment,
# handling the positioning of the line, escaping necessary characters. Also,
# if there are font changes within the line, it is broken into fragments as
# needed.

sub printline {
    local ($buffer) = @_;

    # Temporary hack to take care of the case when formfeed occurs just
    # after an automatic showpage. Basically, we suppress the automatic
    # new_page if the current buffer is all whitespace.
    if ($buffer =~ /\S/ && $ypos < $bottommargin) {
        &new_page;
    }

    $page_dirty = 1;
    if ($buffer =~ /[\x80-\xFF]/) {
        $buffer = &do_boxchars ($buffer, $xpos, $ypos);
    }
    if ($buffer) {
        # add escapes for backslash and parentheses
        $buffer =~ s/\\/\\\\/g;
        $buffer =~ s/\(/\\(/g;
        $buffer =~ s/\)/\\)/g;
        
        # delete any control-D characters
        $buffer =~ s/\004//g;

        # print the line
        printf OUT ("%.6g %.6g moveto\n", $xpos, $ypos);
        while ($buffer =~ /\033/) {
            # print everything up to the escape
            if ($`) {
                printf OUT ("(%s) show\n", $`);
            }
            # make the requested change and process the remainder of the line
            $buffer = &select_font ($');
        }
        if ($buffer) {
            printf OUT ("(%s) show\n\n", $buffer);
        }
    }

    # advance the vertical position and print page if full
    $ypos -= $linespacing;
}

# Handle the switch to the next (first) page image, updating both our internal
# state and outputting any Postscript code, including buffered commands for
# the previous page and the header for the new page.

sub new_page {

    print STDERR '.' unless $quiet;
    $xpos = $leftmargin;
    $ypos = $pageheight - $topmargin - $linespacing;
    if ($page_state == 0) {
        $page_dirty = 0;
        $page_state = 1;
        &page_header;
        print OUT "612 0 translate 90 rotate\n" if $landscape;
    } elsif ($page_state == 1) {
        &dump_segments;
        &page_trailer;
        $page_dirty = 0;
        &page_header;
        print OUT "initial_CTM setmatrix 612 0 translate 90 rotate\n"
            if $landscape;
    } elsif ($page_state == 2) {
        &page_header;
        print OUT "left_page\n";
        print OUT "612 0 translate 90 rotate\n" if $landscape;
        $page_dirty = 1;
        $page_state = 3;
    } elsif ($page_state == 3) {
        &dump_segments;
        print OUT "right_page\n";
        print OUT "612 0 translate 90 rotate\n" if $landscape;
        $page_state = 4;
    } else {
        &dump_segments;
        &page_trailer;
        &page_header;
        print OUT "left_page\n";
        print OUT "612 0 translate 90 rotate\n" if $landscape;
        $page_dirty = 1;
        $page_state = 3;
    }
}

# This function is called with the portion of a line following an ESC
# character. It finds, processes and removes the rest of the escape sequence
# and returns whatever else is left on the line following that.

sub select_font {
    local ($buff) = @_;

    if ($buff =~ /^G/) {        # Bold start
        $bold = 1;
        &use_font ($italics ? 'Courier-BoldOblique' : 'Courier-Bold');
        print OUT "10 scalefont setfont\n";
    } elsif ($buff =~ /^H/) {   # Bold end
        $bold = 0;
        &use_font ($italics ? 'Courier-Oblique' : 'Courier');
        print OUT "10 scalefont setfont\n";
    } elsif ($buff =~ /^4/) {   # Italics start
        $italics = 1;
        &use_font ($bold ? 'Courier-BoldOblique' : 'Courier-Oblique');
        print OUT "10 scalefont setfont\n";
    } elsif ($buff =~ /^5/) {   # Italics end
        $italics = 0;
        &use_font ($bold ? 'Courier-Bold' : 'Courier');
        print OUT "10 scalefont setfont\n";
    } elsif ($buff =~ /^!/) {   # Epson printer mode, switch to 6 LPI
        $linespacing = 12 * 63 / 64;    #  1/6" compressed slightly
        $topmargin = 0;
#       $bottommargin = 0;
        $leftmargin = 0;
        $rightmargin = 0;

        $xpos = $leftmargin;
        if (!$page_dirty) {
            $ypos = $pageheight - $topmargin - $linespacing;
        }

        if ($buff =~ /^!\001/) {        # Elite (12 CPI)
            &use_font ('Courier');
            print OUT "10 scalefont setfont\n";
            $cpl = 100;                 # 12 cpi * 8.5 inches
            $cpi = 12;
        } elsif ($buff =~ /^!\004/) {   # condensed (17 CPI)
            &use_font ('Courier');
            print OUT "[7 0 0 10 0 0] makefont setfont\n";
            $cpl = 145;                 # 17 cpi * 8.5 inches
            $cpi = 17;
        } elsif ($buff =~ /^!\010/) {   # emphasized Pica (10 CPI)
            &use_font ('Courier');
            print OUT "[12 0 0 10 0 0] makefont setfont\n";
            $cpl = 85;                  # 10 cpi * 8.5 inches
            $cpi = 10;
        } elsif ($buff =~ /^!\021/) {   # doublestrike Elite (12 CPI)
            &use_font ('Courier-Bold');
            print OUT "10 scalefont setfont\n";
            $cpl = 100;                 # 12 cpi * 8.5 inches
            $cpi = 12;
        } elsif ($buff =~ /^!\030/) {   # doublestrike emphasized Pica (10 CPI)
            &use_font ('Courier-Bold');
            print OUT "[12 0 0 10 0 0] makefont setfont\n";
            $cpl = 85;                  # 10 cpi * 8.5 inches
            $cpi = 10;
        } else {
            # ignore for now
        }
    } else {
        # ignore for now
    }

    # Return everything after the last pattern matched
    $';
}

# Draw the equivalents of the IBM PC box characters.
# The line-drawing optimizations rely on the fact that the box characters are
# added left-to-right, top-to-bottom. This means that the horizontal lines are
# extended only to the right and the vertical lines are extended only toward
# the bottom. If a segment being added in the current character does not extend
# all the way to the right or to the bottom, then the command is printed and
# the segment is removed from the buffer.

sub do_boxchars {
    local ($b, $x, $y) = @_;

    while ($b =~ s/([\x80-\xFF])/ /) {
        $ch = $1;

        # precalculate the coordinates
        $h = 72/$cpi;
        $h0 = $x + length($`)*$h;
        $h1 = $h0 + 1*$h/3;
        $h2 = $h0 + $h/2;
        $h3 = $h0 + 2*$h/3;
        $h4 = $h0 + $h;
        $v0 = $y - $linespacing/6;
        $v1 = $v0 + 3*$linespacing/8;
        $v2 = $v0 + 5*$linespacing/8;
        $v3 = $v0 + $linespacing;

        # draw the line segments
        # Note that horizontal segments are drawn left-to-right,
        # vertical segments from bottom-to-top.
        if ($ch eq "\xB3") {
            &vsegment ($h2, $v0, $v3);
        } elsif ($ch eq "\xB4") {
            &vsegment ($h2, $v0, $v3);
            &hsegment_term ($h0, $h2, $v1);
        } elsif ($ch eq "\xB5") {
            &vsegment ($h2, $v0, $v3);
            &hsegment_term ($h0, $h2, $v1);
            &hsegment_term ($h0, $h2, $v2);
        } elsif ($ch eq "\xB6") {
            &vsegment ($h1, $v0, $v3);
            &vsegment ($h3, $v0, $v3);
            &hsegment_term ($h0, $h1, $v1);
        } elsif ($ch eq "\xB7") {
            &vsegment ($h1, $v0, $v1);
            &vsegment ($h3, $v0, $v1);
            &hsegment_term ($h0, $h3, $v1);
        } elsif ($ch eq "\xB8") {
            &vsegment ($h2, $v0, $v2);
            &hsegment_term ($h0, $h2, $v1);
            &hsegment_term ($h0, $h2, $v2);
        } elsif ($ch eq "\xB9") {
            &vsegment ($h3, $v0, $v3);
            &vsegment_term ($h1, $v2, $v3);
            &vsegment ($h1, $v0, $v1);
            &hsegment_term ($h0, $h1, $v1);
            &hsegment_term ($h0, $h1, $v2);
        } elsif ($ch eq "\xBA") {
            &vsegment ($h1, $v0, $v3);
            &vsegment ($h3, $v0, $v3);
        } elsif ($ch eq "\xBB") {
            &vsegment ($h3, $v0, $v2);
            &vsegment ($h1, $v0, $v1);
            &hsegment_term ($h0, $h1, $v1);
            &hsegment_term ($h0, $h3, $v2);
        } elsif ($ch eq "\xBC") {
            &hsegment_term ($h0, $h3, $v1);
            &vsegment_term ($h3, $v1, $v3);
            &hsegment_term ($h0, $h1, $v2);
            &vsegment_term ($h1, $v2, $v3);
        } elsif ($ch eq "\xBD") {
            &hsegment_term ($h0, $h3, $v1);
            &vsegment_term ($h1, $v1, $v3);
            &vsegment_term ($h3, $v1, $v3);
        } elsif ($ch eq "\xBE") {
            &vsegment_term ($h2, $v1, $v3);
            &hsegment_term ($h0, $h2, $v1);
            &hsegment_term ($h0, $h2, $v2);
        } elsif ($ch eq "\xBF") {
            &hsegment_term ($h0, $h2, $v1);
            &vsegment ($h2, $v0, $v1);
        } elsif ($ch eq "\xC0") {
            &vsegment_term ($h2, $v1, $v3);
            &hsegment ($h2, $h4, $v1);
        } elsif ($ch eq "\xC1") {
            &hsegment ($h0, $h4, $v1);
            &vsegment_term ($h2, $v1, $v3);
        } elsif ($ch eq "\xC2") {
            &hsegment ($h0, $h4, $v1);
            &vsegment ($h2, $v0, $v1);
        } elsif ($ch eq "\xC3") {
            &vsegment ($h2, $v0, $v3);
            &hsegment ($h2, $h4, $v1);
        } elsif ($ch eq "\xC4") {
            &hsegment ($h0, $h4, $v1);
        } elsif ($ch eq "\xC5") {
            &hsegment ($h0, $h4, $v1);
            &vsegment ($h2, $v0, $v3);
        } elsif ($ch eq "\xC6") {
            &vsegment ($h2, $v0, $v3);
            &hsegment ($h2, $h4, $v1);
            &hsegment ($h2, $h4, $v2);
        } elsif ($ch eq "\xC7") {
            &vsegment ($h1, $v0, $v3);
            &vsegment ($h3, $v0, $v3);
            &hsegment ($h3, $h4, $v1);
        } elsif ($ch eq "\xC8") {
            &vsegment_term ($h1, $v1, $v3);
            &hsegment ($h1, $h4, $v1);
            &vsegment_term ($h3, $v2, $v3);
            &hsegment ($h3, $h4, $v2);
        } elsif ($ch eq "\xC9") {
            &vsegment ($h1, $v0, $v2);
            &hsegment ($h1, $h4, $v2);
            &vsegment ($h3, $v0, $v1);
            &hsegment ($h3, $h4, $v1);
        } elsif ($ch eq "\xCA") {
            &hsegment ($h0, $h4, $v1);
            &hsegment_term ($h0, $h1, $v2);
            &vsegment_term ($h1, $v2, $v3);
            &vsegment_term ($h3, $v2, $v3);
            &hsegment ($h3, $h4, $v2);
        } elsif ($ch eq "\xCB") {
            &hsegment ($h0, $h4, $v2);
            &hsegment_term ($h0, $h1, $v1);
            &vsegment ($h1, $v0, $v1);
            &vsegment ($h3, $v0, $v1);
            &hsegment ($h3, $h4, $v1);
        } elsif ($ch eq "\xCC") {
            &vsegment ($h1, $v0, $v3);
            &vsegment_term ($h3, $v2, $v3);
            &vsegment ($h3, $v0, $v1);
            &hsegment ($h3, $h4, $v1);
            &hsegment ($h3, $h4, $v2);
        } elsif ($ch eq "\xCD") {
            &hsegment ($h0, $h4, $v1);
            &hsegment ($h0, $h4, $v2);
        } elsif ($ch eq "\xCE") {
            &hsegment_term ($h0, $h1, $v1);
            &hsegment_term ($h0, $h1, $v2);
            &vsegment_term ($h1, $v2, $v3);
            &vsegment ($h1, $v0, $v1);
            &vsegment_term ($h3, $v2, $v3);
            &vsegment ($h3, $v0, $v1);
            &hsegment ($h3, $h4, $v1);
            &hsegment ($h3, $h4, $v2);
        } elsif ($ch eq "\xCF") {
            &hsegment ($h0, $h4, $v1);
            &hsegment ($h0, $h4, $v2);
            &vsegment_term ($h2, $v2, $v3);
        } elsif ($ch eq "\xD0") {
            &hsegment ($h0, $h4, $v1);
            &vsegment_term ($h1, $v1, $v3);
            &vsegment_term ($h3, $v1, $v3);
        } elsif ($ch eq "\xD1") {
            &hsegment ($h0, $h4, $v1);
            &hsegment ($h0, $h4, $v2);
            &vsegment ($h2, $v0, $v1);
        } elsif ($ch eq "\xD2") {
            &hsegment ($h0, $h4, $v1);
            &vsegment ($h1, $v0, $v1);
            &vsegment ($h3, $v0, $v1);
        } elsif ($ch eq "\xD3") {
            &hsegment ($h1, $h4, $v1);
            &vsegment_term ($h1, $v1, $v3);
            &vsegment_term ($h3, $v1, $v3);
        } elsif ($ch eq "\xD4") {
            &vsegment_term ($h2, $v1, $v3);
            &hsegment ($h2, $h4, $v1);
            &hsegment ($h2, $h4, $v2);
        } elsif ($ch eq "\xD5") {
            &vsegment ($h2, $v0, $v2);
            &hsegment ($h2, $h4, $v1);
            &hsegment ($h2, $h4, $v2);
        } elsif ($ch eq "\xD6") {
            &hsegment ($h1, $h4, $v1);
            &vsegment ($h1, $v0, $v1);
            &vsegment ($h3, $v0, $v1);
        } elsif ($ch eq "\xD7") {
            &vsegment ($h1, $v0, $v3);
            &vsegment ($h3, $v0, $v3);
            &hsegment ($h0, $h4, $v1);
        } elsif ($ch eq "\xD8") {
            &hsegment ($h0, $h4, $v1);
            &hsegment ($h0, $h4, $v2);
            &vsegment ($h2, $v0, $v3);
        } elsif ($ch eq "\xD9") {
            &hsegment_term ($h0, $h2, $v1);
            &vsegment_term ($h2, $v1, $v3);
        } elsif ($ch eq "\xDA") {
            &vsegment ($h2, $v0, $v1);
            &hsegment ($h2, $h4, $v1);
        } else {
            # draw a box for all others for now
            &vsegment ($h0, $v0, $v3);
            &hsegment ($h0, $h4, $v0);
            &hsegment ($h0, $h4, $v3);
            &vsegment ($h4, $v0, $v3);
        }
    }

    # Strip trailing blanks from the buffer and return it
    $b =~ s/ +$//;
    $b;
}

# If there is already a segment at this X coordinate, see if it can be extended.
# If not, output it and then output the new segment; otherwise, output the
# extended segment.

sub vsegment_term {
    local ($x, $y1, $y2) = @_;

    if ($vtop{$x}) {
        if ($vbottom{$x} == $y2) {
            # draw the combined segment
            $y2 = $vtop{$x};
            printf OUT ("%.6g %.6g %.6g VL\n", $y2, $x, $y1);
        } else {
            # draw the older segment
            printf OUT ("%.6g %.6g %.6g VL\n", $vtop{$x}, $x, $vbottom{$x});
            # draw the new segment
            printf OUT ("%.6g %.6g %.6g VL\n", $y2, $x, $y1);
        }
        # delete the segment
        delete $vtop{$x};
    } else {
        # output the command
        printf OUT ("%.6g %.6g %.6g VL\n", $y2, $x, $y1);
    }
}

# If there is already a segment at this X coordinate, see if it can be extended.
# If not, output it and then save the new segment; otherwise, save the
# extended segment.

sub vsegment {
    local ($x, $y1, $y2) = @_;

    if ($vtop{$x}) {
        if ($vbottom{$x} == $y2) {
            # save the combined segment
            $vbottom{$x} = $y1;
        } else {
            # draw the older segment
            printf OUT ("%.6g %.6g %.6g VL\n", $vtop{$x}, $x, $vbottom{$x});
            # save the new segment
            $vbottom{$x} = $y1;
            $vtop{$x} = $y2;
        }
    } else {
        # save the new segment
        $vbottom{$x} = $y1;
        $vtop{$x} = $y2;
    }
}

# If there is already a segment at this Y coordinate, see if it can be extended.
# If not, output it and then output the new segment; otherwise, output the
# extended segment.

sub hsegment_term {
    local ($x1, $x2, $y) = @_;

    if ($hleft{$y}) {
        if ($hright{$y} == $x1) {
            # draw the combined segment
            $x1 = $hleft{$y};
            printf OUT ("%.6g %.6g %.6g HL\n", $x2, $y, $x1);
        } else {
            # draw the older segment
            printf OUT ("%.6g %.6g %.6g HL\n", $hright{$y}, $y, $hleft{$y});
            # draw the new segment
            printf OUT ("%.6g %.6g %.6g HL\n", $x2, $y, $x1);
        }
        # delete the segment
        delete $hleft{$y};
    } else {
        # output the command
        printf OUT ("%.6g %.6g %.6g HL\n", $x2, $y, $x1);
    }
}

# If there is already a segment at this Y coordinate, see if it can be extended.
# If not, output it and then save the new segment; otherwise, save the
# extended segment.

sub hsegment {
    local ($x1, $x2, $y) = @_;

    if ($hleft{$y}) {
        if ($hright{$y} == $x1) {
            # save the combined segment
            $hright{$y} = $x2;
        } else {
            # draw the older segment
            printf OUT ("%.6g %.6g %.6g HL\n", $hright{$y}, $y, $hleft{$y});
            # save the new segment
            $hleft{$y} = $x1;
            $hright{$y} = $x2;
        }
    } else {
        # save the new segment
        $hleft{$y} = $x1;
        $hright{$y} = $x2;
    }
}

# Output any buffered segments before moving to the next page.

sub dump_segments {
    foreach $x (sort keys %vtop) {
        printf OUT ("%.6g %.6g %.6g VL\n", $vtop{$x}, $x, $vbottom{$x});
    }
    foreach $y (sort keys %hleft) {
        printf OUT ("%.6g %.6g %.6g HL\n", $hright{$y}, $y, $hleft{$y});
    }
    undef %vtop;
    undef %hleft;
}

sub use_font {
    local ($fontname) = @_;

    print OUT "/$fontname findfont ";
    $pagefonts{$fontname} = 1;
    $docfonts{$fontname} = 1;
}

sub page_header {
    undef %pagefonts;
    $ps_pagenum++;
    print OUT <<"!";
%%Page: $ps_pagenum $ps_pagenum
%%PageResources: (atend)
!
}

sub page_trailer {
    local ($pagefonts) = join (' ', sort keys %pagefonts);
    print OUT <<"!";
showpage
%%PageTrailer
%%PageResources: font $pagefonts

!
}

sub show_timestamp {
    my ($filename) = @_;
    my $time = 'Modified: ' . &print_local_time ((stat ($filename))[9]);
    my $spaces = $cpl - length ($filename) - length ($time);
    &printlongline ($filename . ' 'x$spaces . $time);
    $time = 'Printed: ' . &print_local_time (time);
    $spaces = $cpl - length ($time);
    &printlongline (' 'x$spaces . $time);
}
