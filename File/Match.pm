package File::Match;

# History:
#   2007/03/31 DT   Pull out from diffx.

use strict;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION = 1.00;
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = qw(match);
    %EXPORT_TAGS = ();
}

# Returns 1 if two files are identical. We already know that both exist
# and they're the same size.
sub match {
    my ($name1, $name2, $heuristic) = @_;
    my $same = 1;

    # Check to see if either is a directory
    if (-d $name1) {
        return -d $name2 ? 1 : 0;
    } elsif (-d $name2) {
        return 0;
    }

    # Compare them as files
    if ($heuristic) {
        # Compare their timestamps only
        (stat ($name1))[9] == (stat ($name2))[9];
    } else {
        my ($buff1, $buff2);
        my $len;

        # Compare their contents
        open (F1, $name1) || die "can't open $name1: $!\n";
        open (F2, $name2) || die "can't open $name2: $!\n";
        while ($same) {
            $len = read (F1, $buff1, 8192);
            $same = $len == read (F2, $buff2, 8192);
            last unless $same;
            $same = $buff1 eq $buff2;
            last unless $same;
            last if $len < 8192;
        }
        close (F1);
        close (F2);
        $same;
    }
}

1;
