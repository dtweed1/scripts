package File::CSV;

# Read and write CSV files, using Text::CSV.

# To do:
#   Handle a newline that appears within a quoted field.

# History:
#   2009/02/21 DT   Handle MS-style line endings (\r\n) even when running
#                   under Cygwin.
#   2003/01/18 DT   Convert to a module.
#   2002/03/26 DT   Chomp database lines when loading.
#   2002/03/16 DT   Use Text::CSV2.
#   2002/01/06 DT   Start.

use strict;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION = 1.00;
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = qw(load_CSV_file write_CSV_file);
    %EXPORT_TAGS = ();
}

use Text::CSV2;

# Load a CSV file into an array or hash.
# The first argument is the filename.
# The optional second argument is either a true scalar, indicating that the
# first line of the file contains headings, or an arrayref that gives the
# headings. In either case, a reference to an array of hashrefs is returned,
# where each hash uses the headers as keys.
# If no headers are available (no second argument), the return value is a
# reference to an array of arrays, where the secondary arrays contain the
# field values in the order they were found.

sub load_CSV_file {
    my ($filename, $headers) = @_;
    my $csv = Text::CSV->new();
    my @keys;
    my @data;
    local (*FILE);

    open (FILE, $filename) || die "can't read $filename: $!";
    if (ref $headers) {
        @keys = @$headers;
    } elsif ($headers) {
        my $line = <FILE>;
        chomp $line;
        chop $line if $line =~ /\r$/;
        if ($csv->parse ($line)) {
            @keys = $csv->fields();
        } else {
            warn "Bad CSV headers in $filename\n";
            warn $csv->error_input, "\n";
        }
    }

    while (<FILE>) {
        chomp;
        chop if /\r$/;
        if ($csv->parse ($_)) {
            if (@keys) {
                my %record;
                my @fields = $csv->fields();
                warn "More fields than headers in $filename\n"
                    if @fields > @keys;
                for (0..$#keys) {
                    $record{$keys[$_]} = $fields[$_] || '';
                }
                push @data, \%record;
            } else {
                push @data, [$csv->fields()];
            }
        } else {
            warn "Bad CSV record in $filename\n";
            warn $csv->error_input, "\n";
        }
    }

    close FILE;
    return \@data;
}

# Write a CSV file from an array of arrays or hashes.
# If the $dataref points to an array of arrays, the data is written as-is.
# If the $dataref points to an array of hashes, a header line is written to
# the file using the keys from the first record, and then all of the data is
# written using the keys in that order.
# Alternatively, a reference to a list of keys in a particular order can be
# passed in as a third argument.

sub write_CSV_file {
    my ($filename, $dataref, $headers) = @_;
    my $csv = Text::CSV->new();
    my @keys;
    my @fields;
    local (*FILE);

    if (ref $dataref->[0] eq "HASH") {
        if (defined $headers) {
            @keys = @$headers;
        } else {
            @keys = keys %{$dataref->[0]};
        }
    }

    open (FILE, ">$filename") || die "can't write $filename: $!";
    if (@keys) {
        if ($csv->combine (@keys)) {
            print FILE $csv->string, "\n";
        } else {
            warn "Bad CSV headers for $filename\n";
        }
    }
    foreach my $rec (@$dataref) {
        if ($csv->combine (@keys ? @{$rec}{@keys} : @$rec)) {
            print FILE $csv->string, "\n";
        } else {
            warn "Bad CSV record for $filename\n";
        }
    }
    close (FILE) || die "can't close $filename: $!";
}

1;
