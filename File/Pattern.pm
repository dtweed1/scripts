package File::Pattern;

=head1 NAME

File::Pattern - support for "tagged text" databases

=head1 SYNOPSIS

use File::Pattern qw(multi_glob);

=head1 DESCRIPTION

This module provides functions for matching file nanes while waking trees.

=over

=cut

# This comes up often enough that I finally pulled it out to its own module.

# History:
#   2019/02/26 DT   Pulled out from diffx to use in isync.

use strict;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION = 1.00;
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = qw(multi_glob);
    %EXPORT_TAGS = ();
}

=item multi_glob ( [ <options> ] I<pattern> ... );

The patterns are strings. If the first argument is a hashref, it is taken to
be a set of options.

Convert the list of patterns into a list of REs, then combine the REs
into one big filter. If the list is empty, use either /.*/ or /^[^.]/,
depending on the state of the 'allfiles' option.

Similar to File::Glob, but can merge multiple patterns together, requiring
only a single read of the directory.

The return value is the final RE.

=cut

sub multi_glob {
    my $optref = {allfiles => 0};
    $optref = shift if ref $_[0];
    my @patterns = @_;
    my $filter;

    if (@patterns) {
	# convert patterns from wildcard syntax to Perl RE syntax
	foreach (@patterns) {
	    s/\./\\./g;
	    s/\*/.*/g;
	    s/\?/./g;
#           warn "File pattern: /$_/\n";
	}
	# combine the patterns into one big RE
	if (@patterns > 1) {
	    # any of a list of names
	    $filter = '^((' . join (')|(', @patterns) . '))$'; #'
	} else {
	    # one exact name
	    $filter = '^' . (shift @patterns) . '$'; #'
	}
    } elsif ($optref->{'allfiles'}) {
	# all file names
	$filter = '.*';
    } else {
	# default: names not beginning with '.'
	$filter = '^[^.]';
    }
    $filter;
}

=back

=head1 ENVIRONMENT

This script uses only standard Perl modules: C<Exporter>, C<vars>.

=head1 AUTHOR

Written by Dave Tweed.

=cut

1;
