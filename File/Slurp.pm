package File::Slurp;

=head1 NAME

File::Slurp - support for "tagged text" databases

=head1 SYNOPSIS

use File::Slurp qw(slurp slurp2);

=head1 DESCRIPTION

This module provides functions for reading "tagged text" database files,
in which records are blocks of text separated by blank lines.
Within a record, each field contains a name, value pair separated by a colon
(which may be followed by whitespace).
The value may extend to multiple lines; continuation lines start with
whitespace.

The file may also contain comment lines, which start with a hash character.
The hash may have whitespace in front of it as well.

=over

=cut

# This comes up so often that I finally pulled it out to its own module.

# History:
#   2009/12/01 DT   Add option to slurp() to preserve continuation lines.
#                   Used in "mix.pl" for the CAJ index.
#                   Add POD.
#   2007/10/26 DT   Add binmode hack, for use with CCI contest summary.
#   2003/01/18 DT   Convert to a module.
#   2001/04/03 DT   Remove "slurping $datafile" messages.
#                   localize $/ change in slurp().
#   2001/03/08 DT   Make open failure nonfatal.
#                   Localize the filehandles.
#   2000/12/01 DT   Add 'slurp2()'
#   2000/10/05 DT   Pulled out from e:/ccb/website.pl

use strict;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION = 1.00;
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = qw(slurp slurp2);
    %EXPORT_TAGS = ();
}

=item slurp (I<filename> [, I<option> => I<value> ... ]);

Return the contents of a file as an array of records, with each record
containing a set of tagged fields separated by newlines.

Options, if any, are specified as a hash.
These include:

=over

=item no_rejoin => 1

If true, any continuation lines are preserved as-is, rather than being combined.

=back

=cut

sub slurp {
    my ($datafile, %opt) = @_;
    local (*DATABASE);

    local $/ = '';
    unless (open (DATABASE, $datafile)) {
        warn "can't open '$datafile': $!\n";
        return undef;
    }

    # temporary hack to deal with the bizarre setup we have on this system.
    binmode DATABASE, ":crlf";

    # warn "slurping $datafile...\n";
    my @database = grep (!/^#/, <DATABASE>);
    close DATABASE;
    grep (s/\n\s+/ /g, @database) unless $opt{'no_rejoin'};
    return @database;
}

=item slurp2 (I<filename>);

This version splits each record into fields and discards commented-out fields.
It then splits each field into a name, value pair.
It returns an array of arrays of arrays.

=cut

sub slurp2 {
    my ($datafile) = @_;
    my (@result);

    local $/ = '';
    local (*DATABASE);
    unless (open (DATABASE, $datafile)) {
        warn "can't open '$datafile': $!\n";
        return ();
    }

    # temporary hack to deal with the bizarre setup we have on this system.
    binmode DATABASE, ":crlf";

    # warn "slurping $datafile...\n";
    while (<DATABASE>) {
        chomp;

        # process continuation lines
        s/\n\s+/ /g;

        # split out the fields, ignore comments
        my @fields = grep (!/^\s*#/, split "\n");

        # skip this record if no fields are left
        next unless @fields;

        # trim trailing whitespace from each field value
        for (@fields) {s/\s+$//;}

        # split the fields into name, value and add a reference to the
        # resulting array of arrays to the result
        push @result, [map ([split (/:\s+/, $_, 2)], @fields)];
    }
    close DATABASE;
    return @result;
}

=item dump2();

This function used to debug slurp2(),
left in as an example of how to use its output.

=cut

sub dump2 {
    for (@_) {
        print "------\n";
        for (@$_) {
            my ($name, $value) = @$_;
            print "name='$name', value='$value'\n";
        }
    }
    print "------\n";
}

1;

=back

=head1 ENVIRONMENT

This script uses only standard Perl modules: C<Exporter>, C<vars>.

=head1 ISSUES

This module currently includes special support for the bizarre setup that
exists on this system (a mix of the old DJGPP Emacs and Cygwin Perl),
in which files may or may not have <CR> characters at the end of each line.

Note that a comment delimiter on a continuation line (leading whitespace
not on the first line of a record) will not actually be treated as a comment.
If you want to comment out just part of a record or a continuation line,
the '#' needs to be the first character on the line.
This may need to get fixed at some point (or not).

=head1 AUTHOR

Written by Dave Tweed.

=cut
