# Allegro.pm - OO interface to a set of Allegro design files

package Allegro;

#---------------------------------------------------------------------------
# The netlist has three sections, called $PACKAGES, $FUNCTIONS and $NETS.
# The overall structure of the file looks like this:
#
#   (source SCHEMATIC1)
#   $PACKAGES
#                                          (blank line)
#   <package entries>
#                                          (blank line)
#   $FUNCTIONS
#   <function entries>
#                                          (blank line)
#                                          (blank line)
#   $NETS
#   <net entries>
#   $END
#
# A <package entry> looks like this:
#   <package name> ! '<device name>' ! <part value> ; <ref. des.> ...
# Reference designators are separated by spaces.
#
# A <function entry> looks like this:
#   '<device name>' ! '<device name>' ! <part value> ; <instance> ...
# Instance names are of the form F<n>, and are separated by spaces.
#
# A <net entry> looks like this:
#   <net name> ;  <ref. des.>:<instance>.<pin number/name> ...
# Note two spaces after semicolon. Pins are separated by spaces
#
# If an <entry> needs to be broken into multiple lines, all lines except
# the last end with a comma and all but the first begin with four spaces.
# In other words, a continued line has exactly six bytes inserted: a comma,
# a newline and four spaces.
#---------------------------------------------------------------------------
# Each <device name> has a "device file" associated with it that contains
# the following entries:
#
#   (device file for '<device name>')
#   PACKAGE <package name>
#   PINCOUNT <n>
#   PINORDER '<device name>' <pin name> ...
#   PINUSE '<device name>' <pin type> ...
#   FUNCTION <function name> '<device name>' <pin number/name> ...
#   END
#
# If a device contains multiple identical functions, there are multiple
# FUNCTION entries (the names are of the form G<n>), and the PINORDER and
# PINUSE entries apply to only one function at a time. The PINCOUNT is the
# total for the entire device.
#
# <pin type> includes IN OUT UNSPEC BI OCA
#---------------------------------------------------------------------------
# Allegro can't handle heterogeneous parts; these are treated as
# single-function devices, but then all parts other than the first are
# reported as "unused". The netlist itself seems to be correct, however.

# Functionality of this module:
#   Read a design
#   Write a design
#   List by net
#   List by pin
#   Modify pin number/name

# Internal data structure:
#   
#   Device ==> package, value, function(s)
#   Ref. des. ==> Device
#   Instance ==> Ref. des., Function
#   Ref. des., pin ==> net

sub new {
}
