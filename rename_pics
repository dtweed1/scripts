#!/usr/bin/perl -w

# rename_pics - rename the picture files from the Olumpus camera in the form
#               Pmddnnnn.JPG (m is 0, 1, 2, ... 9, A, B, C) to
#               yyyymmddnnn.jpg (I doubt that I'll take more than 1000
#               pictures in a day).

# History:
#   2011/04/05 DT   Add support for Canon camera.
#   2003/11/26 DT   Allow $offset to be set from command line.
#   2003/05/25 DT   Start.

use strict;
use vars;

use lib $ENV{'PERLPATH'};
use Date::Zeller qw(split_time ztoday unzeller);

my ($year, $month, $day) = unzeller (ztoday);
print "Today is $year/$month/$day\n";

# use this for a second or later batch of pictures on a given day
my $offset = shift || 0;

opendir (DIR, '.') || die "can't read '.': $!";
my @files = # grep /^P[0-9ABC]\d{6}/, readdir DIR;
            grep /\.jpg$/i, readdir DIR;
closedir DIR;

for my $file (sort @files) {
    my $newfile;

    if ($file =~ /^P([0-9ABC])(\d\d)(\d\d\d\d)(.*)\.jpg$/i) {
        # Olympus camera; date is partially encoded in filename
        my ($fm, $fd, $findex, $suffix) = ($1, $2, $3, $4);
        $fm = hex ($fm);
        my $fy = $year;
        # If date appears to be in the future, move it back a year.
        --$fy if zeller ($fy, $fm, $fd) > ztoday;
        $newfile = sprintf ("%04d%02d%02d-%03d%s.jpg",
                               $fy, $fm, $fd, $findex+$offset, $suffix);
    } elsif ($file =~ /^IMG_(\d\d\d\d)\.JPG$/i) {
        # Canon camera; get date from file timestamp
        my ($findex) = ($1);
        my $fmtime = (stat ($file))[9];
        my ($fy, $fm, $fd, $fhr, $fmi, $fse) = split_time ($fmtime);
        $newfile = sprintf ("%04d%02d%02d-%03d.jpg",
                               $fy, $fm, $fd, $findex+$offset);
    } else {
        # Skip this file
        next;
    }
    print "renaming $file ==> $newfile\n";
    rename ($file, $newfile) || die " ... can't rename: $!";
}
