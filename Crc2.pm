# Crc2.pm - library of functions for generating CRCs

# To do:
#   Refout isn't implemented yet.
#   Verify the logic of refin for width>8.

# History:
#   2001/12/10 DT   Add the full suite of Rocksoft parameters (see crc_v3.txt).
#                   Switch to hash parameter for new().
#   2001/08/09 DT   Add ability to specify nonstandard polynomial to new().
#   2001/02/21 DT   Add POD. Make some efficiency tweaks.

package Crc;

=head1 NAME

Crc - calculate Cyclic Redundancy Checks

=head1 SYNOPSIS

 use Crc;

 # create a CRC object with the specified algorithm
 $crc = Crc::new (32);
 $crc = Crc::new (width => 32);

 # create a CRC object with the specified algorithm and polynomial
 # Note that the MSB of the poly (at the bit position given by the width)
 # is assumed to be one and is not included in the integer value.
 $crc = Crc::new (8, 0xEA);
 $crc = Crc::new (width => 8, poly => 0xEA);

 # additional parameters to new(), with default values shown
 ... init => 0xFFFFFFFF   # integer, initial value of working register
 ... refin => 0           # boolean, reflect input values before processing
 ... refout => 0          # boolean, reflect output before final XOR
 ... xorout => 0xFFFFFFFF # integer, XOR'd with final result after reflection

 # run a series of bytes through the CRC object
 $crc->Reset;
 $result = $crc->Process_Bytes (@data);

 # run a series of strings through the CRC object
 $crc->Reset;
 $result = $crc->Process_Strings (@data);

 # run the contents of a file through the CRC object
 $crc->Reset;
 $result = $crc->Process_File ($filename);
    
=head1 DESCRIPTION

C<Crc> is a class implementing state machines that calculate CRCs on data
a byte at a time, using a table-driven approach.

=head1 OVERVIEW

=head1 CONSTRUCTOR

=over 4

=item new (WIDTH [, POLY])

Create a CRC state machine with the specified width and optional nonstandard
polynomial.
Currently, the algorithms supported are
8-bit (polynomial 0xB8),
16-bit (polynomial 0xA001, CCITT standard) and
32-bit (polynomial 0xEDB88320, CCITT standard).

The state machine is optimized to process data 8 bits at a time, using
a table that is built when the machine is created.

=back

=head1 METHODS

=over 4

=item Reset()

Reinitialize the state machine.
Returns the initial state of the machine.

=item Process_Bytes (DATA)

Process an array of 8-bit values, updating the state machine with each.
Returns the current state of the machine.

=item Process_Strings (DATA)

Process an array of strings, each of which is broken into bytes and passed
through Process_Bytes().
Returns the final state of the machine.

=item Process_File (FILENAME)

Read the file and pass its contents through Process_Strings().

B<Note:> If the -T test on the file returns true, line terminators are B<not>
included in the CRC.

=back

=head1 INSTALLATION

Copy this file to your Perl 5 library directory.

=head1 AUTHOR

David Tweed

=head1 TO DO

Accept a handle as well as a string in Process_File().

Localize the filehandle used in Process_File().

=head1 BUGS

None found at present.

=head1 COPYRIGHT

Copyright (c) 2000-2001 David Tweed. All rights reserved.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

%default_polys = (
    8  => 0xB8,
    16 => 0xA001,        # CCITT standard
    32 => 0xEDB88320,    # CCITT standard
);

sub new {
    my %param = @_;
    my $self = {};

    my $bits = $param{'width'};
    die "Crc: must specify width >= 8\n" unless defined $bits || $bits < 8;
    $self->{'bits'} = $bits;

    my $polynomial = $param{'poly'};
    unless (defined $polynomial) {
        if (defined $default_polys{$bits}) {
            $polynomial = $default_polys{$bits};
        } else {
            die "Crc: must specify poly\n";
        }
    }

    $self->{'mask'}   = (1<<($bits-8)) - 1;
    $self->{'init'}   = $param{'init'} || (1<<$bits) - 1;
    $self->{'xorout'} = $param{'xorout'} || $self->{'init'};
    $self->{'refout'} = $param{'refout'} || 0;

    my $refin = $param{'refin'} || 0;

    # build the table
    foreach my $i (0..255) {
        my $crc = $i;
        if ($refin) {
            # process the MSB of each byte first
            for (0..7) {
                if ($crc & 0x80) {
                    $crc = ($crc<<1) ^ $polynomial;
                } else {
                    $crc <<= 1;
                }
            }
        } else {
            # process the LSB of each byte first
            for (0..7) {
                $crc ^= $polynomial<<(7-$_) if ($i>>$_) & 1;
            }
        }
        $crc &= (1<<$bits) - 1;
        $self->{'table'}[$i] = $crc;
    }

    Reset ($self);

    bless $self;
    $self;
}

sub Reset {
    my ($self) = @_;
    return undef unless ref($self);

    $self->{'crc'} = $self->{'init'};
    return $self->{'crc'} ^ $self->{'xorout'};
}

sub Process_Bytes {
    my $self = shift;
    return undef unless ref($self);

    my ($crc, $mask, $rtab) = @self->{'crc', 'mask ', 'table'};
    foreach (@_) {
        # $crc = (($crc<<8) | $_) ^ $rtab->[$crc>>24];
	$crc = (($crc>>8) & $mask) ^ $rtab->[$crc ^ $_ & 0xFF];
        # warn sprintf "byte: %02X, CRC: %08X\n", $_, $self->{'crc'};
    }
    $self->{'crc'} = $crc;
    return $crc ^ $self->{'xorout'};
}

sub Process_Strings {
    my $self = shift;
    return undef unless ref($self);

    foreach (@_) {
        # warn "string: $_\n";
        Process_Bytes ($self, unpack ('C*', $_));
    }
    return $self->{'crc'} ^ $self->{'xorout'};
}

# Note the slightly odd semantics: If Perl thinks it's a text file, line
# terminators are not included in the CRC.
sub Process_File {
    my ($self, $path) = @_;
    my ($buffer);
    return undef unless ref($self);

    open (INPUT, $path) || return undef;
    if (-T $path) {
        # warn "text file: $path\n";
        while (<INPUT>) {
            chomp;
            Process_Strings ($self, $_);
        }
    } else {
        # warn "binary file: $path\n";
        binmode INPUT;
        while (read (INPUT, $buffer, 8192)) {
            Process_Strings ($self, $buffer);
        }
    }
    close INPUT;
    return $self->{'crc'} ^ $self->{'xorout'};
}

1;
