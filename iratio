#!/usr/bin/perl -w

# Find a pair of integers that approximates the given ratio

# History:
#   2017/05/18 DT   Add '+', '-' options.
#   2010/12/28 DT   Start.

my $s = '';
my $x = shift;

# If the first argument is '+', only consider results that are greater than
# the given ratio.
# If the first argument is '-', only consider results that are less than
# the given ratio.
# Otherwise, use the smallest absolute error.

if ($x =~ /[+-]/) {
    $s = $x;
    $x = shift;
}

my $i = 1;
my $last_err = 1;

while ($last_err > 1e-9) {
    my $j = int ($i * $x + 0.5);
    if ($s eq '+') {
	++$j if $j/$i < $x;
    } elsif ($s eq '-') {
	--$j if $j/$i > $x;
    }
    my $y = $j/$i;
    my $err = ($y/$x-1);
    my $aerr;
    $aerr = abs ($err);
    if ($aerr < $last_err) {
        if ($aerr < 1e-4) {
            printf "%d/%d = %7f, error = %6.3fppm\n", $j, $i, $y, $err*1e6;
        } else {
            printf "%d/%d = %7f, error = %6.3f%%\n", $j, $i, $y, $err*1e2;
        }
        $last_err = $aerr;
    }
    ++$i;
}
