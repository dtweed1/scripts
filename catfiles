#!/usr/bin/perl -w

# catfiles

# Concatenate a bunch of text files with formfeeds for more compact printing,
# e.g., lots of small source files being printed 2-up.
# File base names and extensions are specified separately -- files with a
# given basename are printed together in the sequence given, with the
# extensions appearing within each group in the given sequence as well.

# History:
#   2004/03/19 DT   Allow full filenames to be specified as well.
#                   These get printed before any base-ext combinations.
#   2004/03/18 DT   Create command-line interface.
#   2004/03/10 DT   Start.

use strict;

my @names;
my @exts;

my @files;

my $flag_debug = 0;
my $flag_base = 0;
my $flag_ext = 0;

# Process the command line

for (@ARGV) {
    if (/^-/) {
        foreach my $c (split (/ */, substr ($_, 1))) {
            if ($c eq 'b') {$flag_base = 1; $flag_ext = 0;}
            if ($c eq 'd') {$flag_debug++;}
            if ($c eq 'e') {$flag_base = 0; $flag_ext = 1;}
            if ($c eq 'f') {$flag_base = 0; $flag_ext = 0;}
        }
    } elsif ($flag_ext) {
        push @exts, $_;
    } elsif ($flag_base) {
        push @names, $_;
    } else {
        push @files, $_;
    }
}

# Combine the base names and extensions and find out which combinations
# actually exist.

for my $basename (@names) {
    for my $ext (@exts) {
        my $f = "$basename.$ext";
        push @files, $f if -e $f;
    }
}

if ($flag_debug) {
    warn "names = @names\n";
    warn "extensions = @exts\n";
    warn "files = @files\n";
    exit;
}

# Read and print each file in sequence, inserting a formfeed between files.

for my $f (@files) {
    open (IN, $f) || die "can't read $f: $!";
    print <IN>;
    close IN;
    print "\014\n" unless $f eq $files[-1];
}
