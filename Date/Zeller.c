/* Zeller.c */

/* C implementation of some of the functions in Zeller.pm, for StackExchange.
 */

/* History:
 *  2013/04/20 DT   Start.
 */

#include <stdio.h>

/* Returns the number of days to the start of the specified year, taking leap
 * years into account, but not the shift from the Julian calendar to the
 * Gregorian calendar. Instead, it is as though the Gregorian calendar is
 * extrapolated back in time to a hypothetical "year zero".
 */
int leap (int year)
{
  return year*365 + (year/4) - (year/100) + (year/400);
}

/* Returns a number representing the number of days since March 1 in the
 * hypothetical year 0, not counting the change from the Julian calendar
 * to the Gregorian calendar that occured in the 16th century. This
 * algorithm is loosely based on a function known as "Zeller's Congruence".
 * This number MOD 7 gives the day of week, where 0 = Monday and 6 = Sunday.
 */
int zeller (int year, int month, int day)
{
  year += ((month+9)/12) - 1;
  month = (month+9) % 12;
  return leap (year) + month*30 + ((6*month+5)/10) + day + 1;
}

/* Returns the day of week (1=Monday, 7=Sunday) for a given date.
 */
int dow (int year, int month, int day)
{
  return (zeller (year, month, day) % 7) + 1;
}

/* Test program
 */
int main (int argc, char *argv[])
{
  char *dayname[] = {"zero", "Monday", "Tuesday", "Wednesday",
		     "Thursday", "Friday", "Saturday", "Sunday"};
  int y = atoi(argv[1]);
  int m = atoi(argv[2]);
  int d = atoi(argv[3]);

  printf ("zeller (%d-%d-%d) is %d\n", y, m, d, zeller (y, m, d));
  printf ("%d-%d-%d is a %s\n", y, m, d, dayname[dow (y, m, d)]);
}
