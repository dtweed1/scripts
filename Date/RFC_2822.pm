package Date::RFC_2822;

=head1 NAME

Date::RFC_2822.pm - parse and print RFC-2822 timestamps

=head1 SYNOPSIS

   use Date::RFC_2822;

   integer = parse_time (string)
   string = print_time (integer)

=head1 DESCRIPTION

This is a module that supports parsing the dates and times used in the RFC-2822
"Date:" field. The time is represented either as a string in the form

  "Dow, dd Mmm yyyy hh:mm:ss -HHMM"

or as an integer number of seconds since midnight 1970/1/1.

=cut

# History:
#    2019/07/13 DT   Pull parse_time() out of ~/mail/index;
#                    print_time() out of ~/mail/send

=head1 ENVIRONMENT

This module reqires Date::Zeller.

=cut

use lib $ENV{'PERLPATH'} || '/bin';
use Date::Zeller;
use Date::Zeller qw(@dname @mname split_time join_time get_tz is_dst);

use Exporter;
@ISA = (Exporter);
@EXPORT = qw();
@EXPORT_OK = qw(parse_time print_time);

=head1 FUNCTIONS

=over

=item integer = parse_time (string)

Returns the number of seconds from 00:00:00 1970/1/1 to the specified
date and time.

=cut

# An RFC2822 date is of the form "[ Dow, ] dd Mmm yyyy hh:mm:ss -HHMM", where
# the date and time are in the sender's local time, and "sHHMM" represent the
# offset from GMT to that time.
# The values might be followed by optional stuff like the name of the sender's
# timezone (e.g., # " (EDT)").
sub parse_time {
    my $string = shift;
    return undef unless defined $string;

    my %mhash = map {substr ($mname[$_], 0, 3) => $_+1} (0 .. $#mname);
    # for (sort keys %mhash) {print "$_ => $mhash{$_}\n";}
    my ($dow, $d, $m, $y, $t, $o, $extra) = split (' ', $string, 7);
    # NOTE: the "Dow," part seems to be optional.
    # If the $t string doesn't contain any ":" characters, shift everything
    # over by one place.
    unless ($t =~ /:/) {
        ($d, $m, $y, $t, $o, $extra) = split (' ', $string, 6); 
    }
    die "parse_date: can't find time field" unless $t =~ /:/;
    # print "Date: $y/$m/$d\n";
    my ($hh, $mm, $ss) = split (/:/, $t);
    # print "Time: $hh:$mm:$ss\n";
    my ($oh, $om) = $o =~ /^([-+]?\d\d)(\d\d)/;
    my $off = $oh*60+$om;
    # print "Offset: $oh:$om ($off minutes)\n";
#   my $z = zeller ($y, $mhash{$m}, $d);
#   ((($z - zeller (1970, 1, 1))*24 + $hh)*60 + $mm - $off)*60 + $ss;
    join_time ($y, $mhash{$m}, $d, $hh, $mm - $off, $ss);
}

=item string = print_time (string)

Converts the number of seconds from 00:00:00 1970/1/1 into an RFC-2822 string.

=cut

# Date: Mon, 24 Mar 2003 12:55:00 -0500
sub print_time {
    my $time = shift;
    my ($offset) = get_tz();
    my ($year, $month, $date, $hour, $minute, $second, $wday) = split_time ($time);
    --$offset if is_dst ($year, $month, $date, $hour);
    my ($daystr) = substr ($dname[$wday], 0, 3);
    my ($monstr) = substr ($mname[$month-1], 0, 3);
    sprintf ("%s, %d %s %d %d:%02d:%02d -%04d",
             $daystr, $date, $monstr, $year,
             $hour, $minute, $second, $offset*100);
}

=back

=head1 AUTHOR

Written by Dave Tweed.

=cut

1;
