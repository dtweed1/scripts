package Date::Zeller;

=head1 NAME

Date::Zeller.pm - common code for cal, wt, etc.

=head1 SYNOPSIS

   use Date::Zeller;

   integer = zeller (year, month, day)
   (year, month, day) = unzeller (integer)
   days = leap (year)
   integer = join_time (year, month, date, hour, minute, second)
   (year, month, date, hour, minute, second) = split_time (integer)
   boolean = is_dst (year, month, date, hour)
   time = perl_fixup (time)
   (integer, string, string) = get_tz()
   time = hardware_time()
   time = local_time (time)
   string = print_local_time (time)
   string = print_GM_time (time)
   integer = ztoday()

=head1 DESCRIPTION

This is a module that supports manipulating dates and times, taking into
account the varying lengths of months, leap years, time zones and daylight
savings time.

"zeller numbers" (values returned by the C<zeller()> function) can be
subtracted to get the number of days between dates, have offsets added
to them to get start/end dates relative to a given date, or be taken MOD 7
to get the day of the week.

You can even implement logic such as "the last Sunday in October".
See the C<is_dst()> function for examples.

=cut

# History:
#   2013/04/20 DT   Fix comments on leap().
#   2011/11/25 DT   Streamline is_dst(); add dst_starts() and dst_ends().
#   2010/03/07 DT   Fix bug in "second Sunday in March" algorithm.
#   2009/02/18 DT   Add POD.
#   2006/10/30 DT   Added new DST rules for next year.
#   2003/02/25 DT   Found the real "Zeller's Congruence" on the net.
#   2003/01/06 DT   Isolate MSWin32 dependencies.
#                   Add scalar output to split_time().
#                   Add get_tz().
#   2003/01/02 DT   Fix export of @dname, @mname.
#   2001/03/10 DT   Debug unzeller(), hopefully for the last time.
#   2001/03/09 DT   Add ztoday().
#                   Convert to Date::Zeller.pm.
#   2000/08/21 DT   Add local_time(). Change local to my.
#   2000/01/12 DT   Fix subtle rounding bug in unzeller().
#   1999/09/27 DT   Adjust $fixed_offset for laptop.
#                   Fix comment regarding days of week.
#   1999/04/20 DT   Add day and month name arrays.
#   1998/04/09 DT   Fix up comments regarding the calendar change.
#   1997/12/30 DT   Fiddle around with $fixed_offset in perl_fixup()
#   1997/04/07 DT   Add print_local_time() and print_GM_time().
#   1995/11/03 DT   Create hardware_time() from code in wtx.bat.
#   1995/10/31 DT   Fix is_dst() for last days of October.
#   1995/08/12 DT   Streamline is_dst().
#   1995/08/01 DT   Create separate file for these functions.

=head1 ENVIRONMENT

This module uses two environment variables:

=over

=item TZ

The local time zone, in the form "EST5EDT".
The first set of letters is the abbreviation for local standard time, the
number in the middle is the number of hours offset from local standard time
to GMT (positive west of England; only integers supported currently),
and the last set of letters is the abbreviation for local daylight savings
time.

If this variable does not exist, the default value is "EST5EDT".

=item ZELLER_OFFSET

If set, the number of seconds to subtract unconditionally from a machine
time value to get GMT.

If this variable does not exist, the default value is 0.

=back

=cut

use Exporter;
@ISA = (Exporter);
@EXPORT = qw(zeller unzeller ztoday);
@EXPORT_OK = qw(@dname @mname join_time split_time dst_starts dst_ends is_dst
                hardware_time local_time print_local_time print_GM_time get_tz);

=head1 DATA

The following data structures are available in this module.

=over

=item @dname

This is a list of the names of the days of the week (in English), starting
with "Sunday".

=cut

@dname = ('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
          'Saturday'
);

=item @mname

This is a list of the names of the months (in English), starting
with "January".

=back

=cut

@mname = ('January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December'
);

=head1 FUNCTIONS

=over

=item integer = zeller (year, month, day)

Returns a number representing the number of days since March 1 in the
hypothetical year 0, not counting the change from the Julian calendar
to the Gregorian calendar that occured in the 16th century. Pope Gregory
skipped directly from October 4 to October 15 (deleting 10 days) in 1582.
Note that non-Catholic countries did this later, (1780-something?
I think it was in September) requiring them to remove 11 days.
This number MOD 7 gives the day of week, where 0 = Monday and 6 = Sunday.

=cut

sub zeller {
    my ($year, $month, $day) = @_;

    $year += int(($month+9)/12) - 1;
    $month = ($month+9) % 12;
    &leap ($year) + $month*30 + int((6*$month+5)/10) + $day + 1;
}

=item (year, month, day) = unzeller (integer)

The inverse of C<zeller()>, returns a list of three integers representing the
date specified by the argument.

=cut

sub unzeller {
    my ($day) = @_;
    my ($year, $month);

if (0) {
    # Test version for debugging
    # warn "unzeller - initial day: $day\n";
    $day -= 2;
    my $fyear = $day * 400 / 146097;
    warn "unzeller - fractional year: $fyear\n";
    $year = int ($fyear);
    ++$year if $day >= &leap($year+1);
    warn "unzeller - initial year: $year\n";
    $day -= &leap($year);
    # warn "unzeller - adjusted day: $day\n";
    $month = int (($day+0.5) / 30.6);
    # warn "unzeller - initial month: $month\n";
    $day -= $month*30 + int((6*$month+5)/10);
    # warn "unzeller - adjusted day: $day\n";
    $year += int (($month+2)/12);
    # warn "unzeller - adjusted year: $year\n";
    $month = ($month+2) % 12 + 1;
    # warn "unzeller - adjusted month: $month\n";
    ($year, $month, $day+1);
} else {
    # regular version
    $day -= 2;
    $year = int ($day * 400 / 146097);
    ++$year if $day >= &leap($year+1);
    $day -= &leap($year);
    $month = int (($day+0.5) / 30.6);
    $day -= $month*30 + int((6*$month+5)/10);
    $year += int (($month+2)/12);
    $month = ($month+2) % 12 + 1;
    ($year, $month, $day+1);
}
}

=pod

Note that the two functions above do not implement the traditional
"Zeller's Congruence", but rather a simplified function that I developed
independently.

The true "Zeller's Congruence", dating from 1887 and pieced together from
various implementations on the web, goes more like the following.
It is only used to calculate day of week. (0 = Saturday)

  sub zellers_congruence {
      my ($year, $month, $day) = @_;
      if ($month < 3) {
          $month += 12;
          --$year;
      }
      # see http://www.howtodothings.com/showarticle.asp?article=260
      ($day + int ((($month+1)*26)/10) + $year
       + int ($year/4) - 2 * int ($year/100) + int ($year/400)) % 7;
      # see http://mathforum.org/library/drmath/view/55837.html
      ($day + [(13*$month-3)/5] + $year%100
       + int($year/4) + int($year/400) - 2*int($year/100)) % 7
      ($day + 2*$month + int ((3*($month+1))/5) + $year
       + int ($year/4) - int ($year/100) + int ($year/400) + 1) % 7;
  }

=cut

=item days = leap (year)

Returns the number of days to the start of the specified year, taking leap
years into account, but not the shift from the Juilan calendar to the
Gregorian calendar.
Instead, it is as though the Gregorian calendar is extrapolated back in time
to a hypothetical "year zero".

=cut

sub leap {
    my ($year) = @_;
    return $year*365 + int($year/4) - int($year/100) + int($year/400);
}

=item integer = join_time (year, month, date, hour, minute, second)

Returns the number of seconds from 00:00:00 1970/1/1 to the specified
date and time.
"year" is a full 4-digit year, and "month" is 1-based.

=cut

sub join_time {
    my ($y, $mo, $md, $h, $mi, $s) = @_;

    # 719470 = &zeller (1970, 1, 1).
    86400 * (&zeller ($y, $mo, $md) - 719470) + 3600 * $h + 60 * $mi + $s;
}

=item (year, month, date, hour, minute, second) = split_time (integer)

The inverse of C<join_time()>, returns a list of numbers representing
the date and time specified by the given integer.

Note that C<split_time()> does almost the same thing as the built-in
C<gmtime()>, except for the year and month fields.
Also, the $dst part of C<gmtime()> is broken -- it always returns 0.

=cut

sub split_time {
    my ($time) = @_;
    # my ($s, $mi, $h, $md, $mo, $y, $wd, $yday) = gmtime ($time);
    # $mo += 1;
    # $y += 1900;
    my ($days, $y, $mo, $md, $h, $mi, $s, $wd);

    # 719470 = &zeller (1970, 1, 1).
    $days = int ($time/86400) + 719470;
    ($y, $mo, $md) = &unzeller ($days);
    $wd = ($days+1) % 7;

    $s = $time % 86400;
    $h = int ($s / 3600);
    $s -= $h * 3600;
    $mi = int ($s / 60);
    $s -= $mi * 60;
    wantarray
        ? ($y, $mo, $md, $h, $mi, $s, $wd)
        : sprintf ('%4d/%02d/%02d %2d:%02d:%02d (%s)',
                   $y, $mo, $md, $h, $mi, $s, $dname[$wd]);
}

=item boolean = is_dst (year, month, date, hour)

Returns a boolean value that is true if the specified date and time falls
within Daylight Savings Time as defined in the US.
The input arguments are assumed to represent local standard time.

In years prior to 2007, Daylight Savings Time is true starting at 02:00:00
on the first Sunday in April up to (but not including) 02:00:00 on the last
Sunday in October.

Starting in 2007, DST starts on the second Sunday in March and
ends on the first Sunday in November.

=cut
sub dst_starts {
    my ($y) = @_;
    if ($y >= 2007) {
	# starting in 2007: second Sunday in March
	# This calculation must start with the day before the
	# earliest possible "second Sunday".
	my $start = &zeller ($y, 3, 7);
	$start += 7 - ($start+1)%7;
    } else {
	# prior to 2007: first Sunday in April
	my $start = &zeller ($y, 3, 31);
	$start += 7 - ($start+1)%7;
    }
}

sub dst_ends {
    my ($y) = @_;
    if ($y >= 2007) {
	# starting in 2007: first Sunday in November
	my $end = &zeller ($y, 10, 31);
	$end += 7 - ($end+1)%7;
    } else {
	# prior to 2007: last Sunday in October
	my $end = &zeller ($y, 10, 24);
	$end += 7 - ($end+1)%7;
    }
}

sub is_dst {
    my ($y, $mo, $md, $h) = @_;

    my $now = &zeller ($y, $mo, $md) * 24 + $h;
    my $start = dst_starts ($y) * 24 + 2;
    my $end = dst_ends ($y) * 24 + 2;
    $start <= $now && $now < $end;
}

# TBD: Kept around in case we really need these performance optimizations.
sub is_dst_old {
    my ($y, $mo, $md, $h) = @_;

    if ($y >= 2007) {
        # new DST rules enacted in USA (Arrgh!)
        if ($mo<3 || $mo>11) {
            # No DST if before March or after November
            0;
        } elsif ($mo>3 && $mo<11) {
            # DST April through October
            1;
        } else {
            my $now = &zeller ($y, $mo, $md) * 24 + $h;
            if ($mo==3) {
                # March; find second Sunday and compare
		# This calculation must start with the day before the
		# earliest possible "second Sunday".
                my $start = &zeller ($y, 3, 7);
                $start += 7 - ($start+1)%7;
# my ($ty, $tmo, $tmd) = &unzeller ($start);
# print "DST starts: $ty/$tmo/$tmd\n";
                $start = $start*24 + 2;
                $start <= $now;
            } else {
                # November; find first Sunday and compare
                my $end = &zeller ($y, 10, 31);
                $end += 7 - ($end+1)%7;
# my ($ty, $tmo, $tmd) = &unzeller ($end);
# print "DST ends: $ty/$tmo/$tmd\n";
                $end = $end*24 + 2;
                $now < $end;
            }
        }
    } else {
        if ($mo<4 || $mo>10) {
            # No DST if before April or after October
            0;
        } elsif ($mo>4 && $mo<10) {
            # DST May through September
            1;
        } else {
            my $now = &zeller ($y, $mo, $md) * 24 + $h;
            if ($mo==4) {
                # April; find first Sunday and compare
                my $start = &zeller ($y, 3, 31);
                $start += 7 - ($start+1)%7;
# my ($ty, $tmo, $tmd) = &unzeller ($start);
# print "DST starts: $ty/$tmo/$tmd\n";
                $start = $start*24 + 2;
                $start <= $now;
            } else {
                # October; find last Sunday and compare
                my $end = &zeller ($y, 10, 24);
                $end += 7 - ($end+1)%7;
# my ($ty, $tmo, $tmd) = &unzeller ($end);
# print "DST ends: $ty/$tmo/$tmd\n";
                $end = $end*24 + 2;
                $now < $end;
            }
        }
    }
}

=item time = perl_fixup (time)

On Microsoft Windows systems, Perl is already attempting to correct the
hardware clock, which it assumes is local time, to GMT by adding 4 or 5
hours (depending on DST).
(How is it deciding what timezone we are in?)

So, assuming that the hardware clock is EST, decide whether Perl thinks it is
DST, and calculate the appropriate offset.
The problem is that the same time value could correspond to hardware settings
of either "4-2-95 1:30:00 EST" or "4-2-95 2:30:00 EDT".
We do a &split_time() on time (assumed to be GMT) corrected to EST.
Then, based on &is_dst() of that value, we return a value that matches the
actual hardware clock, now assumed to be GMT.

=cut

sub perl_fixup {
    my ($time) = @_;
    return $time unless $^O eq 'MSWin32';

    my $fixed_offset = $ENV{'ZELLER_OFFSET'} || 0;
    my ($y, $mo, $md, $h, $mi, $s, $wd) = &split_time ($time-$fixed_offset);
    my $dst = &is_dst ($y, $mo, $md, $h);
    $time - $fixed_offset + ($dst ? 3600 : 0);
}

=item (integer, string, string) = get_tz()

Parses the value of the TZ variable (if available) and returns a three-element
list.
The first element is the timezone standard-time offset in hours.
The second element is the abbreviation for standard time in this zone.
The third element is the abbreviation for daylight-savings time in this zone.

=cut

sub get_tz {
    my $zone = $ENV{'TZ'} || 'EST5EDT';
    my (@tz, $offset);
    ($tz[0], $offset, $tz[1]) = split /(\d+)/, $zone;
    ($offset, @tz);
}

=item time = hardware_time()

Returns the number of seconds from 00:00:00 1970/1/1 GMT to the current
date and time as given by the system clock.

=cut

sub hardware_time {
    &perl_fixup (time);
}

=item time = local_time (time)

This function is used to convert a time value that was generated directly
from the system clock (e.g., a filesystem timestamp), to a correctly adjusted
local time value.

=cut

# We convert GMT to local standard time, then adjust for local DST.
sub local_time {
    my ($time) = &perl_fixup (@_);
    my ($offset, @tz) = get_tz;
    $time -= $offset*3600;
    my ($y, $mo, $md, $h, $mi, $s, $wd) = &split_time ($time);
    $time += 3600 if &is_dst ($y, $mo, $md, $h);
    $time;
}

=item string = print_local_time (time)

Same as C<local_time()>, except that the return value is a string in the form
"yyyy/mm/dd hh:mm:ss zone".

=cut

sub print_local_time {
    my ($y, $mo, $md, $h, $mi, $s, $wd) = &split_time (&local_time (@_));
    my $dst = &is_dst ($y, $mo, $md, $h);
    my ($offset, @tz) = get_tz;
    sprintf ('%4d/%02d/%02d %2d:%02d:%02d %s', $y, $mo, $md, $h, $mi, $s, $tz[$dst]);
}

=item string = print_GM_time (time)

Same as C<print_local_time()>, except that the result is expressed as GMT,
not local time.
The return value is a string in the form "yyyy/mm/dd hh:mm:ss GMT".

=cut

sub print_GM_time {
    my ($y, $mo, $md, $h, $mi, $s, $wd) = &split_time (&perl_fixup (@_));
    sprintf ('%4d/%02d/%02d %2d:%02d:%02d GMT', $y, $mo, $md, $h, $mi, $s);
}

=item integer = ztoday()

Returns the zeller number for today, accounting for the local time zone.

=cut

sub ztoday {
    my $time = &local_time (time);
    my ($y, $mo, $md, $h, $mi, $s, $wd) = &split_time ($time);
    return &zeller ($y, $mo, $md);
}

=back

=head1 AUTHOR

Written by Dave Tweed.

=cut

1;
