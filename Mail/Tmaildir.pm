package Mail::Tmaildir;

=head1 NAME

Mail::Tmaildir - "Trivial" maildir implementation,
in contrast to Qmail/Courier-style Maildir.

=head1 SYNOPSIS

use Mail::Tmaildir;

=head1 DESCRIPTION

Manage collections of RFC2822 messages as a single directory of files, one
message per file.
The filenames are of the form "yyyymmdd-nnn.txt" or "class-yyyymmdd-nnn.txt",
where "nnn" is a sequence number used to make the names for a particular date
unique.

At the moment, this module just comprises a set of useful subroutines.

=over

=cut

# History:
#   2007/08/12 DT   Start by pulling together some commonly-used functions
#                   from other scripts.

use vars qw($VERSION);

$VERSION = "0.01";
sub Version { $VERSION }

=item Mail::Tmaildir::mkpath (I<$path> [ I<%options> ] );

Make sure the specified I<$path> exists in the filesystem, by creating
intermediate directories as needed.

I<%options> include:

=over

=item permission => number

Set permissions for the new directories. 0777 is the default value.

=item verbose => boolean

Show changes being made to filesystem.

=back

=cut

sub mkpath {
    my ($path, %opts) = @_;
    my $perm = $opts{'permission'} || 0777;

    my @path = split '/', $path;
    my $newpath;
    do {
        $newpath .= shift @path;
        unless (-e $newpath && -d $newpath) {
            warn "creating directory $newpath\n" if $opts{'verbose'};
            mkdir ($newpath, $perm) || die "can't mkdir $newpath: $!";
        }
        $newpath .= '/';
    } while @path;
}

=item $new_name = Mail::Tmaildir::pick_destination_filename (I<$dir>, I<$pattern>);

Pick a filename in I<$dir> that matches a particular prefix I<$pattern>,
but has a unique serial number in order to avoid colliding with any existing
files.

=cut

sub pick_destination_filename {
    my ($path, $pattern) = @_;

    my $seq;
    opendir (DIR, $path) || die "can't read directory $path: $!";
    my @files = sort grep (/^$pattern-\d+/, readdir DIR);
    closedir DIR;

    if (@files) {
        die "funky filename: $files[$#files]"
            unless $files[$#files] =~ /^$pattern-(\d+)/;
        $seq = $1 + 1;
    } else {
        $seq = 0;
    }

    sprintf ('%s-%03d.txt', $pattern, $seq);
}

=item Mail::Tmaildir::write_file (I<$dir>, I<$pattern>, I<$rtext> [, I<%options> ] );

Pick a unique name in I<$dir> based on I<$pattern> and write the array of
strings I<$rtext> to it.

I<%options> include:

=over

=item permission => number

Set permissions for any new directories. 0777 is the default value.

=item quiet => boolean

Suppress showing the new filename.

=back

=cut

sub write_file
{
    my ($directory, $pattern, $rtext, %opts) = @_;

    # create the directories, if necessary
    mkpath ($directory, %opts);

    $directory .= '/' . pick_destination_filename ($directory, $pattern);
    warn "$directory\n" unless $opts{'quiet'};

    # write the entire message (including header) to that file.
    open (OUT, ">", $directory) || die "can't write $directory: $!";
    print OUT @$rtext or die "can't print $directory: $!";
    close OUT;
}

=item Mail::Tmaildir::move_file (I<$src>, I<$dest> [, I<%options> ] );

Pick a unique name in I<$dest> based on the current filename (last element of
I<$src> and rename the file.

Note that I<$dest> is relative to the directory that the I<$src> file is in.

I<%options> include:

=over

=item permission => number

Set permissions for the new directories. 0777 is the default value.

=item verbose => boolean

Show changes being made to filesystem.

=back

=cut

sub move_file {
    my ($src, $dest, %opts) = @_;

    my @path = split ('/', $src);
    my $filename = pop @path;
    my $newpath = join ('/', @path, $dest);
    mkpath ($newpath, %opts);

    if (-e "$newpath/$filename") {
        # pick a new name for the file
        $filename =~ /^((\w+-)?\d{8})/;
        $newpath .= '/'.pick_destination_filename ($newpath, $1);
    } else {
        # use the existing name
        $newpath .= '/'.$filename;
    }

    warn "moving $src to $newpath\n" if $opts{'verbose'};
    rename ($src, $newpath) || die "can't rename $src: $!";
}

=back

=head1 NOTES

Don't get too attached to this module -- my intent is to switch everything
over to Mail::Box before too long.

=head1 BUGS

=head1 ENVIRONMENT

=head1 TO DO

=head1 AUTHOR

Written by Dave Tweed.

=cut

#'
1;
