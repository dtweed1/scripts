package Mail::Mdir2;

# objects that represent mdirs of mail (or news) messages

# An mdir has Mail::Message (RFC2822) messages in individual files with
# names of the form "subfolder-yyyymmdd-sss.txt".
# Subfolder is one of:
#   huge
#   junk
#   mult
#   offlist
# The sequence number sss can have alphabetic suffixes as well. Typically:
#   f, g, ...          used for forwarded messages
#   r, s, ...          used for replies

# TBD:
#   Locking semantics?

# History:
#   2003/05/12 DT   Inherit from Mail::Folder.
#   2003/11/10 DT   Start to clone from Mail::Mbox2.

use strict;
use vars qw(@ISA);

use Mail::Folder;
@ISA = ("Mail::Folder");

use Fcntl qw(:DEFAULT :flock);
use IO::File;

# use lib '/home1/dtweed/bin';
# use Mail::Message;

# Create a new mdir object from a directory.
# Optionally lock the file for the lifetime of the object.
# if $lock is defined but false, then the mbox is read-only,
# and not updated when DESTROYed.

sub new {
    my ($pathname, $lock) = @_;

    # Scan the directory for messages, but don't read the messages until
    # needed.
    unless (opendir (DIR, $pathname)) {
        # OK if directory doesn't exist; we'll create it later if necessary
        warn "can't open $pathname: $!";
        my $self = {
            filename => $pathname,
            handle => undef,
            readonly => defined $lock && !$lock,
            msgs => [],
        };
        bless $self;
        return $self;
    }
    if ($lock) {
        # TBD
    }

    # build the array of messages
    # TBD: what about suffix letters?
    my @dir = sort grep /^\d{8}-\d{3}.txt$/, readdir DIR;
    closedir DIR; # unless $lock;

    my @msgs = map {
        # TBD: reformat the filename to resemble gmtime()
        # TBD: any information to be gleaned from the timestamp?
        # Dow Mmm dd hh:mm:ss yyyy
        from => "- $_",
        text => undef
    }, @dir;

    my $self = {
        filename => $pathname,
        handle => $lock ? undef : undef,
        readonly => defined $lock && !$lock,
        msgs => \@msgs,
    };
    bless $self;
}

sub DESTROY {
    my $self = shift;

    return if $self->{'readonly'};

    # Count the remaining messages
    my @msgs = grep defined $_, @{$self->{'msgs'}};

    # If there are none, just unlink the file if it exists
    my $fh = $self->{'handle'};
    my $name = $self->{'filename'};
    unless (@msgs) {
        return unless -e $name;
        warn "unlinking $name\n";
        unless (unlink $name) {
            warn "can't unlink $name: $!";
        }
        close $fh if defined $fh;
        return;
    }

    # if the file was locked (and left open)
    if (defined $fh) {
        # rewind and truncate it
        unless (seek ($fh, 0, 0)) {
            warn "can't rewind $name: $!";
            return;
        }
        unless (truncate ($fh, 0)) {
            warn "can't truncate $name: $!";
            return;
        }
    } else {
        # reopen it for writing
        warn "reopening $name\n";
        $fh = new IO::File;
        unless (open ($fh, ">$name")) {
            warn "can't write $name: $!";
            return;
        }
    }

    # write the remaining messages in the array back out to the file
    for my $m (@msgs) {
        my $rtext = $m->{'text'} || [];
        print $fh $m->{'from'};
        # escape ">*From " lines by adding one '>'
        for (@$rtext) {s/^(>*From )/>$1/}
        print $fh @$rtext;
    }

    # close it
    close $fh;
}

1;
