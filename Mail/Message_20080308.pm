# Message.pm - objects that represent mail (or news) messages

# A message has a header part and a body part.
# If the body is multipart, it is stored as an array of messages; otherwise,
# it is stored as a single string.

# Data structure:
#   raw_header => Reference to array of strings, containing either original
#                 header lines or updated lines. In either case, line breaks
#                 and terminating newlines have been removed.
#   header =>     Reference to a hash containing header values, keyed by
#                 header field name. Most are references to arrays, containing
#                 all of the values found for that name, in sequence. Some of
#                 the MIME headers (content-type) are further broken down and
#                 have more structure.
#   raw_body =>   Reference to array of strings representing the entire body of
#                 the message (but not the blank line separating it from the
#                 header). Newlines still exist at ends of lines.
#   body =>       Either a string (no MIME, identical to raw_body) or
#                 reference to array of messages.
#   opts =>       Reference to hash of options passed to new().
#      fix_base64 => boolean   Attempt to fix up short base64 lines.
#      ismime => boolean       Message is known to have MIME structure.

# To do:
#   For plain text bodies: signatures? uuencoded inclusions?
#   Move check_body() to a separate module?
#     also need to check headers
#   Special handling for large messages?

# History:
#   2008/03/06 DT   Add the ability to decode an enclosed message.
#   2005/03/02 DT   Handle multipart messages that have no blank line before
#                   the first multipart boundary.
#                   Ignore non-text body parts when scanning for spam patterns.
#   2004/11/27 DT   Handle creation of a new empty message.
#   2004/08/01 DT   Allow extra stuff after mime-version.
#   2004/07/15 DT   Handle multipart messages with no boundary specified.
#   2003/05/26 DT   Defer parsing body until necessary.
#   2003/03/19 DT   Add options to new(), fix_base64.
#   2003/03/09 DT   Show detail in check_body() on multi-part messages.
#   2003/02/26 DT   Check for bad syntax in content-type parameters.
#   2003/02/24 DT   Fix check_body() to treat text as one line.
#   2003/01/25 DT   Change describe() to return a string instead of printing.
#                   Start working on as_strings().
#   2003/01/11 DT   Fix uninit var warning in check_body().
#   2002/12/16 DT   Streamline check_body(), add optional detail report.
#   2002/12/05 DT   Fix up special treatment of content-type header hash.
#   2002/12/04 DT   Add more spam triggers.
#                   Allow spaces around the '='s in Content-type: attributes.
#   2002/12/03 DT   Fix bug related to decoding single-part bodies.
#                   Fix bug related to parts with no newline on last line.
#   2002/11/23 DT   Return empty array if no header() of a particular name.
#   2002/11/11 DT   Implement hdr_decode().
#   2002/11/10 DT   Put \Q...\E around boundary strings.
#                   Change ->addressees return value.
#                   Add ->senders.
#   2002/11/08 DT   Add MIME support.
#   2002/06/13 DT   Pull out from fetch.pl

package Mail::Message;

use strict;

# use lib "c:/dtweed/bin";
use Mail::Address;

# Create a new mail object from a string or an arrayref (of strings)

sub new {
    my ($rtext, %opts) = @_;

    my @raw_header = ();
    my @raw_body = ();
    my %header = ();

    if (defined $rtext) {
        if (ref $rtext) {
            unless (ref $rtext eq 'ARRAY') {
                die "bad reference to Message::new ($rtext)";
            }
        } else {
            # TBD: For very large messages, we don't really want to split the
            # entire message into lines and then join all but the header back
            # together again.
            # (Not a problem for fetch.pl, because it passes a reference)
            # Don't use the split; it removes the newlines from the lines
            # $rtext = [split /\n/, $rtext];
            $rtext = [$rtext =~ /[^\n]*\n?/g];
        }

        # Pick the header out of the message and fix up continuation lines
        my $hflag = 1;
        foreach (@$rtext) {
            if ($hflag) {
                if (/^\s*$/) {
                    $hflag = 0;
                } elsif (/^--/) {
                    # multipart boundary embedded in header
                    $hflag = 0;
                    push @raw_body, $_;
                } else {
                    push @raw_header, $_;
                }
            } else {
                push @raw_body, $_;
            }
        }
        my $temp = join ('', @raw_header);
        $temp =~ s/\n\s+/ /g;
        @raw_header = split ("\n", $temp);
        for (split ("\n", $temp)) {
            # TBD: verify success of split
            my ($name, $value) = split (': +', $_, 2);
            push @{$header{lc $name}}, $value;
        }
    }

    my $self = {
        raw_header => \@raw_header,
        header => \%header,
        raw_body => \@raw_body,
        opts => \%opts,
    };
    bless $self;
}

# Parse the MIME headers in a message and interpret the raw_body as
# appropriate, creating the body structure

sub parse_body {
    my ($self) = @_;
    my ($hdr, $raw) = ($self->{'header'}, $self->{'raw_body'});
    my $ismime = $self->{'opts'}{'ismime'};
    my $body = join '', @$raw;

    if (exists $hdr->{'mime-version'}) {
        $hdr->{'mime-version'} = hdr_decode ($hdr->{'mime-version'}[0]);
#        if ($hdr->{'mime-version'} eq '1.0') {
        if ($hdr->{'mime-version'} =~ /^1.0/) {
            $ismime=1;
        }
    }

    if ($ismime) {
        # If the body is encoded, decode it
        #     Content-Transfer-Encoding: <mechanism>
        #          7bit
        #          8bit
        #          binary
        #          base64
        #          quoted-printable
        if (exists $hdr->{'content-transfer-encoding'}) {
            $hdr->{'content-transfer-encoding'} =
                lc hdr_decode ($hdr->{'content-transfer-encoding'}[0]);
            if ($hdr->{'content-transfer-encoding'} eq 'quoted-printable') {
                $body = decode_qprint ($body);
            } elsif ($hdr->{'content-transfer-encoding'} eq 'base64') {
                my @temp = split /\n/, $body;
                if ($self->{'opts'}{'fix_base64'}) {
                    # A common flaw is that a leading '0' on a line has been
                    # dropped somewhere along the way. If a line is short by
                    # one character, try adding it back in.
                    for (@temp) {
                        if ((length) % 4 == 3) {
                            $_ = '0'.$_;
                            warn "base64 line fixed\n";
                        } elsif ((length) % 4) {
                            warn "bad base64 line not fixed\n";
                        }
                    }
                }
                $body = decode_base64 (join '', @temp);
            }
        }

        #     Content-type: <type>/<subtype> [ ; <param>=<value> ... ]
        #          multipart/alternative; boundary="string"
        #          multipart/mixed; boundary="string"
        #          multipart/related; boundary="string"
        #          multipart/report; boundary="string"
        #          text/plain; charset="iso-8859-1"
        #          text/plain; charset=US-ASCII
        #          text/html; charset="iso-8859-1"
        #          message/delivery-status
        #          message/rfc822
        #          application/octet-stream; name=valign.scr
        # Note: Can't hdr_decode() this one because the boundary string can
        # be pretty much anything. TBD: May have to hdr_decode() individual
        # fields after breaking them up.
        if (exists $hdr->{'content-type'}) {
            my @temp = split /;\s*/, $hdr->{'content-type'}[0];
            # TBD: verify success of split
            my ($type, $subtype) = split /\//, lc shift @temp;
            my %attr;
            for (@temp) {
                if ((my ($name, $value) = split /\s*=\s*/, $_, 2) == 2) {
                    $value = $1 if $value =~ /"(.*)"/;
                    $attr{lc $name} = $value;
                } elsif ((($name, $value) = split /\s*:\s*/, $_, 2) == 2) {
                    # Apparently some mail clients are taking liberties
                    # with the syntax
                    $value = $1 if $value =~ /"(.*)"/;
                    $attr{lc $name} = $value;
                } else {
                    # Ignore malformed parameter strings
                }
            }
            $hdr->{'content-type'} = [{
                type => $type,
                subtype => $subtype,
                attr => \%attr,
            }];

            # If the body is multipart, pick it apart and process the parts
            # (recursively)
            if ($hdr->{'content-type'}[0]{'type'} eq 'multipart') {
                my $boundary = $hdr->{'content-type'}[0]{'attr'}{'boundary'};
                if (defined $boundary) {
                    # Note optional inclusion of the leading newline, which
                    # allows badly-formed messages with two boundaries on
                    # successive lines, and also takes care of the no-preamble
                    # case for us.
                    # TBD: verify success of split
                    # TBD: Beware of hierarchical multipart messages that use
                    # the exact same boundary for different parts! Obviously,
                    # the simple split() is not adequate for that case.
                    # We'll have to look for the LAST terminal boundary to get
                    # our epilogue, but then how do we distinguish the inner
                    # non-terminal boundaries from the outer ones???
                    # For now, we simply assume they're all outer parts.
#                    my ($main, $epilogue)
#                        = split /\n?--\Q$boundary\E--\n/, $body;
                    my @chunks = split /\n?--\Q$boundary\E--\n/, $body;
                    my $epilogue = @chunks > 1 ? pop @chunks : '';
                    my $main = join "\n--$boundary\n", @chunks;
                    # TBD: verify success of split
                    my ($preamble, @raw_parts)
                        = $main
                        ? split /\n?--\Q$boundary\E\n/, $main
                        : ('', ''); # TBD: quick hack to kill error message
                    my @parts = map new ($_, %{$self->{'opts'}}, ismime => 1),
                              @raw_parts;
                    $body = [$preamble, @parts, $epilogue];
                }
            }

            # If the body is itself a message, process it and add it to the
            # hierarchy. In this case, we don't know whether the enclosed
            # message is MIME-encoded.
            if ($hdr->{'content-type'}[0]{'type'} eq 'message') {
                $body = ['', new ($body, %{$self->{'opts'}}, ismime => 0), ''];
            }
        }

        # Other MIME headers
        #     Content-Class:
        #          urn:content-classes:message
        #
        #     Content-ID: <SVI94dD8s3Y85>
        #     Content-Disposition:
        #          attachment; FileName = "illegalSex.zip"
    }
    $self->{'body'} = $body;
}

# Given the name of a header field, return an arrayref to a list of values

sub header {
    my ($self, $name) = @_;

    $self->{'header'}{lc $name} || [];
}

# Return the mssage ID

sub id {
    my ($self) = @_;

    $self->{'header'}{'message-id'}[0] || '';
}

# Return all of the values in the "To:" and "Cc:" fields

sub addressees {
    my ($self) = @_;

    my @temp;
    push @temp, @{$self->{'header'}{'to'}} if defined $self->{'header'}{'to'};
    push @temp, @{$self->{'header'}{'cc'}} if defined $self->{'header'}{'cc'};
    extract_addresses (@temp);
}

# Return all of the values in the "From:" field(s)

sub senders {
    my ($self) = @_;

    my @temp;
    push @temp, @{$self->{'header'}{'from'}} if defined $self->{'header'}{'from'};
    extract_addresses (@temp);
}

# Replace an existing header line

sub header_replace {
    my ($self, $name, $value) = @_;

    $self->{'header'}{lc $name} = [$value];
    # TBD: update raw_header as well
}

# Add a header line at the beginning

sub header_prepend {
    my ($self, $name, $value) = @_;

    unshift @{$self->{'header'}{lc $name}}, $value;
    unshift @{$self->{'raw_header'}}, "$name: $value";
    # TBD: break long lines in raw_header
}

# Delete a header field altogether

sub header_delete {
    my ($self, $name) = @_;

    delete $self->{'header'}{lc $name};

    my $rawref = $self->{'raw_header'};
    @$rawref = grep !/^$name:\s/i, @$rawref;
}

# Trim all lines including and following one containing a given regular
# expression (does not affect the parsed body)
sub raw_body_trim_re {
    my ($self, $re) = @_;

    my $raw = $self->{'raw_body'};
    my $j;
    for my $i (0..$#$raw) {
        $j = $i;
        last if $raw->[$i] =~ /$re/i;
    }
    return unless $raw->[$j] =~ /$re/i;
    # TBD: remove blank lines ahead of this pattern as well
    splice @$raw, $j;
}

# Describe the structure of a message for debugging or logging

sub describe {
    my ($self, $indent) = @_;
    $indent ||= '';
    my $output = '';

    for (keys %{$self->{'header'}}) {
        my $val = $self->{'header'}{$_};
        if (ref $val eq 'ARRAY') {
            if (@$val > 1) {
                $val = @$val;
                $val = "($val values)";
            } else {
                $val = $val->[0];
            }
        }
        if (ref $val eq 'HASH') {
            my $type = $val->{'type'};
            my $subtype = $val->{'subtype'};
            my %attr = %{$val->{'attr'}};
            $val = "$type/$subtype";
            for (keys %attr) {
                $val .= "; $_=$attr{$_}";
            }
        }
        $val = '<<undef>>' unless defined $val;
        $output .= "$indent$_: $val\n";
    }

    $self->parse_body unless defined $self->{'body'};
    my $body = $self->{'body'};
    if (ref $body) {
        my @temp = @$body;
        my $preamble = shift @temp;
        if (defined $preamble) {
            my $len = length $preamble;
            $output .= "${indent}Body preamble: $len bytes\n";
        }
        my $epilogue = pop @temp;
        if (defined $epilogue) {
            my $len = length $epilogue;
            $output .= "${indent}Body epilogue: $len bytes\n";
        }
        for (0..$#temp) {
            $output .= "${indent}Part $_:\n";
            $output .= describe ($temp[$_], $indent.'    ');
        }
    } else {
        if (defined $body) {
            my $len = length $body;
            $output .= "${indent}Body: $len bytes\n";
        } else {
            $output .= "${indent}Body: (none)\n";
        }
    }

    $output;
}

# Deliver the entire (updated?) message as a string
# TBD: put the (modified?) body back together

sub print {
    my ($self) = @_;

    # Note that raw_header has no newlines, while raw_body does
    join ("\n", @{$self->{'raw_header'}}, "\n")
        . join ('', @{$self->{'raw_body'}});
}

# Deliver the entire updated message as an array of strings
# TBD
sub as_strings {
    die "Mail::Message->as_strings not ready for prime time";
    my ($self) = @_;
    my @output;

    # TBD: analyze the body structure and make sure MIME headers are correct
    $self->parse_body unless defined $self->{'body'};
    my $body = $self->{'body'};
    if (ref $body) {
    } else {
    }

    # the header
    # TBD: any particular order?
    for my $name (keys %{$self->{'header'}}) {
        for my $value (@{$self->{'header'}{$name}}) {
            # TBD: capitalize $name in the conventional manner
            # TBD: break long $value lines
            push @output, "$name: $value\n";
        }
    }

    # the body

    @output;
}

# Deliver the entire body as a string or a reference to an array of parts
# (preamble and epilogue deleted)

sub body {
    my ($self) = @_;

    $self->parse_body unless defined $self->{'body'};
    return $self->{'body'} unless ref $self->{'body'};

    my @body = @{$self->{'body'}};
    shift @body;
    pop @body;
    \@body;
}

# Check for certain virus or spam characteristics

sub check_body {
    my ($self, $detail) = @_;

    my $body = $self->body;
    return '' unless defined $body;

    if (ref $body) {
        # This is a multipart message - check for executable content
        for (@$body) {
            # TBD: this needs to be cleaner
            # TBD: some multipart spams have no part headers at all
            $_->parse_body unless defined $_->{'body'};
            my $content = $_->header ('content-type');
            next unless defined $content && @$content;
            $content = $content->[0];

            if ($content->{'type'} =~ /audio|application/
                && defined $content->{'attr'}{'name'}
                && $content->{'attr'}{'name'} =~ /\.(bat|com|exe|scr|pif)$/
                ) {
                return 'Virus!';
            } elsif ($content->{'type'} =~ /text|multipart/) {
                my $temp = check_body ($_, $detail);
                return $temp if $temp;
            } else {
                # ignore non-text body parts
                return '';
            }
        }
    } else {
        # This is probably text
        for my $pattern (
            # most recent new websites
            qw/ 
                everawen eggsinbasket
                strangecollective newlevitrapharmacy mwqz\.com fallingdfungus
                watchez strangeflower blingwatch tabsinfofor sex-addicts
                shop4system nowbymail logingcramp banahoder fearendshere
                quickvisitorsnow ruidecle diggingdawg pelvics\.net realego
                wwdoctor myproducts4sell lotsavisitorscheaply uneaten\.net
                wildmail shop4direct bucketed\.net vancedwanow 45right
                replicabargainbasement richsam\.com whiteteeth hpfwatch
                connectwrap getvisitors4less indicejural realmat\.com
                sh0ots\.com grudgingly\.net ezrates mobnight nownowhey
                vulcanise\.net royalgrouprealty tokens4free teamlifeinsurance
                247choicez 62mort\.net reascends\.net shopmyshow e-posten
                whypaym0re highlydemandedmedz moonmin\.info moonboard\.info
                remorgnow jobsjobs acarchformdc pjew\.com 247systems ppeq\.com
                onereplica ownsoft\.net fossilized\.net techdms nereh\.info
                gesart\.info heruk\.info brucereeattax goodreplicaz pi11\.com
                MaxPay pr3tty globalproducts emailpowerblast valentlne\.com
                lithome buymaxxlength agawet\.com phoenician1 annualised\.net
                mp-system r1c3\.net pdirefi premiumdvdsstore modelled\.net
                burntbypens cassidythedog TopDownloads pr0udness r1c3\.com
                wemake-it-easy hathawayglobal rakuten-news planningwrap uxxemm
                ecigs4less easyrx-4u pockmarked\.net watchzbestprice fiphast
                bugbumps fastlow-rates rates-lowered watchezbestprice 247elite
                commodious\.net check4choice simple-refi servicelifeinsurance
                killspyware livelifeinsurance cloudsarefluffy q67\.net teljar
                220\.130\.129\.235 palns\.net ownl\.com prlce\.com antispynow
                orderhydrocodonepharmacy pockmarked\.com bestpricewatchz
                degradedly\.net redmax\.jp thefunding adv-18vision superrefico
                getvisitorsquick paln\.org przc\.com tabletsforall wallloan
                dumfounds ultimatereplicas soggiest\.com joewein bwform\.com
                officialebayinfostore newhealth-pills nospysoft ebookgoldnow
                uncapping\.net homeloankey pochta\.ru val10\.com vaiium\.com
                officialreplicas erevansoft obliquenon BestBuyWatchezOnline
                arkilo\.com chorally\.com replacesoft ossifies\.com topwinsoft
                pillsforcheap waphealth degradedly\.com autoknivesnow osseously
                genuinereplicawatchez lipase\.net prserv\.net skysalononline
                targtraffic noxiously\.net felmuh\.com lipase\.nte huppence21
                cuttingedge plentyofvisitors wwwgethits iprohealth holdoutcap22
                cordoned\.net look4connection biotichealth medianly healthdo
                smilingly\.net bushealth aidemail nonlifenj jansenisten Sniffex
                schnazzyboot curdle\.net tpbaj\.com drizzlier\.com 65net\.info
                bestwatchez stankfinger healthfrom loseweightsystems healthram
                indeedgood medsrealcheap springmulch smokeethebear partied\.net
                unpardoned\.net prefered-loans click4source omnipod\.com 
                defrauding\.net confuting christianhealthcenter diarrhoeal
                incdropfat revetments paperiness jalkarnc pessimistnm furbelows
                allyourpromoneeds marswebsite mchammers\.com misterteepee
                qualitywho skulkered unspotgi genuinely\.net friggings intelamd
                howsoever\.net cauterizes\.net provencaux ucanimprove bornfruit
                getgoodtraffic punanipunch maximum-loans whywaitlonger
                pullpushstop grouploseweight endosonic gossipy\.net gravesides
                snorter\.net depletive\.com qualitydr bulwarking polarizational
                centerhealthinfo urwatchconnection4u theweightlosssearch
                homebuziness soddenness impactions zazxox healthpronews zazxpx
                kellykapowski lover-kiss the1swisswatch chooseyourwatch4u
                walepiececc dropfatnews ladytoysite jeffersondarcy dusked\.net
                stetmlstet zigzagging\.net hassle-free-loans correctspeling
                lessthanless inetwebtraffic enhancemypackage katchemack
                lorryload hybridisms hurtfully\.com stagedcn aconitumil
                perfectwatch4u untyred homelandliberty hi-power\.cc aolkeywerdz
                tobeatdameat ucanttouchfist tatankarules yourfitnessonline
                letmeseethelight americasplan r8tesdrop enhancemetoday
                certifiedreplicas trafficyoursite yourhealthoffice arolex4me
                fjtrsfer yourfitnessfirm keepingtimeinstyle doukacro visagraph
                timelyreplicas r8tdrop violentporno ecomomics\.net globaltelkom
                finallyseven watchesbrand photoshop-video-teacher dreamreplicas
                hitsforyoursite 1clickagency gallanted awebworld truthfu1ly
                famousloaves foursamurais electioncrazyone weightlossfirm
                personalwatches elitereplica liverymen\.com replicas4me
                bubx.x\.info swisstime4us smoothwatches 1clickgallery l73\.net
                value-loans tatzqz c0meh3re 1clicktown rolexinspirations
                salver\.info gr3atworld lowest-rate-loans 1clickexpress 
                famousreplicas backgammon65 jettisonit mortmarket khitsnow
                low-rates-usa loan-quotes loans-america finishline-solutions
                safer-meds massivethree n1c3\.com instant-quotes hottiesonline
                earnwithfaxbiz compepills area-rx cra3y\.com guacofknhb
                addyourprofile thewebreporter2005 thistabs glbpharma pillsonus
                t1me2sav goodratez gr8sav3 masswebsitehits n0wwewillsave
                america-financial [a-z]\d[a-z]\.net [a-z][a-z]\d\.net ftke\.com
                realdatepersonals loans-offered enterthekem1st loans-your-way
                tallish\.net himlove loans-approved procareplus loans-for-you
                uptabs\.com ibtz\.com true-offers mrlend123 more-bills \d+-m-n
                inpillform ltxmeds mrratenow cheap-lender m0rtgagez y-2-m\.com
                photoshop-teacher wetbudapest topsaleofthedays secretmaker
                backdoorhoney puerperal beadwindow salezsalessz grx-meds
                pharmaway drugsbox mandstart flashdrgs myyv\.com guzzling\.net
                spycleaner cmbimebyee 123lenderz loans-r-easy loans-4-u
                jncucullabb freshdomains visitstoyoursite oursiteonline
                reedsims rynoplasti drivers88 rythymloose scenting\.net
                american-loans \.com\/p\/viks ejmichealde ok-ref-now its-simple
                merchantglobalx c64zmlc lw-mrt\.com topsaleplace jubr\.com
                salehouseoflove ru6\.net pflzer\.com DOMAIN_FOR_MAILING
                mort-today cheapoemz4u vfoir9834 dotherx shyandnot ultrameds4u
                yourreplicas nowratez dfspot\d+ mrtg-today gc-nine salesstores
                39jdssl 0bese r0che\.com diaze\.com mrlendez doesrx\.com
                spyware-exterminator e-m-now m-tg-today best-mtg mrt-now
                educate-\d+ quicklikemedstou fior9333 gga4\.info r-fi-now
                easy-finance qklenders herpills oeoutju4 finance-forever
                seasonmeds smokersatwar quickfast fastmail your-re-finance
                simple-finances home-loans getmyplace quickie\d+ com\.\w+\.com
                bolosko xoloxo website-needs-traffic unkindest gr8lendez
                fasteratez medabo[a-z] fds66\.com wwbw\.com medalo[a-z]
                runninshit SUNLITEPHARMACY low-mo-rt-gag-e vaguer colltabl
                moretabl fastlenderz medlo[a-z] creditrepair peopleiq
                officerlas c-u-i crisisinfo lastyeathat sketchpharmacy
                beautifulglimpsesof stemminfro jkcddfemm witpoli offer4meds
                aboutanyotherform starswereseen 123ratez matterotherthanablac
                ofthemasswithavery low-rates-for-you dfglkimie lineinc
                lenderz4you andorbitalpaths amnignanf greatrarez easy-as-1
                hugthehopper ilovemeds enjhejnfj clfmbeiie abletotrackthe
                great-med-price qualitymeds ebiz-mktg rateznow efcknhhnm
                medbo[a-z] aboutstamina frty1 home1enders easy1oans nfrj384
                nupharidol TAKEMYRX werwer rz3e23fzf morree smallrx tehexpertz
                bestlenderz w0wo pharmbof justpills phentemine r0d\.net
                grrrrreat suparates nllhcfdh sogoodrates cyberclik KEYWESTRX
                icccmika pharmded mortslow zoo345 pharmpod CHARTERMEDS pharm.od
                jrz874383w pharmbig web-promo mortzlow lendzx discounthaven
                net-internetstore aboutslim freekin savingcashola pharmlet
                travellers4saul pharned pharmtet spam-cash cdsparadize MYXRX
                lowzratez more-internetstore freeinfoisnomore menteurpills 1oem
                againandagain dohernow integrisales morpheus-spyware ruojrxor
                pharmacyy1 getpharmtoday cheapmedusa PILLBITS pain-relief
                funding-loans financing-loans pdfmrjdnhrj ggg6 clikbusiness
                qpoeruel4 hsewsxcvb hdreuifpl newquotes pharmom strippppnow
                atheholygrail rfxoyi33 pleasepills apparentlyseventy pharmcool
                www\.193 newyorkmedz cheapsales2u honestsalesnow pingopongos
                illumanated wdre24-454 agravitationalpull ljgckfebbh pharmmain
                bbbiibffeh ggheicjgii quikmort fnaiegfmfm kfcidamjfg thinktabs

                csuhqwiyeo cakjsldkjw saveurcashola deysudikfmc thenetbiz
                cdnmxsjdheyr vigra4 w-e-b-promo machomall pickrx bptunsr
                serenely7sibyls pokerunner promo-for-your-site blueriseny
                just-mort-gage TRAITREPILLS atameetingofthe ez-rate servetabs
                godatetonight godatesunday godatetoday epicoff3rs aimspyware
                binarnnn Mustbegood hhtt-rr bcnystwtdio bwtsydhxnstw poqeoiru6
                bszxawir rugeri lowest-mort m3d2u m3dspective researchisneeded
                MEDSSOIN softsalenow wowhealth toconfirmthe CACHERX chillnow
                usingimages great-mort-gages per-fect-mor-tgage 211.235.241.129
                lbmfdlmdba fkidiknije spyware-software seeforyourselves
                pilottown datesaturday stock-notification chilltoday kamkbfhnjf
                bestsite hookupinnotime mytabl allthexpsoft fitnessbiz basqwpl
                say-yes aldringo abncuitof albruni 999-best xe4.net
                educate-1 stuffthatwork bestpharmacy mokatilatiano thesherriff
                amazingrate ratesnow masfre horalustaterista yesmeds xsupersite
                lovtabl whoarates 123bargain webtime-now toseit portumonako
                getyourdhere adoredarling ohsolowrx eazymedds juharistolato
                perofficdocumentation othfollowi mandatoradministrati mahndjni
                hiddenz crockolate futurebadlove bolakursova wowrates tordel
                speshials getyourbhere caymanwalls aujobs smartbuyers fundegree
                PRIVATEINFOPRESCRIPTIONS GOGOM3DS MEMBERSONLY JACKETMEDS
                MEDS4LESS M3DSGOGO DRSMITHDRUG5 CRAZYMEDSKEWL DRPHILMEDICATION
                wallofcorals lots-a-promo goforthesoft lowrates
                ihgahgsb iasdvba iadghhg isytgquy guretyu thoeiuie rihjsh
                eufsabefse eutisbdfbed
                cooltabl gpwqtsavs ewisdsbfee lokjks wetdear 24-7medz
                wetsweetheart stayupontime gakdhikh gaqwsncv gadtftha
                metyys 23mnsa nnnbvnc dibefjsefed domanamregistrat
                bfear neata kyqdv phyyuas rihjks higghsww
                apocryphal.gumption icand.hilt wetyyus fppksk yourtabl karma66
                numeration.shakable roofingrangers lowerhealt baosifewh
                reliablemed bestreplica delivfast fasturnaround mmwatrrtys
                secrrkk chroddii pricechoppers mmmnan basdfkjlke boeirisudb
                want-traffic fastdelivs ftqqbip wetparts fjjpgro getitnow
                representsrant hottylaugh marlmerlmarvmexer online-sales
                transactionjunction hottyground beckiter dajwtjm dzpmvqp
                companionjokes lowestrates officesoft incrediblevisions
                companionground knibindb mnlenekn leadloanlendlastless univm
                perfectperfect onemoretimez fim777 fromtimetotimeitsnothere
                cfefogl csvrldd czgcauz cdmsdts date-jump newholidayplans
                annualteamworkdrive qeekcy needsaslap namyouselec renshouse
                UPSDELIVERMEDS esaleshavetripledinmarch santasrates amhang
                platinumswiss EUROPEM3DS smartreplicas bestxpoem auniv
                naughtyjump frommetoyousoon promotion-you-need pimpslapzz
                got-web-site-traffic 1100i need-web-sales web-promo-max frs23
                abc-soft BESTMDICINE bestpills besthealthpills want-promotion
                yourstopishere flashexperience xmassavings hereitgoesoncemore
                xmastimerates knhhddia mrrefinancing meeting-smile itsmylantern
                cheapsoftshop pedlomaniak xpforyou inbedwithcoolstars antimeds
                kirticritgh citywidedownloads cityscapepharmacy coolresource
                nepel wifesmile randomnameisgood anyoneofus infoemail-web
                wifeground medzstoreonline mortzgagez microwavegesture mortzz
                downloadnetwork sweet-lips dimur datingdeliveries bestxpshop
                imsodamtired all-the-watches thebestmortage funwinterdownloads
                oflist lavadateing chillblow faztsitez medz-store hesatosser
                giantquote em191799 allthegood neoblender dncru imissmybelly
                xmaslowrate beatly bederical firstoem coebvgfuse removeurl
                frogsaregoodfood bestsoftshop thexpertmot ismorea enameit
                mygcboard panelthecompany lowmorgage mrkfin gctousa
                s3curesripts asadoprego youngfreespirits findfunhere 50megs
                santa200 dn-ice-boat mynewwatch imleavingyou dynamicdrive
                eoinf noscr1ptsn3ed3d s4veonl1ne pcsri originalreplica
                hostcity customguru martinisblue extramed buythemnow supramed
                dontriskyourlife meridian2opaque bestwinsoft greencvista
              /,

            # mis-configured spammers
            qw/ %BODY %URL %PARAGRAPH %SITE_URL
              /,

            # software
            qw/ compare\s+our\s+soft\s+with\s+others 
                install\s+and\s+enjoy
              /,

            # stocks
            qw/ 
                Ring\s+Ring
                Guangzhou
                News\s+For\s+Investors
                Allixon 
                SmallCap.Investors
                WWBP\.OB
                Oretech
                Hot\s+Sectors 
                Investor\s+ALERT 
                Advanced\s+Powerline\s+Technologies
                Modern\s+Technology\s+Corp 
                Energy\s+and\s+Asset\s+Technology
                Southwestern\s+Medical\s+Solutions
                Media\s+Gr0up 
                Tiger\s+Team\s+Technologies
                Pop3\s+Media\s+Corp
                OTC-TTMT pink\s+sheet Rodney\s+Chesson Watch\s+This\s+Stock 
                Companies\s+That\s+Explode News\s+at\s+The\s+Close 
                Section\s+27A 
                Oil\s+and\s+Gas\s+Advisory
              /,

            # African money scam
            qw/ million.*(dollar|usd) late\s+husband nigeria congo
                sierra\s+leone lucrative\s+business\s+venture 
              /,

            # Common sex topics
            qw/
                hot\s+college\s+girls
                \bpenis\b \berotic\b \bmanhood\b gotmeadate
                justinaflash 1am4eyes redgingers
              /,

            # drug websites
            qw/ 
                premature\s+ejaculation 
                teeth\s+whitening
                Internet\s+pharmacy
                Softabs
                SPUR-M natural\s+male\s+enhancement 
                common\s+cold\s+is\s+a\s+thing quality\s+med+s 
                ALL\s+known\s+deadly\s+Viruses 
                virus.bacteria.free.living Kills\s+all\s+Known\s+Viruses 
                Captain\s+Blood oral\s+lozenge hard\s+rock\s+erection
                Complete\s+your\s+order\s+here
                Safest\s+Pharm medication\s+network we\s+ship\s+world\s+wide
                online\s+pharmaceutical\s+order
                cool-hoop hoopdirect hoopcenter find-hoop
                medscustomer choice-is-yours
                froidnet igghsent
                generic.pharmacy
                \bambien\b ativan allegra
                buspar
                celebrex \bcialis\b c1al1s 
                didrex
                flonase
                hgh
                levitra
                \bmeridia\b
                nasacort nasonex nexium
                phentermine prilosec propecia
                \brenova\b \bretin\b
                valium valtrex viagra vicodin vaigra
                xanax xenical
                zanaflex zyban zyrtec
                0nline phar-macy
                miracle\s+protein 
                kills\s+all\s+known\s+deadly 
                visit\s+us\s+and\s+see\s+more
                inches\s+or\s+(get\s+)?your\s+money 
                prescription\s+required
                her-bal yourhealthmatters websitetheyourshop
                onlinerxmed sphericalmeds babrjy izbaerdd totalfithealth
                iabewrdf net-meds unkempt1seldom the9strews farmersfound
                l0west CanadianPharmacy neck4conveyance nozvneed perfectvgr
                peoples-medicine-network macromed bringtruemobility
                bestpharmonline tensecondsasyou withoutaprescription
                reverse6curl imfreetonight researchsupport suffragan
                wellgetitdone
              /,

            # Common lending topics
            qw/
                Fastest\s+approval\s+process\s+ever
                SouthTrust 
                morta\(ge 
                lenders\s+compete 
                current\s+financial\s+loan 
                present\s+loan\s+situation
                exisiting\s+loan\s+situation 
                exisiting\s+home\s+loan 
                current\s+home\s+loan
                present\s+home\s+loan
                the\s+best\s+rates\s+in\s+the\s+US
                refinance mortgage \bequity\b dear\s+homeowner 
                bestcorporate priceisright market-the-web letlenderscompete
                play-the-market moneyclever
                bigmamma gooddog m0rtgage
                fujustsi1 kbagbafb refiinternet webrefi moneystates debt-states
                mort-loa-ns carokmandal awesomelender hotcap mort-g-age-now
              /,

            # Music-related sites
            qw/ kitsapbands harmoniemusik musicscene realwerks yourfilenetwork
              /,

            # product research
            qw/ product\s+research\s+panel 
                test\s+and\s+keep 
                domain670
              /,

            # social engineering
            qw/ amount\s+of\s+viruses
                get\s+a\s+capable\s+html\s+e-mailer
                killadwareco antispaware freespywareware
              /,

            # software
            qw/ hemisoft softheaven DOWNLOAD\s+Superstore 
                Windows\s+XP\s+Professional 
                The\s+Download\s+Center
                Grow\s+your\s+computer 
                PC\s+will\s+thank 
              /,

            # knock-off watches
            qw/ watchs bestwatches qutewatches afeet
                you\s+could\s+own\s+a\s+Rolex 
                genuine\s+Rolex
                Rolex\s+Watch
                Rolex\s+Replica 
                Replika\s+Watches 
              /,

            # Misc
            qw/ 
                Customer\s+Care\s+Specialist
                Genuine\s+College\s+Degree University_Degrees
                back\s*up.*dvd copy\s+dvd
                veggiegear libero\.it smoking\.com
                this\s+is\s+not\s+spam
                you\s+have\s+receiv you\s+are\s+receiv
                our\s+price click\s+here
                epcparts
                signed\s+up\s+to\s+receive
                golfwedge
                cigscheap
                inkstop
                penniesforcollege
                cooltvoffers
                4seasons hoover1
                \.biz
                optout
                ggooody richstores officialtvoffers
                seenontv
                c-l-i-c-k
                cold\s+heat
                top\s+quality\s+software
                <br>\s*<br>\s*<br>\s*<br>\s*<br>\s*<br>
                <p>\s*<p>\s*<p>\s*<p>\s*<p>\s*<p>
                take\s+your\s+cash\s+in\s+a\s+flash
                directedsearch newslistreview
                America\s+Gift\s+Card
                nexthitshow
                ficke8 travelive
                healthcare\s+database
                be-in-good-time vip4wag
              /,

            # Web promotion sites
            qw/ trafficmagnet coolstats wizsystems programmers4rent
                best-internet-deals blackboxhosting lutofts
                brightsunshine domeafavor weboffers
                bannerssignsmore
                usaspecials
                imgehost
                bingoboingo offerworks1 addrive laih\.com
                \.vg
                go2\.pl
                9100200 onegooddeal9 800forme showme4
                yourstuffdepot myshopinternet supertraffic linksys2
              /,
        ) {
            return $detail ? "Spam! ($pattern)" : 'Spam!'
                if $body =~ /$pattern/is;
        }
    }
    return '';
}

# Internal functions

sub hdr_decode {
    my $res = shift || '';
    # delete RFC822 comments (inside or outside "encoded words")
    $res =~ s/\(.*?\)//g;
    # If header values are encoded (RFC2047), decode them
    # Note: whitespace between adjacent "encoded words" is deleted first
    $res =~ s/\?=\s*=\?/?==?/g;
    # First field is charset, second field is encoding type, third field
    # is the encoded text
    $res =~ s/=\?.*?\?[qQ]\?(.*?)\?=/decode_qprint_hdr ($1)/ge;
    $res =~ s/=\?.*?\?[bB]\?(.*?)\?=/decode_base64 ($1)/ge;
    $res;
}

# Both of these lifted from MIME::QuotedPrint 2.12 (and modified)
sub decode_qprint {
    my $res = shift || '';
    $res =~ s/[ \t]+?(\r?\n)/$1/g;  # rule #3 (trailing space must be deleted)
    $res =~ s/=\r?\n//g;            # rule #5 (soft line breaks)
    $res =~ s/=([\da-fA-F]{2})/pack("C", hex($1))/ge;
    $res;
}

sub decode_qprint_hdr {
    my $res = shift || '';
    $res =~ s/_/ /g;                # explicit encoding of spaces for RFC2047
    $res =~ s/[ \t]+?(\r?\n)/$1/g;  # rule #3 (trailing space must be deleted)
    $res =~ s/=\r?\n//g;            # rule #5 (soft line breaks)
    $res =~ s/=([\da-fA-F]{2})/pack("C", hex($1))/ge;
    $res;
}

sub decode_base64 {
    my $str = shift || '';
    $str =~ tr|A-Za-z0-9+=/||cd;            # remove non-base64 chars
    if (length($str) % 4) {
	require Carp;
	Carp::carp("Length of base64 data not a multiple of 4")
    }
    $str =~ s/=+$//;                        # remove padding
    $str =~ tr|A-Za-z0-9+/| -_|;            # convert to uuencoded format

    return join'', map( unpack("u", chr(32 + length($_)*3/4) . $_),
	                $str =~ /(.{1,60})/gs);
}

sub extract_addresses {
    my @res;
    for (@_) {
        my @addrs = Mail::Address->parse ($_);
        # The value might contain a list of multiple addresses; we break
        # them apart and keep just the user@domain parts.
        push @res, map $_->address || join ('', $_->phrase), @addrs;
    }
    @res;
}

1;
