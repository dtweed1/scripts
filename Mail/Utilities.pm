package Mail::Utilities;

=head1 NAME

Mail::Utilities - Useful stuff for dealing with email

=head1 SYNOPSIS

use Mail::Utilities;

=head1 DESCRIPTION

At the moment, this module just comprises a set of useful subroutines.

=over

=cut

# History:
#   2020/04/14 DT   Same thing for "develooper.com" (Perl Mongers list server).
#   2018/04/27 DT   Mail that comes via groups.io puts the list address into
#                   Delivered-To: so we have to use X-Original-To: instead.
#   2008/08/12 DT   Mail that has gone through Cubesoft's SpamAssassin seems
#                   to lose its Delivered-To: header and get an
#                   "X-Original-To:" in its place.
#   2007/08/12 DT   Start by pulling together some commonly-used functions
#                   from other scripts.

use vars qw($VERSION);

$VERSION = "0.01";
sub Version { $VERSION }

=item Mail::Utilities::parse_delivered_to ( I<$message> [, I<%options> ] );

This function takes the contents of a message's Delivered-To: field and
returns the specific domain and user that it specifies.
I<$message> is a reference to a C<Mail::Message>.
The return value is a list (domain, user).
If there's no Delivered-To: field in the header, or if its format is not
recognized, these will both be null.

I<%options> include:

=over

=item debug => boolean

Show progress information.

=back

=cut

sub parse_delivered_to {
    my ($msg, %opts) = @_;

    my $deliver = $msg->header ('delivered-to');
    unless (@$deliver) {
        # No delivered-to headers, check for x-original-to instead.
        $deliver = $msg->header ('x-original-to');
    }
    if (@$deliver) {
        # "login-domain-user@domain" Note that user may contain '-'.
        # Note that domain may also contain '-', along with a '.'.
        my $value = $deliver->[0];
        warn "Delivered-to: $value\n" if $opts{'debug'};

	# If the domain is "groups.io", they put the list address into
        # "Delivered-To:", so then we need to look at "X-Original-To:" instead.
        if ($value =~ /groups\.io$/) {
	    $deliver = $msg->header ('x-original-to');
	    $value = $deliver->[0];
	    warn "X-Original-To: $value\n" if $opts{'debug'};
	}

        # If the domain is "develooper.com", they put the list address into
        # "Delivered-To:", so again, we need to look at "X-Original-To:".
        if ($value =~ /develooper\.com$/) {
	    $deliver = $msg->header ('x-original-to');
	    $value = $deliver->[0];
	    warn "X-Original-To: $value\n" if $opts{'debug'};
	}

        $value =~ s/\s//g;
        $value =~ s/\r//g; # TBD: Is this necessary?
        # Special case: Bounces from Yahoo groups end up with the login-domain
        # part duplicated; get rid of the extra copy before parsing.
        $value =~ s/^dtweed-barefooters\.org-(dtweed-barefooters\.org)/$1/i;
        if ($value =~ /^([^-]+)-(\S+\.[^-]+)-([^@]+)@(.+)$/) {
            # Cubesoft's old qmail-based format
            my ($login, $domain, $user, $dom2) = ($1, $2, $3, $4);
            if ($opts{'debug'}) {
                warn "   domain  = $domain\n";
                warn "   user    = $user\n";
                warn "   domain2 = $dom2\n";
            }
            return ($domain, $user);
        } elsif ($value =~ /^([^@]+)@(.+)$/) {
            # Cubesoft's new postifx-based format
            my ($user, $domain) = ($1, $2);
            if ($opts{'debug'}) {
                warn "   domain  = $domain\n";
                warn "   user    = $user\n";
            }
            return ($domain, $user);
        } else {
            warn "*** unexpected Delivered-To: $value\n";
        }
    }
    return ('', '');
}

=item Mail::Utilities::sanitize_directory (I<$domain>, I<$user>);

Convert the given domain and user into an acceptable two-level path for the
filesystem.
The first element is the second-to-last component of the domain name,
and the second element is the user name.
Both elements are converted to lower-case and stripped of everything
except letters, numbers and hyphens.

=cut

sub sanitize_directory {
    my ($domain, $user) = @_;

    # use only the second-to-last component of the domain name
    my $domain_root = (split ('\.', $domain))[-2];

    # convert to lower-case and keep only the letters, numbers and hyphens
    $domain_root = lc $domain_root;
    $domain_root =~ tr/-a-z0-9//cd;

    # convert user to lower-case and keep only the letters, numbers and hyphens
    $user = lc $user;
    $user =~ tr/-a-z0-9//cd;

    "$domain_root/$user";
}

=back

=head1 NOTES

Don't get too attached to this module -- my intent is to switch everything
over to Mail::Box before too long.

=head1 BUGS

=head1 ENVIRONMENT

=head1 TO DO

=head1 AUTHOR

Written by Dave Tweed.

=cut

#'
1;
