# Message.pm - objects that represent mail (or news) messages

# A message has a header part and a body part.
# If the body is multipart, it is stored as an array of messages; otherwise,
# it is stored as a single string.

# TBD: For plain text bodies: signatures? uuencoded inclusions?
# TBD: Move check_body() to a separate module?

# History:
#   2003/03/19 DT   Add options to new(), fix_base64.
#   2003/03/09 DT   Show detail in check_body() on multi-part messages.
#   2003/02/26 DT   Check for bad syntax in content-type parameters.
#   2003/02/24 DT   Fix check_body() to treat text as one line.
#   2003/01/25 DT   Change describe() to return a string instead of printing.
#                   Start working on as_strings().
#   2003/01/11 DT   Fix uninit var warning in check_body().
#   2002/12/16 DT   Streamline check_body(), add optional detail report.
#   2002/12/05 DT   Fix up special treatment of content-type header hash.
#   2002/12/04 DT   Add more spam triggers.
#                   Allow spaces around the '='s in Content-type: attributes.
#   2002/12/03 DT   Fix bug related to decoding single-part bodies.
#                   Fix bug related to parts with no newline on last line.
#   2002/11/23 DT   Return empty array if no header() of a particular name.
#   2002/11/11 DT   Implement hdr_decode().
#   2002/11/10 DT   Put \Q...\E around boundary strings.
#                   Change ->addressees return value.
#                   Add ->senders.
#   2002/11/08 DT   Add MIME support.
#   2002/06/13 DT   Pull out from fetch.pl

package Mail::Message;

use strict;

# use lib "c:/dtweed/bin";
use Mail::Address;

# Create a new mail object from a string or an arrayref (of strings)

sub new {
    my ($rtext, $ismime, %opts) = @_;

    if (ref $rtext) {
        unless (ref $rtext eq 'ARRAY') {
            die "bad reference to Message::new ($rtext)";
        }
    } else {
        # Don't use the split; it removes the newlines from the lines
        # $rtext = [split /\n/, $rtext];
        $rtext = [$rtext =~ /[^\n]*\n?/g];
    }

    # Pick the header out of the message and fix up continuation lines
    my @raw_header;
    my $raw_body;
    my $hflag = 1;
    foreach (@$rtext) {
        if ($hflag) {
            if (/^\s*$/) {
                $hflag = 0;
            } else {
                push @raw_header, $_;
            }
        } else {
            $raw_body .= $_;
        }
    }
    my $temp = join ('', @raw_header);
    $temp =~ s/\n\s+/ /g;
    @raw_header = split ("\n", $temp);
    my %header;
    for (split ("\n", $temp)) {
        # TBD: verify success of split
        my ($name, $value) = split (': +', $_, 2);
        push @{$header{lc $name}}, $value;
    }

    # Parse the MIME headers
    my $body = $raw_body;
    if (exists $header{'mime-version'}) {
        $header{'mime-version'} = hdr_decode ($header{'mime-version'}[0]);
        if ($header{'mime-version'} eq '1.0') {
            $ismime=1;
        }
    }
    if ($ismime) {
        # If the body is encoded, decode it
        #     Content-Transfer-Encoding: <mechanism>
        #          7bit
        #          8bit
        #          binary
        #          base64
        #          quoted-printable
        if (exists $header{'content-transfer-encoding'}) {
            $header{'content-transfer-encoding'} =
                lc hdr_decode ($header{'content-transfer-encoding'}[0]);
            if ($header{'content-transfer-encoding'} eq 'quoted-printable') {
                $body = decode_qprint ($raw_body);
            } elsif ($header{'content-transfer-encoding'} eq 'base64') {
                if ($opts{'fix_base64'}) {
                    # A common flaw is that a leading '0' on a line has been
                    # dropped somewhere along the way. If a line is short by
                    # one character, try adding it back in.
                    my @temp = split /\n/, $raw_body;
                    for (@temp) {
                        if ((length) % 4 == 3) {
                            $_ = '0'.$_;
                            warn "base64 line fixed\n";
                        } elsif ((length) % 4) {
                            warn "bad base64 line not fixed\n";
                        }
                    }
                    $raw_body = join '', @temp;
                }
                $body = decode_base64 ($raw_body);
            }
        }

        #     Content-type: <type>/<subtype> [ ; <param>=<value> ... ]
        #          multipart/alternative; boundary="string"
        #          multipart/mixed; boundary="string"
        #          text/plain; charset="iso-8859-1"
        #          text/plain; charset=US-ASCII
        #          text/html; charset="iso-8859-1"
        #          application/octet-stream; name=valign.scr
        # Note: Can't hdr_decode() this one because the boundary string can
        # be pretty much anything. TBD: May have to hdr_decode() individual
        # fields after breaking them up.
        if (exists $header{'content-type'}) {
            my @temp = split /;\s*/, $header{'content-type'}[0];
            # TBD: verify success of split
            my ($type, $subtype) = split /\//, lc shift @temp;
            my %attr;
            for (@temp) {
                if ((my ($name, $value) = split /\s*=\s*/, $_, 2) == 2) {
                    $value = $1 if $value =~ /"(.*)"/;
                    $attr{lc $name} = $value;
                } elsif ((($name, $value) = split /\s*:\s*/, $_, 2) == 2) {
                    # Apparently some mail clients are taking liberties
                    # with the syntax
                    $value = $1 if $value =~ /"(.*)"/;
                    $attr{lc $name} = $value;
                } else {
                    # Ignore malformed parameter strings
                }
            }
            $header{'content-type'} = [{
                type => $type,
                subtype => $subtype,
                attr => \%attr,
            }];

            # If the body is multipart, pick it apart and process the parts
            # (recursively)
            if ($header{'content-type'}[0]{'type'} eq 'multipart') {
                my $boundary = $header{'content-type'}[0]{'attr'}{'boundary'};
                # Note optional inclusion of the leading newline, which
                # allows badly-formed messages with two boundaries on
                # successive lines, and also takes care of the no-preamble
                # case for us.
                # TBD: verify success of split
                my ($main, $epilogue) = split /\n?--\Q$boundary\E--\n/, $raw_body;
                # TBD: verify success of split
                my ($preamble, @raw_parts) = split /\n?--\Q$boundary\E\n/, $main;
                my @parts = map new ($_, 1, %opts), @raw_parts;
                $body = [$preamble, @parts, $epilogue];
            }
        }

        # Other MIME headers
        #     Content-Class:
        #          urn:content-classes:message
        #
        #     Content-ID: <SVI94dD8s3Y85>
        #     Content-Disposition:
        #          attachment; FileName = "illegalSex.zip"
    }

    my $self = {
        raw_header => \@raw_header,
        header => \%header,
        raw_body => $raw_body,
        body => $body,
    };
    bless $self;
}

# Given the name of a header field, return an arrayref to a list of values

sub header {
    my ($self, $name) = @_;

    $self->{'header'}{lc $name} || [];
}

# Return the mssage ID

sub id {
    my ($self) = @_;

    $self->{'header'}{'message-id'}[0] || '';
}

# Return all of the values in the "To:" and "Cc:" fields

sub addressees {
    my ($self) = @_;

    my @temp;
    push @temp, @{$self->{'header'}{'to'}} if defined $self->{'header'}{'to'};
    push @temp, @{$self->{'header'}{'cc'}} if defined $self->{'header'}{'cc'};
    extract_addresses (@temp);
}

# Return all of the values in the "From:" field(s)

sub senders {
    my ($self) = @_;

    my @temp;
    push @temp, @{$self->{'header'}{'from'}} if defined $self->{'header'}{'from'};
    extract_addresses (@temp);
}

# Replace an existing header line

sub header_replace {
    my ($self, $name, $value) = @_;

    $self->{'header'}{lc $name} = [$value];
    # TBD: update raw_header as well
}

# Add a header line at the beginning

sub header_prepend {
    my ($self, $name, $value) = @_;

    unshift @{$self->{'header'}{lc $name}}, $value;
    unshift @{$self->{'raw_header'}}, "$name: $value";
    # TBD: break long lines in raw_header
}

# Delete a header field altogether

sub header_delete {
    my ($self, $name) = @_;

    delete $self->{'header'}{lc $name};

    my $rawref = $self->{'raw_header'};
    @$rawref = grep !/^$name:\s/i, @$rawref;
}

# Trim all lines including and following one containing a given regular
# expression (does not affect the parsed body)
sub raw_body_trim_re {
    my ($self, $re) = @_;

    my @raw = split ("\n", $self->{'raw_body'});
    my $j;
    for my $i (0..$#raw) {
        $j = $i;
        last if $raw[$i] =~ /$re/i;
    }
    return unless $raw[$j] =~ /$re/i;
    # TBD: remove blank lines ahead of this pattern as well
    splice @raw, $j;
    $self->{'raw_body'} = join ("\n", @raw);
}

# Describe the structure of a message for debugging or logging

sub describe {
    my ($self, $indent) = @_;
    $indent ||= '';
    my $output = '';

    for (keys %{$self->{'header'}}) {
        my $val = $self->{'header'}{$_};
        if (ref $val eq 'ARRAY') {
            if (@$val > 1) {
                $val = @$val;
                $val = "($val values)";
            } else {
                $val = $val->[0];
            }
        }
        if (ref $val eq 'HASH') {
            my $type = $val->{'type'};
            my $subtype = $val->{'subtype'};
            my %attr = %{$val->{'attr'}};
            $val = "$type/$subtype";
            for (keys %attr) {
                $val .= "; $_=$attr{$_}";
            }
        }
        $val = '<<undef>>' unless defined $val;
        $output .= "$indent$_: $val\n";
    }

    my $body = $self->{'body'};
    if (ref $body) {
        my @temp = @$body;
        my $preamble = shift @temp;
        if (defined $preamble) {
            my $len = length $preamble;
            $output .= "${indent}Body preamble: $len bytes\n";
        }
        my $epilogue = pop @temp;
        if (defined $epilogue) {
            my $len = length $epilogue;
            $output .= "${indent}Body epilogue: $len bytes\n";
        }
        for (0..$#temp) {
            $output .= "${indent}Part $_:\n";
            $output .= describe ($temp[$_], $indent.'    ');
        }
    } else {
        if (defined $body) {
            my $len = length $body;
            $output .= "${indent}Body: $len bytes\n";
        } else {
            $output .= "${indent}Body: (none)\n";
        }
    }

    $output;
}

# Deliver the entire (updated?) message as a string
# TBD: put the (modified?) body back together

sub print {
    my ($self) = @_;

    join ("\n", @{$self->{'raw_header'}}, '', $self->{'raw_body'});
}

# Deliver the entire updated message as an array of strings
# TBD
sub as_strings {
    die "Mail::Message->as_strings not ready for prime time";
    my ($self) = @_;
    my @output;

    # TBD: analyze the body structure and make sure MIME headers are correct
    my $body = $self->{'body'};
    if (ref $body) {
    } else {
    }

    # the header
    # TBD: any particular order?
    for my $name (keys %{$self->{'header'}}) {
        for my $value (@{$self->{'header'}{$name}}) {
            # TBD: capitalize $name in the conventional manner
            # TBD: break long $value lines
            push @output, "$name: $value\n";
        }
    }

    # the body

    @output;
}

# Deliver the entire body as a string or a reference to an array of parts
# (preamble and epilogue deleted)

sub body {
    my ($self) = @_;

    return $self->{'body'} unless ref $self->{'body'};

    my @body = @{$self->{'body'}};
    shift @body;
    pop @body;
    \@body;
}

# Check for certain virus or spam characteristics

sub check_body {
    my ($self, $detail) = @_;

    my $body = $self->body;
    return '' unless defined $body;

    if (ref $body) {
        # This is a multipart message - check for executable content
        for (@$body) {
            my $content = $_->header ('content-type');
            next unless defined $content && @$content;
            $content = $content->[0];

            if ($content->{'type'} =~ /audio|application/
                && defined $content->{'attr'}{'name'}
                && $content->{'attr'}{'name'} =~ /\.(bat|exe|scr|pif)$/
                ) {
                return 'Virus!';
            } else {
                my $temp = check_body ($_, $detail);
                return $temp if $temp;
            }
        }
    } else {
        # This is probably text
        for my $pattern (
            # African money scam
            qw/ million.*(dollar|usd) late\s+husband nigeria congo /,

            # Common sex topics
            qw/ \bpenis\b \bviagra\b \berotic\b /,

            # drug websites
            qw/ cool-hoop hoopdirect hoopcenter find-hoop
              /,

            # Common lending topics
            qw/ refinance mortgage \bequity\b
                bestcorporate priceisright market-the-web letlenderscompete
              /,

            # Music-related sites
            qw/ kitsapbands harmoniemusik musicscene /,

            # Misc
            qw/ back\s*up.*dvd copy\s+dvd
                veggiegear libero\.it smoking\.com
                this\s+is\s+not\s+spam
                you\s+have\s+receiv you\s+are\s+receiv
                our\s+price click\s+here
              /,

            # Web promotion sites
            qw/ trafficmagnet coolstats wizsystems w3dp programmers4rent
              /,
        ) {
            return $detail ? "Spam! ($pattern)" : 'Spam!'
                if $body =~ /$pattern/is;
        }
    }
    return '';
}

# Internal functions

sub hdr_decode {
    my $res = shift || '';
    # delete RFC822 comments (inside or outside "encoded words")
    $res =~ s/\(.*?\)//g;
    # If header values are encoded (RFC2047), decode them
    # Note: whitespace between adjacent "encoded words" is deleted first
    $res =~ s/\?=\s*=\?/?==?/g;
    # First field is charset, second field is encoding type, third field
    # is the encoded text
    $res =~ s/=\?.*?\?[qQ]\?(.*?)\?=/decode_qprint_hdr ($1)/ge;
    $res =~ s/=\?.*?\?[bB]\?(.*?)\?=/decode_base64 ($1)/ge;
    $res;
}

# Both of these lifted from MIME::QuotedPrint 2.12 (and modified)
sub decode_qprint {
    my $res = shift || '';
    $res =~ s/[ \t]+?(\r?\n)/$1/g;  # rule #3 (trailing space must be deleted)
    $res =~ s/=\r?\n//g;            # rule #5 (soft line breaks)
    $res =~ s/=([\da-fA-F]{2})/pack("C", hex($1))/ge;
    $res;
}

sub decode_qprint_hdr {
    my $res = shift || '';
    $res =~ s/_/ /g;                # explicit encoding of spaces for RFC2047
    $res =~ s/[ \t]+?(\r?\n)/$1/g;  # rule #3 (trailing space must be deleted)
    $res =~ s/=\r?\n//g;            # rule #5 (soft line breaks)
    $res =~ s/=([\da-fA-F]{2})/pack("C", hex($1))/ge;
    $res;
}

sub decode_base64 {
    my $str = shift || '';
    $str =~ tr|A-Za-z0-9+=/||cd;            # remove non-base64 chars
    if (length($str) % 4) {
	require Carp;
	Carp::carp("Length of base64 data not a multiple of 4")
    }
    $str =~ s/=+$//;                        # remove padding
    $str =~ tr|A-Za-z0-9+/| -_|;            # convert to uuencoded format

    return join'', map( unpack("u", chr(32 + length($_)*3/4) . $_),
	                $str =~ /(.{1,60})/gs);
}

sub extract_addresses {
    my @res;
    for (@_) {
        my @addrs = Mail::Address->parse ($_);
        # The value might contain a list of multiple addresses; we break
        # them apart and keep just the user@domain parts.
        push @res, map $_->address || join ('', $_->phrase), @addrs;
    }
    @res;
}

1;
