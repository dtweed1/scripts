package Mail::Folder;

# objects that represent collections of mail (or news) messages
# This is a generic class that can be instantated using Mail::Mbox2,
# Mail::Maildir or Mail::Mime_box.

# Each message has additional properties that are not contained within the
# text of the message itself:
#   * sender - Envelope sender
#  (* Envelope recipient? not always available)
#   * time - Timestamp when received
#   * class - Classification (junk-, mult-, huge-, offlist-, etc.)
#   * state - State (unread, read, unsent, etc.)
# Each instantiation of this class must deal with this meta-information
# somehow.
#   Maildir - currently encoded in filename, but could use auxiliary file.
#   Mbox - currently encoded in "From " line, but could use auxiliary file.
#   Mime_box - stored in the Mime header for each message.

# To do:
#   What is the right way to generalize the concepts of moving or copying
#   a message from one folder to another (or just reclassifying within a
#   folder)?

# History:
#   2004/05/12 DT   Generalize from Mail::Mbox2.
#                   Keep the 'sender' and 'time' data separate internally.
#   2003/04/15 DT   Delete empty mboxes altogether.
#   2003/02/15 DT   Escape and unescape "From " lines in the message text.
#   2003/01/20 DT   Start.

use strict;

# use lib '/home1/dtweed/bin';
# use Mail::Message;

# Create a new mbox object from a file.
# Optionally lock the file for the lifetime of the object.
# if $lock is defined but false, then the mbox is read-only,
# and not updated when DESTROYed.

# TBD: Can this generic function figure out what kind of folder we're talking
# about by examining the filesystem object passed in? If it's a directory, it
# must be a Maildir, but if it's a file, it could be an Mbox (first line is
# "From ..." and/or there's a meta-info file in the same directory) or a
# Mime_box (first line is "Mime-version: ...").
#
# Obviously, if the object doesn't exist yet, we get to pick what kind it's
# going to be. Maybe we'll even allow the user to specify a preference.

sub new {
    my ($filename, $lock) = @_;

    die "Mail::Folder::new() must be overridden!";
}

sub DESTROY {
    my $self = shift;

    die "Mail::Folder::DESTROY() must be overridden!";
}

# Return a list of message keys

sub keys {
    my $self = shift;
    return () unless @{$self->{'msgs'}};
    (0..$#{$self->{'msgs'}});
}

# Return the next key for a new message

sub next_key {
    my $self = shift;
    scalar @{$self->{'msgs'}};
}

# Delete a message

sub delete {
    my ($self, $key) = @_;
    $self->{'msgs'}[$key] = undef;
}

# Return/set the sender

sub sender {
    my ($self, $key, $newsender) = @_;
    if (defined $newsender) {
        $self->{'msgs'}[$key]{'sender'} = $newsender || '-';
    }
    $self->{'msgs'}[$key]{'sender'};
}

# Return/set the timestamp

sub time {
    my ($self, $key, $newtime) = @_;
    if (defined $newtime) {
        $self->{'msgs'}[$key]{'time'} = $newtime || gmtime;
    }
    $self->{'msgs'}[$key]{'time'};
}

# Return/set the "From " line all at once (sender and timestamp)

sub from {
    my ($self, $key, $newsender, $newtime) = @_;
    if (defined $newsender) {
        $self->sender ($key, $newsender);
        $self->time ($key, $newtime);
    }
    # @{$self->{'msgs'}{$key}}('sender', 'time');
    ($self->{'msgs'}{$key}('sender'), $self->{'msgs'}{$key}('time'));
}

# Return/set the message text (arrayref)

sub text {
    my ($self, $key, $newtext) = @_;
    if (defined $newtext) {
        # copy the data, not just the reference
        $self->{'msgs'}[$key]{'text'} = [@$newtext];
        # make sure there's a valid From line
        unless ($self->{'msgs'}[$key]{'sender'}) {
            $self->from ($key, '', '');
        }
    }
    $self->{'msgs'}[$key]{'text'};
}

1;
