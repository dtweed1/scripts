package Mail::Mbox2;

# objects that represent mboxes of mail (or news) messages

# An mbox has Mail::Message (RFC2822) messages separated by
# "From <sender> <date>" lines.

# History:
#   2008/04/13 DT   Do NOT unlink empty mbox files.
#   2003/05/12 DT   New version that inherits from Mail::Folder.
#   2003/04/15 DT   Delete empty mboxes altogether.
#   2003/02/15 DT   Escape and unescape "From " lines in the message text.
#   2003/01/20 DT   Start.

use strict;
use vars qw(@ISA);

use Mail::Folder;
@ISA = ("Mail::Folder");

use Fcntl qw(:DEFAULT :flock);
use IO::File;

# Create a new mbox object from a file.
# Optionally lock the file for the lifetime of the object.
# if $lock is defined but false, then the mbox is read-only,
# and not updated when DESTROYed.

sub new {
    my ($filename, $lock) = @_;
    my $fh = new IO::File;

    # open and lock the file
    unless (sysopen ($fh, $filename, O_RDWR | O_CREAT)) {
        warn "can't open $filename: $!";
        # OK if file doesn't exist; we'll write a new one later if necessary
        my $self = {
            filename => $filename,
            handle => undef,
            readonly => defined $lock && !$lock,
            msgs => [],
        };
        bless $self;
        return $self;
    }
    if ($lock) {
        unless (flock ($fh, LOCK_EX)) {
            warn "can't lock $filename: $!";
            return undef;
        }
    }

    # build the array of messages
    my @msgs;
    my $key = 0;
    my $temp = <$fh>;
    if ($temp) {
        unless ($temp =~ /^From /) {
            warn "$filename is not an mbox";
            close $fh;
            return undef;
        }
        my ($sender, $time) = ($temp =~ /^From (.*)\s+(.{24})$/);
        $msgs[$key]{'sender'} = $sender;
        $msgs[$key]{'time'} = $time;

        while (<$fh>) {
            if (/^From /) {
                # split out the sender and time strings
                ($sender, $time) = /^From (.*)\s+(.{24})$/;
                $msgs[++$key]{'sender'} = $sender;
                $msgs[++$key]{'time'} = $time;
            } else {
                # Unescape ">+From " lines by deleting one '>'
                s/^>(>*From )/$1/;
                push @{$msgs[$key]{'text'}}, $_;
            }
        }
    }

    close $fh unless $lock;

    my $self = {
        filename => $filename,
        handle => $lock ? $fh : undef,
        readonly => defined $lock && !$lock,
        msgs => \@msgs,
    };
    bless $self;
}

sub DESTROY {
    my $self = shift;

    return if $self->{'readonly'};

    # Count the remaining messages
    my @msgs = grep defined $_, @{$self->{'msgs'}};

    my $fh = $self->{'handle'};
    my $name = $self->{'filename'};

    if (0) {
        # If there are none, just unlink the file if it exists
        unless (@msgs) {
            return unless -e $name;
            warn "unlinking $name\n";
            unless (unlink $name) {
                warn "can't unlink $name: $!";
            }
            close $fh if defined $fh;
            return;
        }
    }

    # if the file was locked (and left open)
    if (defined $fh) {
        # rewind and truncate it
        unless (seek ($fh, 0, 0)) {
            warn "can't rewind $name: $!";
            return;
        }
        unless (truncate ($fh, 0)) {
            warn "can't truncate $name: $!";
            return;
        }
    } else {
        # reopen it for writing
        warn "reopening $name\n";
        $fh = new IO::File;
        unless (open ($fh, ">$name")) {
            warn "can't write $name: $!";
            return;
        }
    }

    # write the remaining messages in the array back out to the file
    for my $m (@msgs) {
        my $rtext = $m->{'text'} || [];
        my ($sender, $time) = @{$m}{'sender', 'time'};
        print $fh "From $sender $time\n";
        # escape ">*From " lines by adding one '>'
        for (@$rtext) {s/^(>*From )/>$1/}
        print $fh @$rtext;
    }

    # close it
    close $fh;
}

1;
