#!/usr/bin/perl -w

# z -- print zeller numbers

# NOTE: This hack and the -w switch above are just to eliminate problems
# with MSDOS line endings.
BEGIN { $^W = 0; }

# History:
#   2011/11/25 DT   Add tests for dst_starts() and dst_ends().
#   2006/10/30 DT   Add feature to debug is_dst().
#   2003/01/06 DT   Clean up by using newer Date::Zeller features.
#   2003/01/02 DT   Use Date::Zeller instead of zeller.pl.
#   2000/11/29 DT   Add -d switch, clean up debug output.
#   2000/08/21 DT   Use new local_time() in zeller.pl.
#   1999/04/20 DT   Add ability to take zeller number from command line.
#                   Move day and month names to zeller.pl.
#   1999/03/30 DT   Add ability to take date from command line.
#   1998/11/02 DT   Start.

use strict;
use vars qw/$debug @words/;

use lib $ENV{'PERLPATH'};
use Date::Zeller qw(:DEFAULT split_time hardware_time local_time dst_starts
                    dst_ends is_dst print_local_time print_GM_time);

# Process switches
$debug = 0;
for (@ARGV) {
    if (/^-/) {
        foreach my $c (split (/ */, substr ($_, 1))) {
            if ($c eq 'd') {$debug++;}
        }
    } else {
        push (@words, $_);
    }
}

if ($debug) {
    # Show the machine time processed in various ways.
    print "time function: ", scalar split_time (time), "\n";
    print "hardware time: ", scalar split_time (hardware_time), "\n";
    print "local time:    ", scalar split_time (local_time (time)), "\n";
    print "print_GM_time:    ", print_GM_time (time), "\n";
    print "print_local_time: ", print_local_time (time), "\n";
    my ($y, $m, $d) = unzeller (ztoday);
    print "DST starts: ", join ('/', unzeller (dst_starts($y))), "\n";
    print "DST ends: ", join ('/', unzeller (dst_ends($y))), "\n";
}

if (my $arg = shift @words) {
    if ($arg =~ /\//) {
        my @arg = split ('/', $arg);
        print zeller (@arg), "\n";
        print ((is_dst (@arg, 12) ? "DST" : "standard time"), "\n")
            if $debug;
    } else {
        my @arg = unzeller ($arg);
        print join ('/', @arg), "\n";
        print ((is_dst (@arg, 12) ? "DST" : "standard time"), "\n")
            if $debug;
    }
} else {
    print ztoday, "\n";
}

