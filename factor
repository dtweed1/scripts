#!/usr/bin/perl -w

# History:
#   2019/01/04 DT   Optimize.
#   2006/04/09 DT   Start.

my $n = shift;

while (2 * int ($n/2) == $n) {
    push @factors, 2;
    $n = $n/2;
}

my $i = 3;
while ($i <= int (sqrt $n)) {
    while ($i * int ($n/$i) == $n) {
        push @factors, $i;
        $n = $n/$i;
    }
    $i += 2;
}

push @factors, $n if $n > 1;

print "factors = @factors\n";
