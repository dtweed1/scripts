
package Eagle::Partlist;

# Load an Eagle partlist

sub load_bom {
    my ($self, $name) = @_;
    my %result;

    open (my $fh, '<', $name) || die "can't open $name: $!\n";

    # The first nonblank line is simply "Partlist".
    expect_line ($fh, '^Partlist$'); #'

    # The second nonblank line is
    # "Exported from <schematic_file> at <date_time>"
    expect_line ($fh, '^Exported from');

    # The third nonblank line is
    # "EAGLE Version <version> Copyright (c) <years> CadSoft"
    # The current version is 4.11, 1988-2003
    expect_line ($fh, '^EAGLE Version');

    # The fourth line is the column headings, and they provide a clue
    # about the spacing of the field values for the records that follow.
    my $line = expect_line ($fh, 'Part +Value +Device +Package +Library +Sheet');
    my @width = map length,
        ($line =~ /(Part +)(Value +)(Device +)(Package +)(Library +)Sheet/);

    # The remaining nonblank lines hold part information, one part per line.
    while (<$fh>) {
        chomp;
        s/\s+$//;
        next unless length;

        my @field;
        for my $w (@width) {
            my $f = substr ($_, 0, $w);
            $_ = substr ($_, $w);
            $f =~ s/\s+$//;
            push @field, $f;
        }
        push @field, $_;

        my ($ref, $value, $libdev, $footprint, $library, $sheet) = @field;
        $result{'parts'}{$ref} = {
            'value' => $value,
            'libdev' => $libdev,
            'footprint' => $footprint,
            'library' => $library,
            'sheet' => $sheet,
        };
    }

    \%result;
}

# Read blank lines (if any) and verify that the next nonblank line matches
# the given pattern. Return the line.

sub expect_line {
    my ($fh, $pattern) = @_;
    while (<$fh>) {
        chomp;
        s/^\s+//;
        s/\s+$//;
        next unless length;
        die "expecting '$pattern', found '$_'\n" unless /$pattern/;
        last;
    }
    $_;
}

1;
