#!/usr/bin/perl -w

# cvs_log - show a chronological summary of changes to a CVS project

# History:
#   2006/11/03 DT   Fix parsing of revisions.
#   2004/05/27 DT   Add command-line arguments.
#   2001/10/12 DT   Start.

use strict;
use vars qw/@lines $temp %log
    /;

chomp (@lines = `cvs log @ARGV`);

# go through and combine any line starting with a tab with the previous line
$temp = join "\n", @lines;
$temp =~ s/\n\t/ /mg;
@lines = split "\n", $temp;

# Each module starts with a blank line and ends with a line of '='.
# Header fields include:
#   RCS file:
#   Working file:
#   head:
#   branch:
#   locks:
#   access list:
#   symbolic names:
#   keyword substitution:
#   total revisions:     ;  selected revisions:
#   description:      (text on next line)
# After that, there's a line of '-' for each revision, followed by
#   revision <number>
#   date: <date> <time>; author: <string>; state: <string>; lines: +<n> -<n>
#   <description>

# We build a hash keyed by the working file name
# {
#   <name> => {
#     'RCS file' => string,
#     'head' => string,
#     ...
#     'description' => string,
#     'revs' => {
#       <number> => {
#         'timestamp' => seconds
#         'author' => string
#         'state' => string
#         'lines' => string
#         'description' => string
#       }
#     }
#   }
# }

while (@lines) {
    # blank line?
    if (length $lines[0]) {
        warn "blank line expected, found: $lines[0])\n";
    } else {
        shift @lines;
    }

    # parse module header until "description:" is found
    my %header;
    my ($name, $value);
    do {
        last unless @lines;
        my $line = shift @lines;
        # print "header line: $line\n";
        for my $field (split (/;\s+/, $line)) {
            ($name, $value) = split (/: */, $field, 2);
            $name =~ s/^ +//;
            # print "  property name: $name\n";
            $header{$name} = $value if $value;
        }
    } until ($name eq 'description');
    $header{'description'} = shift @lines unless ($lines[0] =~ /^-+$/);

    # store the header under the working file name
    my $key = $header{'Working file'};
    die "no working file name" unless defined $key;
    $log{$key} = \%header;

    # get the revisions
    while (@lines && $lines[0] =~ /^-+$/) {
        # discard the line of '-'
        shift @lines;

        # get the revision number
        my $line = shift @lines;
        my ($rev) = ($line =~ /^revision +([\d.]+)$/);
        die "no revision number found" unless defined $rev;

        # get the line(s) of properties
        my @revlines;
        while (@lines && $lines[0] !~ /^[-=]+$/) {
            push @revlines, shift @lines;
        }

        # This assumes that the description line won't have a ':' in it!!!
        # I'm not sure there's a definite way to disambiguate this, but as
        # an additional heuristic, we'll assume that there's always at least
        # one line of description (and maybe more!)
        while (@revlines > 1 && $revlines[0] =~ /:/) {
            $line = shift @revlines;
            for my $field (split (/;\s+/, $line)) {
                my ($name, $value) = split (/: */, $field, 2);
                $log{$key}{'revs'}{$rev}{$name} = $value if $value;
            }
        }
        $log{$key}{'revs'}{$rev}{'description'} = shift @revlines;
    }

    # line of '='?
    if ($lines[0] =~ /^=+$/) {
        shift @lines;
    } else {
        warn "line of '=' expected, found: $lines[0]\n";
    }
}

&dump_chrono;

sub dump_chrono {
    my @chrono;

    for my $file (sort keys %log) {
        my $rfile = $log{$file};
        for my $rev (keys %{$rfile->{'revs'}}) {
            my $rrev = $rfile->{'revs'}{$rev};
            push @chrono, {
                'date' => $rrev->{'date'},
                'desc' => $rrev->{'description'},
                'file' => $file,
                'rev'  => $rev,
            };
        }
    }

    for my $r (sort {$a->{'date'} cmp $b->{'date'}} @chrono) {
        my ($date, $file, $rev, $desc) = @{$r}{qw/date file rev desc/};
        print "$date $file $rev $desc\n";
    }
}

sub dump_log {
    for my $file (sort keys %log) {
        print $file, "\n";
        my $rfile = $log{$file};
        for my $prop (sort keys %$rfile) {
            if ($prop eq 'revs') {
                print "  revisions:\n";
                for my $rev (sort keys %{$rfile->{'revs'}}) {
                    my $rrev = $rfile->{'revs'}{$rev};
                    print "    $rev:\n";
                    for my $name (sort keys %$rrev) {
                        my $val = $rrev->{$name};
                        print "      $name=$val\n";
                    }
                }
            } else {
                my $val = $rfile->{$prop};
                print "  $prop: $val\n";
            }
        }
    }
}
