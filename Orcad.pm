# Orcad.pm

package Orcad;

# These subroutines parse and verify BOMs from Orcad SDT

# The output of Orcad->load_bom() is a reference to a hash:
# {
#   title        => field from report header
#   sch_date     => field from report header
#   drawing      => field from report header
#   revision     => field from report header
#   organization => field from report header
#   address1     => field from report header
#   address2     => field from report header
#   address3     => field from report header
#   address4     => field from report header
#   bom_date     => field from report header
#   parts => {      a hash indexed by part number, whose elements are hashes:
#     {
#       part => the "Part Value" from the schematic
#       qty  => the quantity of item with this Intraplex part number
#       ref  => the list of reference designators from the schematic
#       desc => the description of the item from the include file
#     }
#   }
# }

# To do:
#   Automatically run Orcad PARTLIST if necessary (and to get correct format).
#       In "Tools ==> Bill of Materials", the Header should be:
#   Item\tQuantity\tReference\tPart\tNumber\tFootprint
#       and the Combined property string should be:
#   {Item}\t{Quantity}\t{Reference}\t{Value}\t{1ST PART FIELD}\t{PCB Footprint}
#       and each part needs to be on a separate line.
#   Handle multiple "item"s with the same "partnum" (like "omit") in the
#       Orcad BOM.

# History:
#   2003/08/15 DT   Add optional footprint field to Orcad BOM.
#                   Make up "TBD-xxx-xxx" part numbers as needed.
#   2000/10/18 DT   Bring up to Perl5.
#   1996/12/03 DT   Turn into a true package.
#   1996/08/12 DT   Add support for "ref_only" parts: If this string is
#                   found in the reference list, it is deleted and the
#                   quantity is reduced by one.
#   1996/05/23 DT   Measure width of part field instead of using
#                   hardcoded value.
#   1996/05/22 DT   Add hack to delete Mike's colons.
#   1996/04/16 DT   Fix to handle BOMs with include files.
#   1996/04/02 DT   Split out from comp_bom.bat.

sub load_bom {
    my ($self, $name) = @_;
    my %result;

    open (INPUT, $name) || die "can't open $name: $!\n";

    # Items from the header
    chop ($_ = <INPUT>);
    /^(.+)Revised:\s+(.+)$/;
    $result{'title'} = $1;
    $result{'sch_date'} = $2;
    $result{'title'} =~ s/\s+$//;
    
    chop ($_ = <INPUT>);
    /^(\S+)\s+Revision:\s+(.+)$/;
    $result{'drawing'} = $1;
    $result{'revision'} = $2;
    
    $_ = <INPUT>;   # Blank line
    chop ($result{'organization'} = <INPUT>);
    chop ($result{'address1'} = <INPUT>);
    chop ($result{'address2'} = <INPUT>);
    chop ($result{'address3'} = <INPUT>);
    chop ($result{'address4'} = <INPUT>);
    $_ = <INPUT>;   # Blank line

    chop ($_ = <INPUT>);
    /^Bill Of Materials\s+(.+)Page/;
    $result{'bom_date'} = $1;
    $result{'bom_date'} =~ s/\s+/ /g;
    chop $result{'bom_date'};
    $_ = <INPUT>;   # Blank line
    $_ = <INPUT>;   # Column titles
    # In the titles row, the next field after "Part" is actually from
    # the first line of the include file. We can use its position to
    # determine how wide the "Part" field really is.
    /(Part\s+)/;
    my ($part_width) = length ($1);
    $_ = <INPUT>;   # Underscores
    $_ = <INPUT>;   # Blank line

    # Each item line contains an item number, a quantity, a reference
    # designator, a part value, and an Intraplex part number. The first
    # two fields are blank if the same as before.
    while (<INPUT>) {
        chomp;
        my ($item_no, $qty, $ref, $part, $partnum, $footprint) = split ("\t");

        # If there's no $partnum, make up one.
        $footprint ||= 'TBD';
        $partnum = "TBD-$part-$footprint" unless $partnum;

        # If there's something in the first 8 columns, assume the start
        # of a new item; otherwise, just get the reference designator.
        if ($item_no) {
            $result{'parts'}{$partnum} = {
                'footprint' => $footprint,
                'part' => $part,
                'qty' => $qty,
                'ref' => [$ref],
            };
        } else {
            push @{$result{'parts'}{$partnum}{'ref'}}, $ref;
        }
    }

    close (INPUT);

    # Go through and delete "ref_only" and "omit" parts -- this may
    # reduce the quantity to zero.
    foreach my $p (keys %{$result{'parts'}}) {
        if ($p =~ /omit/) {
            delete $result{'parts'}{$p};
            next;
        }

        my $ra = $result{'parts'}{$p}{'ref'};
# warn "Before: $p -- ", join (',', @$ra), "\n";
        @$ra = grep (!/^ref_only$/, @$ra);
        $result{'parts'}{$p}{'qty'} = @$ra;
# warn "After: $p -- ", join (',', @$ra), "\n";
    }

    return \%result;
}

# Split $part into part value and IPX part no.
sub split_part {
    my ($in, $part_width) = @_;
    my ($part, $desc);

    # Trim trailing whitespace
    $in =~ s/\s+$//;

    # If the string is >$part_width characters, it
    # includes a description from the include file.
    if (length ($in) > $part_width) {
        # split the description off at the first nonblank on or after
        # position $part_width
        $part = substr ($in, 0, $part_width);
        $desc = substr ($in, $part_width);
        # Use the last field of the first part as the Intraplex part
        # number
        $part =~ s/\s+$//;
        $part =~ /(.+) (.+)/;
        ($1, $2, $desc);
    } else {
        # otherwise, just take the last field as the Intraplex part
        # number; return the part value as the description
        $in =~ /(.+) (.+)/;
        ($1, $2, $1);
    }
}

sub dump_bom {
    my ($self, $name, $ref) = @_;

    open (OUTPUT, ">$name") || die "can't open $name: $!\n";

    print OUTPUT "       Title: $ref->{'title'}\n";
    print OUTPUT "     Updated: $ref->{'sch_date'}\n";
    print OUTPUT "     Drawing: $ref->{'drawing'}\n";
    print OUTPUT "    Revision: $ref->{'revision'}\n";
    print OUTPUT "Organization: $ref->{'organization'}\n";
    print OUTPUT "       Addr1: $ref->{'address1'}\n";
    print OUTPUT "       Addr2: $ref->{'address2'}\n";
    print OUTPUT "       Addr3: $ref->{'address3'}\n";
    print OUTPUT "       Addr4: $ref->{'address4'}\n";
    print OUTPUT "    BOM date: $ref->{'bom_date'}\n";

    for (sort keys %{$ref->{'parts'}}) {
        my $rpart = $ref->{'parts'}{$_};
        printf OUTPUT ("%-14s %3s %s\n", $_, $rpart->{'qty'},
                       join (',', @{$rpart->{'ref'}}));
    }
    close OUTPUT;
}

1;
