#!/usr/bin/perl -w

# mailsplit - split Netscape mail files into one message per file

# Note: Before starting, make sure you've "compacted mailboxes" in
# Netscape. This will ensure that all deleted messages will really
# be gone, and won't come back to haunt us.

# Each mailbox consists of a "Mailbox" file and a "Mailbox.snm" file.
# The first one is the one we want; each message starts with a line of
# the form:
#   From - <day> <month> <date> <hh>:<mm>:<ss> <year>
# followed immediately by the actual headers for the message

# The mail hierarchy (under /Program Files/Netscape/Users/<user>/Mail)
# also contains "Mailbox.sbd" directories, which themselves contain
# mailboxes.

# We'll create a directory for the <user> in ~/mail, then under that
# we'll create a directory for each mailbox and put message files in
# it, using the date + sequence number as the filename (the way fetch.pl
# does it)

# Usage: mailsplit <user> ...

use strict;
use vars qw($debug $quiet @users %months);

%months = ('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04',
           'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08',
           'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12',
           );

# Process command line
for (@ARGV) {
    if (/^-/) {
        foreach my $c (split (/ */, substr ($_, 1))) {
            if ($c eq 'd') {$debug++;}
            if ($c eq 'q') {$quiet++;}
        }
    } else {
        push (@users, $_);
    }
}

foreach my $user (@users) {
    warn "splitting mail for $user\n" unless $quiet;

    my $srcpath = "c:/Program Files/Netscape/Users/$user/Mail";
    my $destpath = "c:/dtweed/mail/$user";
    &process_mail_directory ($srcpath, $destpath);
}

sub process_mail_directory {
    my ($spath, $dpath) = @_;
    my %file;

    die "$dpath not a directory" unless -d $dpath;

    opendir (DIR, $spath) || die "can't readdir $spath: $!";
    my @dir = sort grep (!/^\./, readdir DIR);
    closedir DIR;

    # Find all the unique basenames and match them up with .snm files and/or
    # .sbd directories. Ignore anything that doesn't have an .snm file
    foreach (@dir) {
        if (/\./) {
            my ($base, $ext) = /^(.*)\.([^.]*)$/;
            $file{$base}{$ext} = 1;
        } else {
            $file{$_}{''} = 1;
        }
    }

    foreach my $key (sort keys %file) {
        if ($debug) {
            my @exts = sort keys %{$file{$key}};
            warn "base=$key, exts='", join ("', '", @exts), "'\n";
        }

        next unless $file{$key}{'snm'};

        my $dir = "$dpath/$key";

        if (defined $file{$key}{''}) {
            warn "file $key\n" unless $quiet;

            # create a destination directory if necessary
            mkdir "$dir", 0377 unless -d "$dir";

            # read the destination directory so we can check for duplicate
            # names
            opendir (DIR, $dir) || die "can't readdir $dir: $!";
            my @dfiles = grep (!/^\./, readdir DIR);
            closedir DIR;

            # open the mail file for reading
            open (IN, "$spath/$key") || die "can't read $spath/$key: $!";

            my $out = 0;

            while (<IN>) {
                if (/^From - \w\w\w (\w\w\w) (\d\d) \d\d:\d\d:\d\d (\d\d\d\d)$/) {
                    my ($m, $d, $y) = ($months{$1}, $2, $3);

                    # close the previous output file, if any
                    close OUT if $out;
                    $out = 0;

                    # make a new output filename
                    my $date = "$y$m$d";
                    my $seq;
                    my @files = sort grep (/^$date/, @dfiles);
                    if (@files) {
                        $files[$#files] =~ /^(\d+)-(\d+)/;
                        $seq = $2 + 1;
                    } else {
                        $seq = 0;
                    }
                    my $fname = sprintf ('%s-%03d.txt', $date, $seq);
                    warn "  new file $fname\n" unless $quiet;
                    
                    # open the output file
                    open (OUT, ">$dir/$fname")
                        || die "can't write $dir/$fname: $!";
                    push @dfiles, $fname;
                    $out = 1;
                }
                # write the line to the current output file, if any
                print OUT $_ if $out;
            }
            # close the current output file, if any
            close OUT if $out;
            close IN;
        }

        if (defined $file{$key}{'sbd'}) {
            warn "directory $key\n" unless $quiet;

            # create a destination directory if necessary
            mkdir "$dir", 0377 unless -d "$dir";

            process_mail_directory ("$spath/$key.sbd", "$dir");
        }
    }
}
